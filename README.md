### 创建业务逻辑😀😀😀

[学习链接](https://blog.csdn.net/weixin_47872288/article/details/135258686)

#### 1.流程
1.  Entity 创建数据库实体类
2. Mapper 创建Mapper  该框架包含CRUD 如果有特殊的数据处理写在里面 主要链接SQL 语句
3. Mapper.xml SQL 语句
4. Service 如果有需要特殊处理的SQL可以写在里面，类似于**目前的删除指令 boolean deleteAccount()**
5. ServiceImpl 封装指令
6. Controller 业务逻辑

#### 2.网络请求注解
1.  @GetMapping 查询 -> (**@PathVariable Long id**)
2.  @PutMapping 更新资源   -> (**@PathVariable Long id, @RequestBody User updatedUser**)
3.  @DeleteMapping 删除 -> (**@PathVariable Long id**)
4.  @PostMapping 创建新的资源 插入 -> (**@RequestBody User newUser**)

#### 3.业务流程
1.定义实体类

>model.entity

```java
//Student.java

@Data
public class Student extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;
    private String address;
    private Integer age;

    private Boolean sex;

}

```
2.创建Mapper接口
>mapper
```java
//StudentMapper

@Mapper
public interface StudentMapper  extends BaseMapper<Student> {
    List<Student> getAllStudents();
    Student getUserById(Long id);
    void insertStudent(Student student);
    void updateStudent(Student student);
    void deleteStudent(Student student);
}


```
3.创建Mapper XML文件：（在resources目录下创建一个与Mapper接口同名的XML文件，定义SQL语句）
>resources.mapper
```xml
<!--StudentMapper.xml-->

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.youlai.system.mapper.StudentMapper">

    <select id="getAllStudents" resultType="com.youlai.system.model.entity.Student">
        SELECT  *   FROM students;
    </select>

    <select id="getStudentById" parameterType="Long" resultType="com.youlai.system.model.entity.Student">
        SELECT  *   FROM students WHERE id = #{id};
    </select>

    <select id="insertStudent" parameterType="com.youlai.system.model.entity.Student">
        INSERT INTO students (name, address, age) VALUES (#{name}, #{address}, #{age});
    </select>

    <select id="updateStudent" parameterType="com.youlai.system.model.entity.Student">
        UPDATE students SET name = #{name} , address = #{address}, age = #{age} WHERE id = #{id};
    </select>

    <select id="deleteStudent" parameterType="com.youlai.system.model.entity.Student">
        DELETE FROM students WHERE  id = #{id};
    </select>

</mapper>
```
4.创建Service接口和实现类
>service
>
>**注意这里要调用函数 this.baseMapper.XXXX 即可**
```java
//StudentService

public interface StudentService extends IService<Student> {
    List<Student> getAllStudents();
    Student getStudentById(Long id);
    void insertStudent(Student student);
    void updateStudent(Student student);
    void deleteStduent(Student  student);

}
```
实现类

> **注意这里要调用函数 this.baseMapper.XXXX 即可**

```java
//StudentServiceImpl

@Service
public class StudentServiceImpl  extends ServiceImpl<StudentMapper, Student> implements StudentService {

    @Override
    public List<Student> getAllStudents() {
        return this.baseMapper.getAllStudents();
    }

    @Override
    public Student getStudentById(Long id) {
        return this.baseMapper.getUserById(id) ;
    }

    @Override
    public void insertStudent(Student student) {
        this.baseMapper.insertStudent(student);
    }

    @Override
    public void updateStudent(Student student) {
        this.baseMapper.updateStudent(student);
    }

    @Override
    public void deleteStduent(Student student) {
        this.baseMapper.deleteStudent(student);
    }
}

```

5.五、创建Controller：（用于处理HTTP请求）
>controller.StudentController
```java
//StudentController.java


@Tag(name = "990.Account")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/test/account")
public class AccountController {
    private final AccountService accountService;

    @GetMapping("/page")
    @Operation(summary = "账户分页查询")
    public PageResult<AccountPageVO> getAccountPage(@ParameterObject AccountQuery accountQuery){
        Page<AccountPageVO> result = accountService.getAccountPage(accountQuery);
        return PageResult.success(result);
    }

    @GetMapping
    @Operation(summary = "获取所有账户")
    public Result<List<Account>> getAllAccount(){
        return  Result.success(accountService.list());
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取账户By Id")
    public Result<Account> getAccountById(@PathVariable Long id){
        return Result.success(accountService.getById(id));
    }

    @PutMapping
    @Operation(summary = "更新账户信息")
    @PreAuthorize("@ss.hasPerm('sys:account:edit')")
    public Result<Boolean> updateAccount(@RequestBody Account account){
        return  Result.judge(accountService.updateById(account));
    }

    @DeleteMapping("/{ids}")
    @Operation(summary = "删除账户信息")
    @PreAuthorize("@ss.hasPerm('sys:account:delete')")
    public Result<Boolean> deleteAccount(@PathVariable String ids){
        return Result.judge(accountService.deleteAccount(ids));
    }

    @PostMapping
    @PreventDuplicateSubmit
    @Operation(summary = "添加用户")
    @PreAuthorize("@ss.hasPerm('sys:account:add')")
    public Result<Boolean> saveAccount(@RequestBody Account account){
        return Result.judge(accountService.save(account));
    }
}

```

```youlai-boot
├── sql                                 # SQL脚本
│   ├── mysql5                          # MySQL5 脚本
│   └── mysql8                          # MySQL8 脚本
├── src                                 # 源码目录
│   ├── common                          # 公共模块
│   │   ├── annotation                  # 注解定义
│   │   ├── base                        # 基础类
│   │   ├── constant                    # 常量
│   │   ├── enums                       # 枚举类型
│   │   ├── exception                   # 异常处理
│   │   ├── model                       # 数据模型
│   │   ├── result                      # 结果封装
│   │   └── util                        # 工具类
│   ├── config                          # 自动装配配置
│   │   ├── CorsConfig                  # 跨域共享配置
│   │   ├── MybatisConfig               # Mybatis 自动装配配置
│   │   ├── RedisCacheConfig            # Redis 缓存自动装配配置
│   │   ├── RedisConfig                 # Redis 自动装配配置
│   │   ├── SecurityConfig              # Spring Security 自动装配配置
│   │   ├── SwaggerConfig               # API 接口文档配置
│   │   ├── WebMvcConfig                # WebMvc 配置
│   │   ├── WebSocketConfig             # WebSocket 自动装配配置
│   │   └── XxlJobConfig                # XXL-JOB 自动装配配置
│   ├── core                            # 核心功能模块
│   │   ├── aspect                      # 切面
│   │   │   ├── LogAspect               # 日志切面
│   │   │   └── RepeatSubmitAspect      # 防重提交切面
│   │   ├── filter                      # 过滤器
│   │   │   ├── RateLimiterFilter       # 限流过滤器
│   │   │   └── RequestLogFilter        # 请求日志过滤器
│   │   ├── handler                     # 处理器
│   │   │   ├── MyDataPermissionHandler # 数据权限处理器
│   │   │   └── MyMetaObjectHandler     # 元对象字段填充处理器
│   │   └── security                    # Security 安全中心
│   ├── platform                        # 平台基础设施模块
│   │   ├── auth                        # 授权
│   │   ├── file                        # 文件处理
│   │   ├── generator                   # 代码生成器
│   │   ├── mail                        # 邮件处理
│   │   └── sms                         # 短信处理
│   ├── system                          # 系统模块
│   │   ├── controller                  # 控制层
│   │   ├── converter                   # MapStruct 转换器
│   │   ├── event                       # 事件处理
│   │   ├── handler                     # 处理器
│   │   ├── listener                    # 监听器
│   │   ├── model                       # 模型层
│   │   │   ├── bo                      # 业务对象
│   │   │   ├── dto                     # 数据传输对象
│   │   │   ├── entity                  # 实体对象
│   │   │   ├── form                    # 表单对象
│   │   │   ├── query                   # 查询参数对象
│   │   │   └── vo                      # 视图对象
│   │   ├── mapper                      # 数据库访问层
│   │   └── service                     # 业务逻辑层
│   └── YouLaiApplication               # 启动类
└── end                     ```

```
加@Resource就可以直接引用ApplicationContextHelper.getBean(RedisUtil.class);

    @Resource
    RedisUtil redisUtil = ApplicationContextHelper.getBean(RedisUtil.class);
    @Resource
    TrendConfigService trendConfigService = ApplicationContextHelper.getBean(TrendConfigService.class);

        try{
            //获取数据库所有trend
            trendConfigList = trendConfigService.list();
            //获取redis数据
            for (TrendConfig trendConfig : trendConfigList){
                RedisNode redisData = (RedisNode) redisUtil.get(trendConfig.getAddress());
                parameterPanel.setUnit(redisData.getTrendConfig().getUnit());
                parameterPanel.setDescription(redisData.getTrendConfig().getDescription());
                parameterPanel.setEnable(redisData.getTrendConfig().getEnable());
                parameterPanel.setValue(redisData.getCurrentValue());
                parameterPanel.setDevice(redisData.getTrendConfig().getDevice());
                parameterPanelList.add(parameterPanel);
            }
        }catch (Exception e){
            throw new RuntimeException("更新故障：",e);
        }
        return  Result.success(parameterPanelList);
    }

}

```