/*
 Navicat Premium Dump SQL

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 80036 (8.0.36)
 Source Host           : localhost:3306
 Source Schema         : db_plc

 Target Server Type    : MySQL
 Target Server Version : 80036 (8.0.36)
 File Encoding         : 65001

 Date: 22/10/2024 18:12:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 83 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES (61, 'Hello', '1', '1');
INSERT INTO `account` VALUES (62, 'Allen2', '1', '1');

-- ----------------------------
-- Table structure for data_logs
-- ----------------------------
DROP TABLE IF EXISTS `data_logs`;
CREATE TABLE `data_logs`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `device_id` int NULL DEFAULT NULL,
  `value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `unit` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `property` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of data_logs
-- ----------------------------
INSERT INTO `data_logs` VALUES (1, 1, '25.23', '℃', '2024-09-02 15:10:10', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (2, 1, '25.4', '℃', '2024-09-02 15:10:11', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (3, 1, '25.5', '℃', '2024-09-02 15:10:12', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (4, 1, '23.5', '℃', '2024-09-02 15:10:13', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (5, 1, '22.5', '℃', '2024-09-02 15:10:14', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (6, 1, '26.4', '℃', '2024-09-02 15:10:15', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (7, 1, '25.3', '℃', '2024-09-02 15:10:16', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (8, 1, '28.6', '℃', '2024-09-02 15:10:17', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (9, 1, '30.0', '℃', '2024-09-02 15:10:18', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (10, 1, '25.23', '℃', '2024-09-02 15:10:19', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (11, 1, '19.2', '℃', '2024-09-02 15:10:20', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (12, 1, '33.6', '℃', '2024-09-02 15:10:21', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (13, 1, '25.3', '℃', '2024-09-02 15:10:22', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (14, 1, '28.5', '℃', '2024-09-02 15:10:23', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (15, 1, '25.23', '℃', '2024-09-02 15:10:24', 'NULL', '温度');
INSERT INTO `data_logs` VALUES (19, 1, '25.23', '℃', '2024-09-02 15:10:25', 'NULL', '温度');

-- ----------------------------
-- Table structure for energy_device
-- ----------------------------
DROP TABLE IF EXISTS `energy_device`;
CREATE TABLE `energy_device`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '设备名称',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '设备编码',
  `type` tinyint NOT NULL DEFAULT 1 COMMENT '类型(1-正常 0-禁用)',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '状态（0：无；1=水；2=电；3=气）',
  `value` decimal(10, 0) NULL DEFAULT 0 COMMENT '实际值',
  `value_h` decimal(10, 0) NULL DEFAULT 0 COMMENT '高警告',
  `value_h_h` decimal(10, 0) NULL DEFAULT 0 COMMENT '高故障',
  `value_l` decimal(10, 0) NULL DEFAULT 0 COMMENT '低警告',
  `value_l_l` decimal(10, 0) NULL DEFAULT 0 COMMENT '低故障',
  `log_interval` decimal(10, 0) NULL DEFAULT 0 COMMENT '低故障',
  `create_by` bigint NULL DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint NULL DEFAULT NULL COMMENT '修改人ID',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_code`(`code` ASC) USING BTREE COMMENT '编号唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '能源设备' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of energy_device
-- ----------------------------
INSERT INTO `energy_device` VALUES (4, 'qqq', 'qqq', 1, 1, 0, 0, 0, 0, 0, 0, NULL, '2024-08-28 18:48:42', NULL, '2024-08-28 18:48:42');

-- ----------------------------
-- Table structure for energy_log_electricity
-- ----------------------------
DROP TABLE IF EXISTS `energy_log_electricity`;
CREATE TABLE `energy_log_electricity`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `energy_device_id` bigint NOT NULL COMMENT '设备ID',
  `value` decimal(10, 0) NULL DEFAULT 0 COMMENT '实际值',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '能源设备' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of energy_log_electricity
-- ----------------------------

-- ----------------------------
-- Table structure for energy_log_gas
-- ----------------------------
DROP TABLE IF EXISTS `energy_log_gas`;
CREATE TABLE `energy_log_gas`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `energy_device_id` bigint NOT NULL COMMENT '设备ID',
  `value` decimal(10, 0) NULL DEFAULT 0 COMMENT '实际值',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '能源设备' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of energy_log_gas
-- ----------------------------

-- ----------------------------
-- Table structure for energy_log_water
-- ----------------------------
DROP TABLE IF EXISTS `energy_log_water`;
CREATE TABLE `energy_log_water`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `energy_device_id` bigint NOT NULL COMMENT '设备ID',
  `value` decimal(10, 0) NULL DEFAULT 0 COMMENT '实际值',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '能源设备' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of energy_log_water
-- ----------------------------

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `status` tinyint(1) NOT NULL COMMENT '类型 0=正常，1=警告，2=错误',
  `message` varchar(5000) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '信息',
  `source` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '来源',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` bigint NOT NULL COMMENT '创建人',
  `update_user` bigint NOT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_bin COMMENT = '日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of log
-- ----------------------------

-- ----------------------------
-- Table structure for people
-- ----------------------------
DROP TABLE IF EXISTS `people`;
CREATE TABLE `people`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `age` int NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of people
-- ----------------------------

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sex` tinyint(1) NULL DEFAULT NULL,
  `age` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of students
-- ----------------------------
INSERT INTO `students` VALUES (1, '2', '2', 22, 15);
INSERT INTO `students` VALUES (2, 'Allen', '四川省达州市宣汉县', 1, 25);
INSERT INTO `students` VALUES (3, '1', '1', 1, 1);

-- ----------------------------
-- Table structure for sys_alarm_statis
-- ----------------------------
DROP TABLE IF EXISTS `sys_alarm_statis`;
CREATE TABLE `sys_alarm_statis`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `event` int NOT NULL,
  `warning` int NOT NULL,
  `fault` int NOT NULL,
  `time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_alarm_statis
-- ----------------------------
INSERT INTO `sys_alarm_statis` VALUES (1, 1, 1, 1, '1');
INSERT INTO `sys_alarm_statis` VALUES (2, 2, 2, 2, '2');
INSERT INTO `sys_alarm_statis` VALUES (3, 3, 3, 3, '3');

-- ----------------------------
-- Table structure for sys_city
-- ----------------------------
DROP TABLE IF EXISTS `sys_city`;
CREATE TABLE `sys_city`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `city_point` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_city
-- ----------------------------
INSERT INTO `sys_city` VALUES (1, '成都', '15.2.2858', '2024-05-06 15:23:15');
INSERT INTO `sys_city` VALUES (2, '成都', '15.2.2858', '2024-05-06 15:23:15');
INSERT INTO `sys_city` VALUES (3, '成都', '15.2.2858', '2024-05-06 15:23:15');
INSERT INTO `sys_city` VALUES (4, '成都', '15.2.2858', '2024-05-06 15:23:15');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门名称',
  `parent_id` bigint NOT NULL DEFAULT 0 COMMENT '父节点id',
  `tree_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '父节点id路径',
  `sort` int NULL DEFAULT 0 COMMENT '显示顺序',
  `status` tinyint NOT NULL DEFAULT 1 COMMENT '状态(1:正常;0:禁用)',
  `deleted` tinyint NULL DEFAULT 0 COMMENT '逻辑删除标识(1:已删除;0:未删除)',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_by` bigint NULL DEFAULT NULL COMMENT '创建人ID',
  `update_by` bigint NULL DEFAULT NULL COMMENT '修改人ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, '费德自动化（重庆）重庆有限公司', 0, '0', 1, 1, 0, NULL, '2024-08-30 00:46:03', 1, 1);
INSERT INTO `sys_dept` VALUES (2, '研发部门', 1, '0,1', 1, 1, 0, NULL, '2022-04-19 12:46:37', 2, 2);
INSERT INTO `sys_dept` VALUES (3, '测试部门', 1, '0,1', 1, 1, 0, NULL, '2022-04-19 12:46:37', 2, 2);

-- ----------------------------
-- Table structure for sys_device_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_device_config`;
CREATE TABLE `sys_device_config`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `ip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `port` int NULL DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type` int NULL DEFAULT NULL,
  `protocol` int NULL DEFAULT NULL,
  `connected` tinyint(1) NULL DEFAULT NULL,
  `subscribed` tinyint(1) NULL DEFAULT NULL,
  `enable` tinyint(1) NULL DEFAULT NULL,
  `position` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `time` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_device_config
-- ----------------------------
INSERT INTO `sys_device_config` VALUES (1, '192.168.0.1', 4840, 'L1MFB11', 1, 1, 0, 0, 1, '116.39,39.9', 'KVS系统1', '2024-10-15 21:31:59');
INSERT INTO `sys_device_config` VALUES (2, '192.168.0.2', 4840, 'L1MFB12', 1, 1, 0, 0, 1, '116.41,39.92', 'KVS系统2', '2024-10-15 21:25:16');
INSERT INTO `sys_device_config` VALUES (3, '192.168.0.3', 4840, 'L1MFB13', 1, 1, 0, 0, 1, '116.37,39.88', 'KVS系统3', '2024-10-15 21:25:16');
INSERT INTO `sys_device_config` VALUES (31, '192.168.6.6', 4840, '测试设备1', 1, 1, 0, 0, 1, '116.37,40', '测试设备1，区域2', '2024-10-21 18:25:59');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典类型编码',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典项名称',
  `value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典项值',
  `sort` int NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint NULL DEFAULT 0 COMMENT '状态(1:正常;0:禁用)',
  `defaulted` tinyint NULL DEFAULT 0 COMMENT '是否默认(1:是;0:否)',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, 'gender', '男', '1', 1, 1, 0, NULL, '2019-05-05 13:07:52', '2022-06-12 23:20:39');
INSERT INTO `sys_dict` VALUES (2, 'gender', '女', '2', 2, 1, 0, NULL, '2019-04-19 11:33:00', '2019-07-02 14:23:05');
INSERT INTO `sys_dict` VALUES (3, 'gender', '未知', '0', 1, 1, 0, NULL, '2020-10-17 08:09:31', '2020-10-17 08:09:31');
INSERT INTO `sys_dict` VALUES (69, 'avatar', 'avatar_1', 'null', 1, 1, 0, '头像', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '类型名称',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '类型编码',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '状态(0:正常;1:禁用)',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `type_code`(`code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '性别', 'gender', 1, NULL, '2019-12-06 19:03:32', '2022-06-12 16:21:28');
INSERT INTO `sys_dict_type` VALUES (89, '头像', 'avatar', 1, NULL, '2024-08-30 00:27:47', '2024-08-30 00:27:47');

-- ----------------------------
-- Table structure for sys_dm_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_dm_config`;
CREATE TABLE `sys_dm_config`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `device` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `enable` tinyint(1) NOT NULL,
  `text_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `zh_cn` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `en_us` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `de_de` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ko_kr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dm_config
-- ----------------------------
INSERT INTO `sys_dm_config` VALUES (1, 'L1MFB11', 'Test.BOOL1', 1, 'F00_2000', '电机温度故障', 'Temperature Fault Of  Motor ', 'Motortemperaturfehler', '모터 온도 고장', '2024-10-18 22:22:37');
INSERT INTO `sys_dm_config` VALUES (2, 'PLC_1', 'Test.BOOL2', 1, 'W00_2000', '电机温度警告', 'Temperature Warning Of Motor', 'Motor-Temperatur-Warnung', '모터 온도 경고', '2024-09-22 10:14:40');
INSERT INTO `sys_dm_config` VALUES (3, 'PLC_1', 'Test.BOOL3', 1, 'W00_2000', '电机温度警告', 'Temperature Warning Of Motor', 'Motor-Temperatur-Warnung', '모터 온도 경고', '2024-09-25 22:02:14');
INSERT INTO `sys_dm_config` VALUES (101, 'PLC_1', 'Test.BOOL4', 1, 'F00_3000', '高压力故障', 'Pressure High Fault', 'Pressure High Fault', 'Pressure High Fault', '2024-09-25 22:19:32');

-- ----------------------------
-- Table structure for sys_event
-- ----------------------------
DROP TABLE IF EXISTS `sys_event`;
CREATE TABLE `sys_event`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `device_group` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `status` int NOT NULL,
  `confirm` tinyint(1) NULL DEFAULT NULL,
  `confirm_user` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `confirm_time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 953 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_event
-- ----------------------------
INSERT INTO `sys_event` VALUES (745, '地址', '组1', '订阅', 'DBLine.ArrReal[10]', 'S7-1500', 1, 1, 'admin', '电机温度警告', '2024-10-04 15:09:14', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (746, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机温度故障', '2024-10-02 16:20:22', '2024-10-04 15:09:21');
INSERT INTO `sys_event` VALUES (747, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机启动', '2024-10-04 15:09:42', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (748, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机温度故障', '2024-10-02 16:20:23', '2024-10-04 15:09:33');
INSERT INTO `sys_event` VALUES (749, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机启动', '2024-10-04 15:15:26', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (750, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机启动', '2024-10-04 15:15:26', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (751, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机温度故障', '2024-10-04 15:15:26', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (752, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机启动', '2024-10-04 15:09:51', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (753, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机启动', '2024-10-04 15:09:51', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (754, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机启动', '2024-10-04 15:09:51', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (755, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-02 16:20:36', '2024-10-04 15:17:38');
INSERT INTO `sys_event` VALUES (756, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机启动', '2024-10-02 16:20:36', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (757, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机启动', '2024-10-02 16:20:37', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (758, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 1, 1, 'admin', '电机启动', '2024-10-02 16:20:39', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (827, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-02 16:49:53', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (828, '地址', '组1', '订阅', 'DBLine.Real6', 'S7-1500', 3, 1, 'admin', '电机温度故障6', '2024-10-02 16:49:54', '2024-10-04 15:17:56');
INSERT INTO `sys_event` VALUES (829, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-02 16:49:55', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (830, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-02 16:49:55', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (831, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-02 16:49:56', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (832, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-02 16:49:56', '2024-10-04 15:18:34');
INSERT INTO `sys_event` VALUES (833, '设备', '组1', '连接PLC', '现场设备', 'S7-1500', 3, 1, 'admin', 'PLC连接失败请检查参数，或者网络', '2024-10-15 20:30:05', '2024-10-15 20:30:17');
INSERT INTO `sys_event` VALUES (834, '设备', '组1', '连接PLC', '现场设备', 'S7-1500', 3, 1, 'admin', 'PLC连接失败请检查参数，或者网络', '2024-10-15 21:31:37', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (835, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:33:53', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (836, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:33:53', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (837, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:33:54', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (838, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:33:54', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (839, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:33:55', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (840, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:33:55', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (841, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:33:56', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (842, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:33:56', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (843, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:33:57', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (844, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:33:57', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (845, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:33:58', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (846, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:33:58', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (847, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:33:58', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (848, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:33:58', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (849, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:33:59', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (850, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:33:59', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (851, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:00', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (852, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:00', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (853, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:01', '2024-10-15 21:45:43');
INSERT INTO `sys_event` VALUES (854, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:01', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (855, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:03', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (856, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:03', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (857, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:03', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (858, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:04', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (859, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:04', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (860, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:05', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (861, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:05', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (862, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:06', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (863, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:06', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (864, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:07', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (865, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:07', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (866, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:07', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (867, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:07', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (868, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:08', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (869, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:08', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (870, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:09', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (871, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:09', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (872, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:10', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (873, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:10', '2024-10-15 21:35:01');
INSERT INTO `sys_event` VALUES (874, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:11', '2024-10-15 21:46:02');
INSERT INTO `sys_event` VALUES (875, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:11', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (876, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '电机温度警告', '2024-10-15 21:34:12', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (877, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '电机温度故障', '2024-10-15 21:34:12', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (878, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:54:42', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (879, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:54:42', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (880, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:54:44', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (881, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:54:46', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (882, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:54:46', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (883, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:54:48', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (884, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:54:48', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (885, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:54:49', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (886, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:54:50', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (887, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:54:52', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (888, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:54:53', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (889, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:54:56', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (890, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:54:57', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (891, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:54:57', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (892, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:54:58', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (893, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:00', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (894, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:55:00', '2024-10-15 21:55:56');
INSERT INTO `sys_event` VALUES (895, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:01', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (896, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:05', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (897, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:55:05', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (898, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:06', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (899, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:55:06', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (900, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:07', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (901, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:08', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (902, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:11', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (903, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:13', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (904, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:14', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (905, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:15', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (906, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:17', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (907, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:18', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (908, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:20', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (909, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:55:20', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (910, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:21', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (911, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:23', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (912, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:24', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (913, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:55:24', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (914, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:25', '2024-10-15 21:55:59');
INSERT INTO `sys_event` VALUES (915, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:26', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (916, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:27', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (917, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:55:27', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (918, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:28', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (919, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:55:28', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (920, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:30', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (921, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:31', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (922, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:33', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (923, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:34', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (924, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:40', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (925, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:41', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (926, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:42', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (927, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:46', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (928, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:47', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (929, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:48', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (930, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:49', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (931, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:50', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (932, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:52', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (933, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:53', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (934, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:55', '2024-10-18 22:16:27');
INSERT INTO `sys_event` VALUES (935, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:55:55', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (936, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:55:56', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (937, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:55:56', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (938, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:57', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (939, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:58', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (940, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:55:59', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (941, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:56:00', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (942, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:56:01', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (943, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:56:02', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (944, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:56:04', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (945, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度高警告', '2024-10-15 21:56:05', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (946, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 2, 1, 'admin', '温度低警告', '2024-10-15 21:56:07', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (947, '地址', '组1', '订阅', 'DBLine.Real1', 'S7-1500', 3, 1, 'admin', '温度低故障', '2024-10-15 21:56:07', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (948, '设备', '组1', '断开PLC', '现场设备', 'S7-1500', 1, 1, 'admin', '尝试断开连接PLC', '2024-10-15 21:56:09', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (949, '设备', '组1', '连接PLC', '现场设备', 'S7-1500', 3, 1, 'admin', 'PLC连接失败请检查参数，或者网络', '2024-10-18 22:16:07', '2024-10-18 22:16:23');
INSERT INTO `sys_event` VALUES (950, '设备', '组1', '连接PLC', '现场设备', 'S7-1500', 3, 1, 'admin', 'PLC连接失败请检查参数，或者网络', '2024-10-18 22:21:11', '2024-10-18 22:21:33');
INSERT INTO `sys_event` VALUES (951, '设备', '组1', '连接PLC', '现场设备', 'S7-1500', 3, 0, NULL, 'PLC连接失败请检查参数，或者网络', '2024-10-18 22:21:18', NULL);
INSERT INTO `sys_event` VALUES (952, '设备', '组1', '连接PLC', '现场设备', 'S7-1500', 3, 0, NULL, 'PLC连接失败请检查参数，或者网络', '2024-10-22 10:26:15', NULL);

-- ----------------------------
-- Table structure for sys_event_text_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_event_text_config`;
CREATE TABLE `sys_event_text_config`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `text_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `zh_cn` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `en_us` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `de_de` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ko_kr` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_event_text_config
-- ----------------------------
INSERT INTO `sys_event_text_config` VALUES (24, 'F00_2000', '电机温度故障', 'Temperature Fault Of  Motor ', 'Motortemperaturfehler', '모터 온도 고장', '2024-09-22 00:20:55');
INSERT INTO `sys_event_text_config` VALUES (25, 'W00_2000', '电机温度警告', 'Temperature Warning Of Motor', 'Motor-Temperatur-Warnung', '모터 온도 경고', '2024-09-22 00:16:17');
INSERT INTO `sys_event_text_config` VALUES (26, 'E00_2000', '电机启动', 'Start Of Motor', 'Motorstart', '모터 시작', '2024-09-22 10:15:06');
INSERT INTO `sys_event_text_config` VALUES (27, 'F00_3000', '高压力故障', 'Pressure High Fault', 'Pressure High Fault', 'Pressure High Fault', '2024-09-25 22:18:48');
INSERT INTO `sys_event_text_config` VALUES (28, 'F00_3000', '温度高故障', 'High Temperature Fault', 'High Temperature Fault', 'High Temperature Fault', '2024-10-15 21:51:55');
INSERT INTO `sys_event_text_config` VALUES (29, 'F00_3001', '温度低故障', 'Low Temperature Fault', 'Low Temperature Fault', 'Low Temperature Fault', '2024-10-15 21:51:45');
INSERT INTO `sys_event_text_config` VALUES (30, 'W00_3000', '温度高警告', 'High Temperature Warning', 'High Temperature Warning', 'High Temperature Warning', '2024-10-15 21:52:35');
INSERT INTO `sys_event_text_config` VALUES (31, 'W00_3002', '温度低警告', 'Low Temperature Warning', 'Low Temperature Warning', 'Low Temperature Warning', '2024-10-15 21:53:03');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `parent_id` bigint NOT NULL COMMENT '父菜单ID',
  `tree_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父节点ID路径',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单名称',
  `type` tinyint NOT NULL COMMENT '菜单类型(1:菜单 2:目录 3:外链 4:按钮)',
  `path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由路径(浏览器地址栏路径)',
  `component` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径(vue页面完整路径，省略.vue后缀)',
  `perm` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `visible` tinyint(1) NOT NULL DEFAULT 1 COMMENT '显示状态(1-显示;0-隐藏)',
  `sort` int NULL DEFAULT 0 COMMENT '排序',
  `icon` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单图标',
  `redirect` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '跳转路径',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `always_show` tinyint NULL DEFAULT NULL COMMENT '【目录】只有一个子路由是否始终显示(1:是 0:否)',
  `keep_alive` tinyint NULL DEFAULT NULL COMMENT '【菜单】是否开启页面缓存(1:是 0:否)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 194 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '0', '系统管理', 2, '/system', 'Layout', NULL, 1, 1, 'system', '/system/user', '2021-08-28 09:12:21', '2024-08-28 15:54:27', NULL, NULL);
INSERT INTO `sys_menu` VALUES (2, 1, '0,1', '用户管理', 1, 'user', 'system/user/index', NULL, 1, 1, 'user', NULL, '2021-08-28 09:12:21', '2021-08-28 09:12:21', NULL, 1);
INSERT INTO `sys_menu` VALUES (3, 1, '0,1', '角色管理', 1, 'role', 'system/role/index', NULL, 1, 2, 'role', NULL, '2021-08-28 09:12:21', '2021-08-28 09:12:21', NULL, 1);
INSERT INTO `sys_menu` VALUES (4, 1, '0,1', '菜单管理', 1, 'menu', 'system/menu/index', NULL, 1, 3, 'menu', NULL, '2021-08-28 09:12:21', '2021-08-28 09:12:21', NULL, 1);
INSERT INTO `sys_menu` VALUES (5, 1, '0,1', '部门管理', 1, 'dept', 'system/dept/index', NULL, 1, 4, 'tree', NULL, '2021-08-28 09:12:21', '2021-08-28 09:12:21', NULL, 1);
INSERT INTO `sys_menu` VALUES (6, 1, '0,1', '字典管理', 1, 'dict', 'system/dict/index', NULL, 1, 5, 'dict', NULL, '2021-08-28 09:12:21', '2021-08-28 09:12:21', NULL, 1);
INSERT INTO `sys_menu` VALUES (20, 0, '0', '多级菜单', 2, '/multi-level', 'Layout', NULL, 1, 9, 'cascader', '/multi-level/multi-level1', '2022-02-16 23:11:00', '2024-08-30 00:25:40', NULL, NULL);
INSERT INTO `sys_menu` VALUES (21, 20, '0,20', '菜单一级', 1, 'multi-level1', 'demo/multi-level/level1', NULL, 1, 1, '', '/multi-level/multi-level2', '2022-02-16 23:13:38', '2022-02-16 23:13:38', NULL, 1);
INSERT INTO `sys_menu` VALUES (22, 21, '0,20,21', '菜单二级', 1, 'multi-level2', 'demo/multi-level/children/level2', NULL, 1, 1, '', '/multi-level/multi-level2/multi-level3-1', '2022-02-16 23:14:23', '2022-02-16 23:14:23', NULL, 1);
INSERT INTO `sys_menu` VALUES (23, 22, '0,20,21,22', '菜单三级-1', 1, 'multi-level3-1', 'demo/multi-level/children/children/level3-1', NULL, 1, 1, '', '', '2022-02-16 23:14:51', '2022-02-16 23:14:51', NULL, 1);
INSERT INTO `sys_menu` VALUES (24, 22, '0,20,21,22', '菜单三级-2', 1, 'multi-level3-2', 'demo/multi-level/children/children/level3-2', NULL, 1, 2, '', '', '2022-02-16 23:15:08', '2022-02-16 23:15:08', NULL, 1);
INSERT INTO `sys_menu` VALUES (26, 0, '0', '平台文档', 2, '/doc', 'Layout', NULL, 1, 8, 'document', NULL, '2022-02-17 22:51:20', '2024-08-30 00:25:36', NULL, NULL);
INSERT INTO `sys_menu` VALUES (30, 26, '0,26', '平台文档(外链)', 3, 'https://juejin.cn/post/7228990409909108793', '', NULL, 1, 2, 'link', '', '2022-02-18 00:01:40', '2022-02-18 00:01:40', NULL, NULL);
INSERT INTO `sys_menu` VALUES (31, 2, '0,1,2', '用户新增', 4, '', NULL, 'sys:user:add', 1, 1, '', '', '2022-10-23 11:04:08', '2022-10-23 11:04:11', NULL, NULL);
INSERT INTO `sys_menu` VALUES (32, 2, '0,1,2', '用户编辑', 4, '', NULL, 'sys:user:edit', 1, 2, '', '', '2022-10-23 11:04:08', '2022-10-23 11:04:11', NULL, NULL);
INSERT INTO `sys_menu` VALUES (33, 2, '0,1,2', '用户删除', 4, '', NULL, 'sys:user:delete', 1, 3, '', '', '2022-10-23 11:04:08', '2022-10-23 11:04:11', NULL, NULL);
INSERT INTO `sys_menu` VALUES (36, 0, '0', '组件封装', 2, '/component', 'Layout', NULL, 1, 10, 'menu', '', '2022-10-31 09:18:44', '2024-08-30 00:25:27', NULL, NULL);
INSERT INTO `sys_menu` VALUES (37, 36, '0,36', '文本编辑', 1, 'wang-editor', 'demo/wang-editor', NULL, 1, 1, '', '', NULL, '2024-09-25 15:42:24', NULL, 1);
INSERT INTO `sys_menu` VALUES (38, 36, '0,36', '图片上传', 1, 'upload', 'demo/upload', NULL, 1, 2, '', '', '2022-11-20 23:16:30', '2022-11-20 23:16:32', NULL, 1);
INSERT INTO `sys_menu` VALUES (39, 36, '0,36', '图标选择器', 1, 'icon-selector', 'demo/icon-selector', NULL, 1, 3, '', '', '2022-11-20 23:16:30', '2022-11-20 23:16:32', NULL, 1);
INSERT INTO `sys_menu` VALUES (40, 0, '0', '接口', 2, '/api', 'Layout', NULL, 1, 7, 'api', '', '2022-02-17 22:51:20', '2024-08-30 00:24:30', 1, NULL);
INSERT INTO `sys_menu` VALUES (41, 40, '0,40', '接口文档', 1, 'api-doc', 'demo/api-doc', NULL, 1, 1, 'api', '', '2022-02-17 22:51:20', '2022-02-17 22:51:20', NULL, 1);
INSERT INTO `sys_menu` VALUES (70, 3, '0,1,3', '角色新增', 4, '', NULL, 'sys:role:add', 1, 1, '', NULL, '2023-05-20 23:39:09', '2023-05-20 23:39:09', NULL, NULL);
INSERT INTO `sys_menu` VALUES (71, 3, '0,1,3', '角色编辑', 4, '', NULL, 'sys:role:edit', 1, 2, '', NULL, '2023-05-20 23:40:31', '2023-05-20 23:40:31', NULL, NULL);
INSERT INTO `sys_menu` VALUES (72, 3, '0,1,3', '角色删除', 4, '', NULL, 'sys:role:delete', 1, 3, '', NULL, '2023-05-20 23:41:08', '2023-05-20 23:41:08', NULL, NULL);
INSERT INTO `sys_menu` VALUES (73, 4, '0,1,4', '菜单新增', 4, '', NULL, 'sys:menu:add', 1, 1, '', NULL, '2023-05-20 23:41:35', '2023-05-20 23:41:35', NULL, NULL);
INSERT INTO `sys_menu` VALUES (74, 4, '0,1,4', '菜单编辑', 4, '', NULL, 'sys:menu:edit', 1, 3, '', NULL, '2023-05-20 23:41:58', '2023-05-20 23:41:58', NULL, NULL);
INSERT INTO `sys_menu` VALUES (75, 4, '0,1,4', '菜单删除', 4, '', NULL, 'sys:menu:delete', 1, 3, '', NULL, '2023-05-20 23:44:18', '2023-05-20 23:44:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES (76, 5, '0,1,5', '部门新增', 4, '', NULL, 'sys:dept:add', 1, 1, '', NULL, '2023-05-20 23:45:00', '2023-05-20 23:45:00', NULL, NULL);
INSERT INTO `sys_menu` VALUES (77, 5, '0,1,5', '部门编辑', 4, '', NULL, 'sys:dept:edit', 1, 2, '', NULL, '2023-05-20 23:46:16', '2023-05-20 23:46:16', NULL, NULL);
INSERT INTO `sys_menu` VALUES (78, 5, '0,1,5', '部门删除', 4, '', NULL, 'sys:dept:delete', 1, 3, '', NULL, '2023-05-20 23:46:36', '2023-05-20 23:46:36', NULL, NULL);
INSERT INTO `sys_menu` VALUES (79, 6, '0,1,6', '字典类型新增', 4, '', NULL, 'sys:dict_type:add', 1, 1, '', NULL, '2023-05-21 00:16:06', '2023-05-21 00:16:06', NULL, NULL);
INSERT INTO `sys_menu` VALUES (81, 6, '0,1,6', '字典类型编辑', 4, '', NULL, 'sys:dict_type:edit', 1, 2, '', NULL, '2023-05-21 00:27:37', '2023-05-21 00:27:37', NULL, NULL);
INSERT INTO `sys_menu` VALUES (84, 6, '0,1,6', '字典类型删除', 4, '', NULL, 'sys:dict_type:delete', 1, 3, '', NULL, '2023-05-21 00:29:39', '2023-05-21 00:29:39', NULL, NULL);
INSERT INTO `sys_menu` VALUES (85, 6, '0,1,6', '字典数据新增', 4, '', NULL, 'sys:dict:add', 1, 4, '', NULL, '2023-05-21 00:46:56', '2023-05-21 00:47:06', NULL, NULL);
INSERT INTO `sys_menu` VALUES (86, 6, '0,1,6', '字典数据编辑', 4, '', NULL, 'sys:dict:edit', 1, 5, '', NULL, '2023-05-21 00:47:36', '2023-05-21 00:47:36', NULL, NULL);
INSERT INTO `sys_menu` VALUES (87, 6, '0,1,6', '字典数据删除', 4, '', NULL, 'sys:dict:delete', 1, 6, '', NULL, '2023-05-21 00:48:10', '2023-05-21 00:48:20', NULL, NULL);
INSERT INTO `sys_menu` VALUES (88, 2, '0,1,2', '重置密码', 4, '', NULL, 'sys:user:reset_pwd', 1, 4, '', NULL, '2023-05-21 00:49:18', '2023-05-21 00:49:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES (89, 0, '0', '功能演示', 2, '/function', 'Layout', NULL, 1, 11, 'menu', '', '2022-10-31 09:18:44', '2024-08-30 00:25:31', NULL, NULL);
INSERT INTO `sys_menu` VALUES (90, 89, '0,89', 'threejs', 1, 'threejs', 'demo/threejs', NULL, 1, 3, 'cascader', '', '2022-11-20 23:16:30', '2024-09-16 13:32:29', NULL, 1);
INSERT INTO `sys_menu` VALUES (91, 89, '0,89', '敬请期待...', 2, 'other', 'demo/other', NULL, 1, 4, '', '', '2022-11-20 23:16:30', '2022-11-20 23:16:32', NULL, NULL);
INSERT INTO `sys_menu` VALUES (93, 36, '0,36', '签名', 1, 'signature', 'demo/signature', NULL, 1, 6, '', '', '2022-11-20 23:16:30', '2022-11-20 23:16:32', NULL, 1);
INSERT INTO `sys_menu` VALUES (94, 36, '0,36', '表格', 1, 'table', 'demo/table', NULL, 1, 7, '', '', '2022-11-20 23:16:30', '2022-11-20 23:16:32', NULL, 1);
INSERT INTO `sys_menu` VALUES (95, 36, '0,36', '字典组件', 1, 'dict-demo', 'demo/dict', NULL, 1, 4, '', '', '2022-11-20 23:16:30', '2022-11-20 23:16:32', NULL, 1);
INSERT INTO `sys_menu` VALUES (96, 89, '0,89', 'Permission', 1, 'permission', 'demo/permission/page', NULL, 1, 1, '', '', '2022-11-20 23:16:30', '2022-11-20 23:16:32', NULL, 1);
INSERT INTO `sys_menu` VALUES (97, 89, '0,89', 'Icons', 1, 'icon-demo', 'demo/icons', NULL, 1, 2, '', '', '2022-11-20 23:16:30', '2022-11-20 23:16:32', NULL, 1);
INSERT INTO `sys_menu` VALUES (98, 0, '0', 'Table', 2, '/table', 'Layout', NULL, 1, 10, 'table', '', '2023-08-08 20:49:50', '2024-08-30 00:25:20', NULL, NULL);
INSERT INTO `sys_menu` VALUES (99, 98, '0,98', '动态Table', 1, 'dynamic-table', 'table/dynamic-table/index', NULL, 1, 1, '', '', '2023-08-08 20:54:42', '2024-09-09 10:59:46', NULL, 1);
INSERT INTO `sys_menu` VALUES (100, 98, '0,98', '拖拽Table', 1, 'drag-table', 'table/drag-table', NULL, 1, 2, '', '', '2023-08-08 20:54:42', '2024-09-09 10:59:52', NULL, 1);
INSERT INTO `sys_menu` VALUES (101, 98, '0,98', '综合Table', 1, 'complex-table', 'table/complex-table', NULL, 1, 3, '', '', '2023-08-08 20:54:42', '2024-09-09 11:00:02', NULL, 1);
INSERT INTO `sys_menu` VALUES (102, 26, '0,26', '平台文档(内嵌)', 3, 'internal-doc', 'demo/internal-doc', NULL, 1, 1, 'document', '', '2022-02-18 00:01:40', '2022-02-18 00:01:40', NULL, NULL);
INSERT INTO `sys_menu` VALUES (119, 0, '0', '能源管理', 2, '/energy', 'Layout', NULL, 1, 1, 'client', '/energy/device', '2024-08-28 15:30:06', '2024-08-30 00:46:44', 1, 0);
INSERT INTO `sys_menu` VALUES (120, 119, '0,119', '设备管理', 1, 'device', 'energy/devicePage/index', NULL, 1, 1, 'table', NULL, '2024-08-28 15:31:44', '2024-09-03 13:30:57', NULL, NULL);
INSERT INTO `sys_menu` VALUES (121, 120, '0,119,120', '设备新增', 4, '', NULL, 'sys:energy:add', 1, 1, '', NULL, '2024-08-28 16:24:15', '2024-08-28 16:24:52', NULL, NULL);
INSERT INTO `sys_menu` VALUES (122, 120, '0,119,120', '设备编辑', 4, '', NULL, 'sys:energy:edit', 1, 2, '', NULL, '2024-08-28 16:25:18', '2024-08-28 16:25:52', NULL, NULL);
INSERT INTO `sys_menu` VALUES (123, 120, '0,119,120', '设备删除', 4, '', NULL, 'sys:energy:delete', 1, 3, '', NULL, '2024-08-28 16:25:45', '2024-08-28 16:25:45', NULL, NULL);
INSERT INTO `sys_menu` VALUES (124, 119, '0,119', '设备配置', 1, 'config', 'energy/config/index', NULL, 1, 1, 'document', NULL, '2024-08-30 00:38:14', '2024-08-30 00:46:59', NULL, 1);
INSERT INTO `sys_menu` VALUES (125, 0, '0', 'AI模型', 2, '/demoAI', 'Layout', NULL, 1, 1, 'api', '/demoAI/Qwen2', '2024-08-30 00:52:49', '2024-09-11 13:25:20', 1, NULL);
INSERT INTO `sys_menu` VALUES (126, 125, '0,125', 'Qwen2', 1, 'demoAI', 'demoAI/Qwen2/index', NULL, 1, 1, 'fullscreen-exit', NULL, '2024-08-30 00:53:48', '2024-08-30 00:53:48', NULL, 1);
INSERT INTO `sys_menu` VALUES (128, 0, '0', '学生列表', 2, '/student', 'Layout', NULL, 1, 1, 'user', '/student', '2024-08-31 14:34:58', '2024-08-31 14:34:58', 1, NULL);
INSERT INTO `sys_menu` VALUES (129, 128, '0,128', 'Student', 1, 'student', 'student/index', NULL, 1, 1, 'tree', NULL, '2024-08-31 14:35:35', '2024-08-31 14:35:35', NULL, 1);
INSERT INTO `sys_menu` VALUES (130, 0, '0', '账户列表', 2, '/account', 'Layout', NULL, 1, 1, 'api', '/account', '2024-09-02 11:30:08', '2024-09-02 11:30:28', 0, 0);
INSERT INTO `sys_menu` VALUES (131, 130, '0,130', '账户列表', 1, '/account', 'account/index', NULL, 1, 1, 'client', NULL, '2024-09-02 11:31:04', '2024-09-11 13:25:03', NULL, 1);
INSERT INTO `sys_menu` VALUES (132, 131, '0,130,131', '新增', 4, '', NULL, 'sys:account:add', 1, 1, '', NULL, '2024-09-02 20:31:27', '2024-09-02 20:31:44', NULL, NULL);
INSERT INTO `sys_menu` VALUES (133, 131, '0,130,131', '编辑', 4, '', NULL, 'sys:account:edit', 1, 1, '', NULL, '2024-09-02 20:32:10', '2024-09-02 20:32:10', NULL, NULL);
INSERT INTO `sys_menu` VALUES (134, 131, '0,130,131', '删除', 4, '', NULL, 'sys:account:delete', 1, 1, '', NULL, '2024-09-02 20:32:26', '2024-09-02 20:32:26', NULL, NULL);
INSERT INTO `sys_menu` VALUES (135, 0, '0', '系统曲线', 2, '/trend', 'Layout', NULL, 1, 1, 'system', '/trend', '2024-09-06 02:29:10', '2024-09-09 22:16:34', 1, NULL);
INSERT INTO `sys_menu` VALUES (136, 135, '0,135', '曲线配置', 1, 'trend', 'trend/config/index', NULL, 1, 1, 'client', NULL, '2024-09-06 02:29:52', '2024-09-09 22:17:08', NULL, 1);
INSERT INTO `sys_menu` VALUES (137, 136, '0,135,136', '删除', 4, '', NULL, 'sys:trend:delete', 1, 1, '', NULL, '2024-09-06 15:34:50', '2024-09-06 15:37:29', NULL, NULL);
INSERT INTO `sys_menu` VALUES (138, 136, '0,135,136', '添加', 4, '', NULL, 'sys:trend:add', 1, 1, '', NULL, '2024-09-06 15:36:14', '2024-09-06 15:36:14', NULL, NULL);
INSERT INTO `sys_menu` VALUES (139, 136, '0,135,136', '编辑', 4, '', NULL, 'sys:trend:edit', 1, 1, '', NULL, '2024-09-06 15:36:29', '2024-09-06 15:37:47', NULL, NULL);
INSERT INTO `sys_menu` VALUES (140, 0, '0', '参数下载', 2, '/parameter-config', 'Layout', NULL, 1, 1, 'download', '/parameter-config', '2024-09-06 17:36:57', '2024-09-06 17:41:44', 1, 0);
INSERT INTO `sys_menu` VALUES (141, 140, '0,140', '参数下载', 1, 'parameter-config', 'parameter-config/index', NULL, 1, 1, 'close_right', NULL, '2024-09-06 17:37:37', '2024-09-06 17:42:05', NULL, 1);
INSERT INTO `sys_menu` VALUES (142, 141, '0,140,141', '添加', 4, '', NULL, 'sys:parameter:add', 1, 1, '', NULL, '2024-09-06 17:37:56', '2024-09-06 17:37:56', NULL, NULL);
INSERT INTO `sys_menu` VALUES (143, 141, '0,140,141', '编辑', 4, '', NULL, 'sys:parameter:edit', 1, 1, '', NULL, '2024-09-06 17:38:11', '2024-09-06 17:38:11', NULL, NULL);
INSERT INTO `sys_menu` VALUES (144, 141, '0,140,141', '删除', 4, '', NULL, 'sys:parameter:delete', 1, 1, '', NULL, '2024-09-06 17:38:23', '2024-09-06 17:38:23', NULL, NULL);
INSERT INTO `sys_menu` VALUES (145, 0, '0', '设备配置', 2, '/device-config', 'Layout', NULL, 1, 1, 'cascader', '/device-config', '2024-09-06 19:51:14', '2024-09-06 19:51:14', 1, 0);
INSERT INTO `sys_menu` VALUES (146, 145, '0,145', 'PLC配置', 1, '/device-config', 'device-config/index', NULL, 1, 1, 'document', NULL, '2024-09-06 19:51:53', '2024-09-06 19:51:53', NULL, 1);
INSERT INTO `sys_menu` VALUES (147, 146, '0,145,146', '添加', 4, '', NULL, 'sys:device:add', 1, 1, '', NULL, '2024-09-06 19:52:23', '2024-09-06 19:52:23', NULL, NULL);
INSERT INTO `sys_menu` VALUES (148, 146, '0,145,146', '编辑', 4, '', NULL, 'sys:device:edit', 1, 1, '', NULL, '2024-09-06 19:52:36', '2024-09-06 19:52:36', NULL, NULL);
INSERT INTO `sys_menu` VALUES (149, 146, '0,145,146', '删除', 4, '', NULL, 'sys:device:delete', 1, 1, '', NULL, '2024-09-06 19:52:54', '2024-09-06 19:52:54', NULL, NULL);
INSERT INTO `sys_menu` VALUES (150, 146, '0,145,146', '操作：订阅', 4, '', NULL, 'sys:op:subscribe', 1, 1, '', NULL, '2024-09-08 11:13:08', '2024-09-08 11:13:08', 0, 0);
INSERT INTO `sys_menu` VALUES (151, 146, '0,145,146', '操作：连接', 4, '', NULL, 'sys:op:connect', 1, 1, '', NULL, '2024-09-08 11:13:30', '2024-09-08 11:13:30', NULL, NULL);
INSERT INTO `sys_menu` VALUES (152, 146, '0,145,146', '操作：取消订阅', 4, '', NULL, 'sys:op:cancel', 1, 1, '', NULL, '2024-09-08 11:13:56', '2024-09-08 11:13:56', NULL, NULL);
INSERT INTO `sys_menu` VALUES (153, 146, '0,145,146', '操作：断开', 4, '', NULL, 'sys:op:disconnect', 1, 1, '', NULL, '2024-09-08 11:14:17', '2024-09-08 11:14:17', NULL, NULL);
INSERT INTO `sys_menu` VALUES (154, 141, '0,140,141', '操作：下载', 4, '', NULL, 'sys:parameter:download', 1, 1, '', NULL, '2024-09-08 20:23:14', '2024-09-08 20:23:14', 0, 0);
INSERT INTO `sys_menu` VALUES (155, 141, '0,140,141', '操作：批量导入', 4, '', NULL, 'sys:parameter:addList', 1, 1, '', NULL, '2024-09-09 15:49:55', '2024-09-09 15:49:55', 0, 0);
INSERT INTO `sys_menu` VALUES (156, 146, '0,145,146', '操作：批量导入', 4, '', NULL, 'sys:device:addList', 1, 1, '', NULL, '2024-09-09 16:41:00', '2024-09-09 16:41:00', 0, 0);
INSERT INTO `sys_menu` VALUES (157, 136, '0,135,136', '操作：批量导入', 4, '', NULL, 'sys:trend:addList', 1, 1, '', NULL, '2024-09-09 16:41:32', '2024-09-09 16:41:32', NULL, NULL);
INSERT INTO `sys_menu` VALUES (158, 135, '0,135', '曲线记录', 1, 'trend/display', 'trend/display/index', NULL, 1, 1, 'eye-open', NULL, '2024-09-09 22:18:06', '2024-09-09 22:18:59', 0, 1);
INSERT INTO `sys_menu` VALUES (159, 135, '0,135', '数据分析', 1, 'trend/analyze', 'trend/analyze/index', NULL, 1, 1, 'table', NULL, '2024-09-12 13:12:38', '2024-09-12 13:12:38', 0, 1);
INSERT INTO `sys_menu` VALUES (160, 89, '0,89', '拖拽组件', 1, 'dnd', 'demo/dnd', NULL, 1, 1, 'captcha', NULL, '2024-09-14 12:01:22', '2024-09-16 13:32:12', NULL, 1);
INSERT INTO `sys_menu` VALUES (162, 0, '0', 'THREEJS', 2, '/Threejs', 'Layout', NULL, 1, 1, 'client', '', '2024-09-16 13:46:54', '2024-09-27 11:31:27', 1, 0);
INSERT INTO `sys_menu` VALUES (163, 162, '0,162', 'Demo1', 1, 'demo1', 'Threejs/demo1', NULL, 1, 1, 'api', NULL, '2024-09-16 13:47:30', '2024-09-16 13:56:57', NULL, 1);
INSERT INTO `sys_menu` VALUES (164, 162, '0,162', 'Demo2', 1, 'demo2', 'Threejs/demo2', NULL, 1, 1, 'table', NULL, '2024-09-16 13:57:57', '2024-09-16 13:57:57', NULL, NULL);
INSERT INTO `sys_menu` VALUES (165, 162, '0,162', 'Demo3', 1, 'demo3', 'Threejs/demo3', NULL, 1, 1, 'peoples', NULL, '2024-09-16 13:58:11', '2024-09-16 13:58:11', NULL, NULL);
INSERT INTO `sys_menu` VALUES (166, 0, '0', '事件管理', 2, '/event', 'Layout', NULL, 1, 1, 'dict', '/event', '2024-09-19 17:03:26', '2024-09-19 17:03:26', 1, NULL);
INSERT INTO `sys_menu` VALUES (167, 166, '0,166', '通用事件', 1, 'event', 'event/index', NULL, 1, 1, 'table', NULL, '2024-09-19 17:04:15', '2024-09-19 17:04:15', NULL, 1);
INSERT INTO `sys_menu` VALUES (168, 0, '0', '文本配置', 2, '/eventTextConfig', 'Layout', NULL, 1, 1, 'document', '/eventTextConfig', '2024-09-20 21:40:14', '2024-09-20 21:40:14', 1, NULL);
INSERT INTO `sys_menu` VALUES (169, 168, '0,168', '国际化文本配置', 1, 'eventTextConfig', 'eventTextConfig/index', NULL, 1, 1, 'size', NULL, '2024-09-20 21:40:51', '2024-09-20 21:40:51', NULL, 1);
INSERT INTO `sys_menu` VALUES (170, 169, '0,168,169', '操作：编辑', 4, '', NULL, 'sys:evenText:edit', 1, 1, '', NULL, '2024-09-20 21:41:51', '2024-09-21 09:54:31', NULL, NULL);
INSERT INTO `sys_menu` VALUES (171, 169, '0,168,169', '操作：删除', 4, '', NULL, 'sys:evenText:delete', 1, 1, '', NULL, '2024-09-20 21:42:07', '2024-09-20 21:42:07', NULL, NULL);
INSERT INTO `sys_menu` VALUES (172, 169, '0,168,169', '操作：批量添加', 4, '', NULL, 'sys:evenText:addList', 1, 1, '', NULL, '2024-09-20 21:42:28', '2024-09-20 21:42:28', NULL, NULL);
INSERT INTO `sys_menu` VALUES (173, 169, '0,168,169', '操作：添加', 4, '', NULL, 'sys:evenText:add', 1, 1, '', NULL, '2024-09-20 21:43:06', '2024-09-20 21:43:06', NULL, NULL);
INSERT INTO `sys_menu` VALUES (174, 0, '0', '离散量配置', 2, '/dmConfig', 'Layout', NULL, 1, 1, 'menu', '/dmConfig', '2024-09-21 09:51:06', '2024-09-21 09:51:06', 1, 0);
INSERT INTO `sys_menu` VALUES (175, 174, '0,174', '事件配置', 1, 'dmConfig', 'dmConfig/index', NULL, 1, 1, 'publish', NULL, '2024-09-21 09:51:49', '2024-09-21 09:51:49', NULL, 1);
INSERT INTO `sys_menu` VALUES (176, 175, '0,174,175', '操作：编辑', 4, '', NULL, 'sys:dmConfig:edit', 1, 1, '', NULL, '2024-09-21 09:53:13', '2024-09-21 09:53:44', 0, 0);
INSERT INTO `sys_menu` VALUES (177, 175, '0,174,175', '操作：添加', 4, '', NULL, 'sys:dmConfig:add', 1, 1, '', NULL, '2024-09-21 09:53:32', '2024-09-21 09:53:32', NULL, NULL);
INSERT INTO `sys_menu` VALUES (178, 175, '0,174,175', '操作：批量添加', 4, '', NULL, 'sys:dmConfig:addList', 1, 1, '', NULL, '2024-09-21 09:54:06', '2024-09-21 09:54:06', NULL, NULL);
INSERT INTO `sys_menu` VALUES (179, 175, '0,174,175', '操作：删除', 4, '', NULL, 'sys:dmConfig:delete', 1, 1, '', NULL, '2024-09-21 09:54:22', '2024-09-21 09:54:22', NULL, NULL);
INSERT INTO `sys_menu` VALUES (180, 36, '0,36', 'websocket', 1, 'websocket', 'demo/websocket', NULL, 1, 1, 'github', NULL, '2024-09-22 16:07:06', '2024-09-22 16:08:53', NULL, 1);
INSERT INTO `sys_menu` VALUES (181, 0, '0', 'demo', 2, '/demo', 'Layout', NULL, 1, 1, '', 'demo', '2024-09-22 16:09:26', '2024-09-22 16:09:26', 1, 0);
INSERT INTO `sys_menu` VALUES (182, 181, '0,181', 'websocket', 1, 'websocket', 'demo/websocket', NULL, 1, 1, '', NULL, '2024-09-22 16:09:41', '2024-09-22 16:09:41', NULL, 1);
INSERT INTO `sys_menu` VALUES (183, 0, '0', '数据大屏', 2, '/bigview', 'Layout', NULL, 1, 1, 'client', 'bigview', '2024-09-22 20:07:46', '2024-10-09 22:13:22', 1, 0);
INSERT INTO `sys_menu` VALUES (186, 183, '0,183', '大屏显示', 3, 'http://localhost:3000/#/bigview/index', NULL, NULL, 1, 1, 'fullscreen', NULL, '2024-09-25 15:27:31', '2024-09-25 15:47:30', NULL, NULL);
INSERT INTO `sys_menu` VALUES (187, 183, '0,183', '数据大屏', 1, 'index', 'bigview/index', NULL, 0, 1, 'close_other', NULL, '2024-09-25 15:29:26', '2024-09-25 15:47:56', 0, 1);
INSERT INTO `sys_menu` VALUES (188, 0, '0', 'Node-Red', 3, 'http://127.0.0.1:1880/', NULL, NULL, 1, 1, 'api', NULL, '2024-09-26 14:35:25', '2024-09-26 14:36:54', 0, 0);
INSERT INTO `sys_menu` VALUES (190, 0, '0', '设备列表', 2, '/device-list', 'Layout', NULL, 1, 1, 'cascader', '/device-list', '2024-10-15 20:28:52', '2024-10-15 20:28:52', 1, NULL);
INSERT INTO `sys_menu` VALUES (191, 190, '0,190', '设备列表', 1, '/device-list/index', 'device-list/index', NULL, 1, 1, 'document', NULL, '2024-10-15 20:29:35', '2024-10-15 20:29:35', NULL, 1);
INSERT INTO `sys_menu` VALUES (192, 0, '0', '时间程序', 2, '/timeprogram', 'Layout', NULL, 1, 1, 'edit', '/timeprogram', '2024-10-22 16:04:12', '2024-10-22 16:04:12', 1, NULL);
INSERT INTO `sys_menu` VALUES (193, 192, '0,192', '时间程序', 1, 'timeprogram', 'timeprogram/index', NULL, 1, 1, 'el-icon-Aim', NULL, '2024-10-22 16:05:00', '2024-10-22 16:05:00', NULL, 1);

-- ----------------------------
-- Table structure for sys_panel_description
-- ----------------------------
DROP TABLE IF EXISTS `sys_panel_description`;
CREATE TABLE `sys_panel_description`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `componentName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_panel_description
-- ----------------------------
INSERT INTO `sys_panel_description` VALUES (1, 'OnePanel', 'OnePanelImg', '参数面板1', 'OnePanel 是一个简洁的 Vue.js 组件，用于展示单个面板信息，包括标题、描述和标签。采用 Flexbox 布局，支持标签自动换行，适用于产品列表或文章摘要展示。', 'OnePanel');

-- ----------------------------
-- Table structure for sys_parameter_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_parameter_config`;
CREATE TABLE `sys_parameter_config`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `device` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `parameter` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `enable` tinyint(1) NULL DEFAULT NULL,
  `unit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `time` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 217 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_parameter_config
-- ----------------------------
INSERT INTO `sys_parameter_config` VALUES (2, 'parSetting.par1', 'Int', 'L1MFB11', '-15', 1, '℃', '模板参数', '2024-10-22 14:47:26');
INSERT INTO `sys_parameter_config` VALUES (3, 'parSetting.par2', 'Real', 'L1MFB11', '9999999.69', 1, '℃', '模板参数', '2024-10-22 13:49:25');
INSERT INTO `sys_parameter_config` VALUES (4, 'parSetting.par3', 'Bool', 'L1MFB11', 'true', 1, '℃', '模板参数', '2024-10-22 13:36:32');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色编码',
  `sort` int NULL DEFAULT NULL COMMENT '显示顺序',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '角色状态(1-正常；0-停用)',
  `data_scope` tinyint NULL DEFAULT NULL COMMENT '数据权限(0-所有数据；1-部门及子部门数据；2-本部门数据；3-本人数据)',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除标识(0-未删除；1-已删除)',
  `create_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 128 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'ROOT', 1, 1, 1, 0, '2021-05-21 14:56:51', '2018-12-23 16:00:00');
INSERT INTO `sys_role` VALUES (2, '系统管理员', 'ADMIN', 1, 1, 1, 0, '2021-03-25 12:39:54', '2024-09-02 20:39:00');
INSERT INTO `sys_role` VALUES (3, '访问游客', 'GUEST', 3, 1, 2, 0, '2021-05-26 15:49:05', '2019-05-05 16:00:00');
INSERT INTO `sys_role` VALUES (4, 'SuperMaster', 'Allen', 2, 1, 0, 0, '2021-03-25 12:39:54', '2024-09-20 10:15:27');
INSERT INTO `sys_role` VALUES (5, '系统管理员2', 'ADMIN1', 2, 1, 1, 1, '2021-03-25 12:39:54', '2024-08-30 00:54:49');
INSERT INTO `sys_role` VALUES (6, '系统管理员3', 'ADMIN1', 2, 1, 1, 1, '2021-03-25 12:39:54', '2024-08-30 00:55:05');
INSERT INTO `sys_role` VALUES (7, '系统管理员4', 'ADMIN1', 2, 1, 1, 1, '2021-03-25 12:39:54', '2024-08-30 00:54:50');
INSERT INTO `sys_role` VALUES (8, '系统管理员5', 'ADMIN1', 2, 1, 1, 1, '2021-03-25 12:39:54', '2024-08-30 00:55:03');
INSERT INTO `sys_role` VALUES (9, '系统管理员6', 'ADMIN1', 2, 1, 1, 1, '2021-03-25 12:39:54', '2024-08-30 00:54:57');
INSERT INTO `sys_role` VALUES (10, '系统管理员7', 'ADMIN1', 2, 1, 1, 1, '2021-03-25 12:39:54', '2024-08-30 00:54:55');
INSERT INTO `sys_role` VALUES (11, '系统管理员8', 'ADMIN1', 2, 1, 1, 1, '2021-03-25 12:39:54', '2024-08-30 00:54:53');
INSERT INTO `sys_role` VALUES (12, '系统管理员9', 'ADMIN1', 2, 1, 1, 1, '2021-03-25 12:39:54', '2024-08-30 00:54:52');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (4, 1);
INSERT INTO `sys_role_menu` VALUES (4, 2);
INSERT INTO `sys_role_menu` VALUES (4, 31);
INSERT INTO `sys_role_menu` VALUES (4, 32);
INSERT INTO `sys_role_menu` VALUES (4, 33);
INSERT INTO `sys_role_menu` VALUES (4, 88);
INSERT INTO `sys_role_menu` VALUES (4, 3);
INSERT INTO `sys_role_menu` VALUES (4, 70);
INSERT INTO `sys_role_menu` VALUES (4, 71);
INSERT INTO `sys_role_menu` VALUES (4, 72);
INSERT INTO `sys_role_menu` VALUES (4, 4);
INSERT INTO `sys_role_menu` VALUES (4, 73);
INSERT INTO `sys_role_menu` VALUES (4, 75);
INSERT INTO `sys_role_menu` VALUES (4, 74);
INSERT INTO `sys_role_menu` VALUES (4, 5);
INSERT INTO `sys_role_menu` VALUES (4, 76);
INSERT INTO `sys_role_menu` VALUES (4, 77);
INSERT INTO `sys_role_menu` VALUES (4, 78);
INSERT INTO `sys_role_menu` VALUES (4, 6);
INSERT INTO `sys_role_menu` VALUES (4, 79);
INSERT INTO `sys_role_menu` VALUES (4, 81);
INSERT INTO `sys_role_menu` VALUES (4, 84);
INSERT INTO `sys_role_menu` VALUES (4, 85);
INSERT INTO `sys_role_menu` VALUES (4, 86);
INSERT INTO `sys_role_menu` VALUES (4, 87);
INSERT INTO `sys_role_menu` VALUES (4, 145);
INSERT INTO `sys_role_menu` VALUES (4, 146);
INSERT INTO `sys_role_menu` VALUES (4, 147);
INSERT INTO `sys_role_menu` VALUES (4, 148);
INSERT INTO `sys_role_menu` VALUES (4, 149);
INSERT INTO `sys_role_menu` VALUES (4, 150);
INSERT INTO `sys_role_menu` VALUES (4, 151);
INSERT INTO `sys_role_menu` VALUES (4, 152);
INSERT INTO `sys_role_menu` VALUES (4, 153);
INSERT INTO `sys_role_menu` VALUES (4, 156);
INSERT INTO `sys_role_menu` VALUES (4, 140);
INSERT INTO `sys_role_menu` VALUES (4, 141);
INSERT INTO `sys_role_menu` VALUES (4, 144);
INSERT INTO `sys_role_menu` VALUES (4, 143);
INSERT INTO `sys_role_menu` VALUES (4, 142);
INSERT INTO `sys_role_menu` VALUES (4, 154);
INSERT INTO `sys_role_menu` VALUES (4, 155);
INSERT INTO `sys_role_menu` VALUES (4, 119);
INSERT INTO `sys_role_menu` VALUES (4, 124);
INSERT INTO `sys_role_menu` VALUES (4, 120);
INSERT INTO `sys_role_menu` VALUES (4, 121);
INSERT INTO `sys_role_menu` VALUES (4, 122);
INSERT INTO `sys_role_menu` VALUES (4, 123);
INSERT INTO `sys_role_menu` VALUES (4, 135);
INSERT INTO `sys_role_menu` VALUES (4, 136);
INSERT INTO `sys_role_menu` VALUES (4, 139);
INSERT INTO `sys_role_menu` VALUES (4, 138);
INSERT INTO `sys_role_menu` VALUES (4, 137);
INSERT INTO `sys_role_menu` VALUES (4, 157);
INSERT INTO `sys_role_menu` VALUES (4, 158);
INSERT INTO `sys_role_menu` VALUES (4, 159);
INSERT INTO `sys_role_menu` VALUES (4, 128);
INSERT INTO `sys_role_menu` VALUES (4, 129);
INSERT INTO `sys_role_menu` VALUES (4, 130);
INSERT INTO `sys_role_menu` VALUES (4, 131);
INSERT INTO `sys_role_menu` VALUES (4, 134);
INSERT INTO `sys_role_menu` VALUES (4, 133);
INSERT INTO `sys_role_menu` VALUES (4, 132);
INSERT INTO `sys_role_menu` VALUES (4, 125);
INSERT INTO `sys_role_menu` VALUES (4, 126);
INSERT INTO `sys_role_menu` VALUES (4, 162);
INSERT INTO `sys_role_menu` VALUES (4, 163);
INSERT INTO `sys_role_menu` VALUES (4, 164);
INSERT INTO `sys_role_menu` VALUES (4, 165);
INSERT INTO `sys_role_menu` VALUES (4, 166);
INSERT INTO `sys_role_menu` VALUES (4, 167);
INSERT INTO `sys_role_menu` VALUES (4, 40);
INSERT INTO `sys_role_menu` VALUES (4, 41);
INSERT INTO `sys_role_menu` VALUES (4, 26);
INSERT INTO `sys_role_menu` VALUES (4, 102);
INSERT INTO `sys_role_menu` VALUES (4, 30);
INSERT INTO `sys_role_menu` VALUES (4, 20);
INSERT INTO `sys_role_menu` VALUES (4, 21);
INSERT INTO `sys_role_menu` VALUES (4, 22);
INSERT INTO `sys_role_menu` VALUES (4, 23);
INSERT INTO `sys_role_menu` VALUES (4, 24);
INSERT INTO `sys_role_menu` VALUES (4, 98);
INSERT INTO `sys_role_menu` VALUES (4, 99);
INSERT INTO `sys_role_menu` VALUES (4, 100);
INSERT INTO `sys_role_menu` VALUES (4, 101);
INSERT INTO `sys_role_menu` VALUES (4, 36);
INSERT INTO `sys_role_menu` VALUES (4, 37);
INSERT INTO `sys_role_menu` VALUES (4, 38);
INSERT INTO `sys_role_menu` VALUES (4, 39);
INSERT INTO `sys_role_menu` VALUES (4, 95);
INSERT INTO `sys_role_menu` VALUES (4, 93);
INSERT INTO `sys_role_menu` VALUES (4, 94);
INSERT INTO `sys_role_menu` VALUES (4, 89);
INSERT INTO `sys_role_menu` VALUES (4, 96);
INSERT INTO `sys_role_menu` VALUES (4, 160);
INSERT INTO `sys_role_menu` VALUES (4, 97);
INSERT INTO `sys_role_menu` VALUES (4, 90);
INSERT INTO `sys_role_menu` VALUES (4, 91);
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 31);
INSERT INTO `sys_role_menu` VALUES (2, 32);
INSERT INTO `sys_role_menu` VALUES (2, 33);
INSERT INTO `sys_role_menu` VALUES (2, 88);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 70);
INSERT INTO `sys_role_menu` VALUES (2, 71);
INSERT INTO `sys_role_menu` VALUES (2, 72);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 73);
INSERT INTO `sys_role_menu` VALUES (2, 74);
INSERT INTO `sys_role_menu` VALUES (2, 75);
INSERT INTO `sys_role_menu` VALUES (2, 5);
INSERT INTO `sys_role_menu` VALUES (2, 76);
INSERT INTO `sys_role_menu` VALUES (2, 77);
INSERT INTO `sys_role_menu` VALUES (2, 78);
INSERT INTO `sys_role_menu` VALUES (2, 6);
INSERT INTO `sys_role_menu` VALUES (2, 79);
INSERT INTO `sys_role_menu` VALUES (2, 81);
INSERT INTO `sys_role_menu` VALUES (2, 84);
INSERT INTO `sys_role_menu` VALUES (2, 85);
INSERT INTO `sys_role_menu` VALUES (2, 86);
INSERT INTO `sys_role_menu` VALUES (2, 87);
INSERT INTO `sys_role_menu` VALUES (2, 135);
INSERT INTO `sys_role_menu` VALUES (2, 136);
INSERT INTO `sys_role_menu` VALUES (2, 137);
INSERT INTO `sys_role_menu` VALUES (2, 138);
INSERT INTO `sys_role_menu` VALUES (2, 139);
INSERT INTO `sys_role_menu` VALUES (2, 157);
INSERT INTO `sys_role_menu` VALUES (2, 158);
INSERT INTO `sys_role_menu` VALUES (2, 159);
INSERT INTO `sys_role_menu` VALUES (2, 140);
INSERT INTO `sys_role_menu` VALUES (2, 141);
INSERT INTO `sys_role_menu` VALUES (2, 142);
INSERT INTO `sys_role_menu` VALUES (2, 143);
INSERT INTO `sys_role_menu` VALUES (2, 144);
INSERT INTO `sys_role_menu` VALUES (2, 154);
INSERT INTO `sys_role_menu` VALUES (2, 155);
INSERT INTO `sys_role_menu` VALUES (2, 145);
INSERT INTO `sys_role_menu` VALUES (2, 146);
INSERT INTO `sys_role_menu` VALUES (2, 147);
INSERT INTO `sys_role_menu` VALUES (2, 148);
INSERT INTO `sys_role_menu` VALUES (2, 149);
INSERT INTO `sys_role_menu` VALUES (2, 150);
INSERT INTO `sys_role_menu` VALUES (2, 151);
INSERT INTO `sys_role_menu` VALUES (2, 152);
INSERT INTO `sys_role_menu` VALUES (2, 153);
INSERT INTO `sys_role_menu` VALUES (2, 156);
INSERT INTO `sys_role_menu` VALUES (2, 166);
INSERT INTO `sys_role_menu` VALUES (2, 167);
INSERT INTO `sys_role_menu` VALUES (2, 168);
INSERT INTO `sys_role_menu` VALUES (2, 169);
INSERT INTO `sys_role_menu` VALUES (2, 170);
INSERT INTO `sys_role_menu` VALUES (2, 171);
INSERT INTO `sys_role_menu` VALUES (2, 172);
INSERT INTO `sys_role_menu` VALUES (2, 173);
INSERT INTO `sys_role_menu` VALUES (2, 174);
INSERT INTO `sys_role_menu` VALUES (2, 175);
INSERT INTO `sys_role_menu` VALUES (2, 176);
INSERT INTO `sys_role_menu` VALUES (2, 177);
INSERT INTO `sys_role_menu` VALUES (2, 178);
INSERT INTO `sys_role_menu` VALUES (2, 179);
INSERT INTO `sys_role_menu` VALUES (2, 183);
INSERT INTO `sys_role_menu` VALUES (2, 186);
INSERT INTO `sys_role_menu` VALUES (2, 187);
INSERT INTO `sys_role_menu` VALUES (2, 190);
INSERT INTO `sys_role_menu` VALUES (2, 191);
INSERT INTO `sys_role_menu` VALUES (2, 192);
INSERT INTO `sys_role_menu` VALUES (2, 193);
INSERT INTO `sys_role_menu` VALUES (2, 36);
INSERT INTO `sys_role_menu` VALUES (2, 37);

-- ----------------------------
-- Table structure for sys_scheduled
-- ----------------------------
DROP TABLE IF EXISTS `sys_scheduled`;
CREATE TABLE `sys_scheduled`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `job_name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '定时任务名称',
  `class_name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '类名',
  `method` varchar(250) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '方法名',
  `cron` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '定时任务表达式',
  `status` int NOT NULL COMMENT '启用标记',
  `limit_time` int NOT NULL COMMENT '限制次数 0=不限制',
  `temp1` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_user` bigint NOT NULL COMMENT '创建人',
  `update_user` bigint NOT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_class_name`(`class_name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '定时任务配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_scheduled
-- ----------------------------
INSERT INTO `sys_scheduled` VALUES (1, '文件复制任务', 'FileScheduledTask', 'taskFunction', '* * * * * ?', 1, 1, ' ', '2021-05-06 17:20:07', '2021-05-10 02:24:09', 1, 1);
INSERT INTO `sys_scheduled` VALUES (2, '读取MOPick', 'MoPickScheduledTask', 'taskFunction', '* * * * * ?', 1, 1, ' ', '2021-05-06 17:20:07', '2021-05-10 02:24:09', 1, 1);
INSERT INTO `sys_scheduled` VALUES (3, '读取传票', 'CitationScheduledTask', 'taskFunction', '* * * * * ?', 1, 1, ' ', '2021-05-06 17:20:07', '2021-05-10 02:24:09', 1, 1);
INSERT INTO `sys_scheduled` VALUES (4, '读取材质证明书', 'MaterialScheduledTask', 'taskFunction', '* 0/1 * * * ?', 1, 1, ' ', '2021-05-06 17:20:07', '2023-09-08 14:38:53', 1, 1);
INSERT INTO `sys_scheduled` VALUES (5, '读取手动阀自检记录表', 'CheckSheetScheduledTask', 'taskFunction', '* * * * * ?', 1, 1, ' ', '2021-05-06 17:20:07', '2021-05-10 02:24:09', 1, 1);
INSERT INTO `sys_scheduled` VALUES (6, '读取材料代码对照表', 'MaterialCodeScheduledTask', 'taskFunction', '* * * * * ?', 1, 1, ' ', '2021-05-06 17:20:07', '2021-05-10 02:24:09', 1, 1);
INSERT INTO `sys_scheduled` VALUES (7, '读取法兰标准缩写代码检索表', 'FlangeCodeScheduledTask', 'taskFunction', '* * * * * ?', 1, 1, ' ', '2021-05-06 17:20:07', '2021-05-10 02:24:09', 1, 1);
INSERT INTO `sys_scheduled` VALUES (8, '读取结构长度对照表', 'SizeCodeScheduledTask', 'taskFunction', '* * * * * ?', 1, 1, ' ', '2021-05-06 17:20:07', '2021-05-10 02:24:09', 1, 1);
INSERT INTO `sys_scheduled` VALUES (9, '生成检查证明书', 'ReportScheduledTask', 'taskFunction', '* 0/1 * * * ?', 1, 1, ' ', '2021-05-06 17:20:07', '2023-09-08 10:30:19', 1, 1);

-- ----------------------------
-- Table structure for sys_trend_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_trend_config`;
CREATE TABLE `sys_trend_config`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `device` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `parameter` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `enable` tinyint(1) NULL DEFAULT NULL,
  `unit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `L_parameter` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `L_textId` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `L_enable` tinyint(1) NULL DEFAULT NULL,
  `LL_parameter` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `LL_textId` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `LL_enable` tinyint(1) NULL DEFAULT NULL,
  `H_parameter` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `H_textId` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `H_enable` tinyint(1) NULL DEFAULT NULL,
  `HH_parameter` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `HH_textId` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `HH_enable` tinyint(1) NULL DEFAULT NULL,
  `time` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 301 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_trend_config
-- ----------------------------
INSERT INTO `sys_trend_config` VALUES (299, 'DBLine.Real1', 'Real', 'L1MFB11', '1', 1, '℃', '系统温度', '30', '电机启动', 1, '20', '温度低故障', 1, '70', '温度高故障', 1, '80', '温度低故障', 1, '2024-10-18 22:15:15');

-- ----------------------------
-- Table structure for sys_trend_logs
-- ----------------------------
DROP TABLE IF EXISTS `sys_trend_logs`;
CREATE TABLE `sys_trend_logs`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `device` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `unit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 192 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_trend_logs
-- ----------------------------
INSERT INTO `sys_trend_logs` VALUES (1, 'PLC_1', 'DBLine.Real1', 'Real', '2.8', '℃', '2024-10-15 21:33:15', '123');
INSERT INTO `sys_trend_logs` VALUES (2, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '63.0', '℃', '2024-10-15 21:33:15', '12222');
INSERT INTO `sys_trend_logs` VALUES (3, 'PLC_1', 'DBLine.Real1', 'Real', '85.6', '℃', '2024-10-15 21:33:16', '123');
INSERT INTO `sys_trend_logs` VALUES (4, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '3.1', '℃', '2024-10-15 21:33:16', '12222');
INSERT INTO `sys_trend_logs` VALUES (5, 'PLC_1', 'DBLine.Real1', 'Real', '13.1', '℃', '2024-10-15 21:33:17', '123');
INSERT INTO `sys_trend_logs` VALUES (6, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '6.2', '℃', '2024-10-15 21:33:17', '12222');
INSERT INTO `sys_trend_logs` VALUES (7, 'PLC_1', 'DBLine.Real1', 'Real', '47.2', '℃', '2024-10-15 21:33:18', '123');
INSERT INTO `sys_trend_logs` VALUES (8, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '59.4', '℃', '2024-10-15 21:33:18', '12222');
INSERT INTO `sys_trend_logs` VALUES (9, 'PLC_1', 'DBLine.Real1', 'Real', '88.0', '℃', '2024-10-15 21:33:19', '123');
INSERT INTO `sys_trend_logs` VALUES (10, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '76.4', '℃', '2024-10-15 21:33:19', '12222');
INSERT INTO `sys_trend_logs` VALUES (11, 'PLC_1', 'DBLine.Real1', 'Real', '78.9', '℃', '2024-10-15 21:33:20', '123');
INSERT INTO `sys_trend_logs` VALUES (12, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '68.2', '℃', '2024-10-15 21:33:20', '12222');
INSERT INTO `sys_trend_logs` VALUES (13, 'PLC_1', 'DBLine.Real1', 'Real', '30.8', '℃', '2024-10-15 21:33:21', '123');
INSERT INTO `sys_trend_logs` VALUES (14, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '67.3', '℃', '2024-10-15 21:33:21', '12222');
INSERT INTO `sys_trend_logs` VALUES (15, 'PLC_1', 'DBLine.Real1', 'Real', '95.1', '℃', '2024-10-15 21:33:22', '123');
INSERT INTO `sys_trend_logs` VALUES (16, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '56.0', '℃', '2024-10-15 21:33:22', '12222');
INSERT INTO `sys_trend_logs` VALUES (17, 'PLC_1', 'DBLine.Real1', 'Real', '40.2', '℃', '2024-10-15 21:33:23', '123');
INSERT INTO `sys_trend_logs` VALUES (18, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '87.9', '℃', '2024-10-15 21:33:23', '12222');
INSERT INTO `sys_trend_logs` VALUES (19, 'PLC_1', 'DBLine.Real1', 'Real', '24.1', '℃', '2024-10-15 21:33:24', '123');
INSERT INTO `sys_trend_logs` VALUES (20, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '39.0', '℃', '2024-10-15 21:33:24', '12222');
INSERT INTO `sys_trend_logs` VALUES (21, 'PLC_1', 'DBLine.Real1', 'Real', '61.6', '℃', '2024-10-15 21:33:25', '123');
INSERT INTO `sys_trend_logs` VALUES (22, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '11.8', '℃', '2024-10-15 21:33:25', '12222');
INSERT INTO `sys_trend_logs` VALUES (23, 'PLC_1', 'DBLine.Real1', 'Real', '1.5', '℃', '2024-10-15 21:33:26', '123');
INSERT INTO `sys_trend_logs` VALUES (24, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '72.3', '℃', '2024-10-15 21:33:26', '12222');
INSERT INTO `sys_trend_logs` VALUES (25, 'PLC_1', 'DBLine.Real1', 'Real', '95.0', '℃', '2024-10-15 21:33:27', '123');
INSERT INTO `sys_trend_logs` VALUES (26, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '9.8', '℃', '2024-10-15 21:33:27', '12222');
INSERT INTO `sys_trend_logs` VALUES (27, 'PLC_1', 'DBLine.Real1', 'Real', '93.4', '℃', '2024-10-15 21:33:28', '123');
INSERT INTO `sys_trend_logs` VALUES (28, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '57.3', '℃', '2024-10-15 21:33:28', '12222');
INSERT INTO `sys_trend_logs` VALUES (29, 'PLC_1', 'DBLine.Real1', 'Real', '79.6', '℃', '2024-10-15 21:33:29', '123');
INSERT INTO `sys_trend_logs` VALUES (30, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '60.1', '℃', '2024-10-15 21:33:29', '12222');
INSERT INTO `sys_trend_logs` VALUES (31, 'PLC_1', 'DBLine.Real1', 'Real', '53.6', '℃', '2024-10-15 21:33:30', '123');
INSERT INTO `sys_trend_logs` VALUES (32, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '43.0', '℃', '2024-10-15 21:33:30', '12222');
INSERT INTO `sys_trend_logs` VALUES (33, 'PLC_1', 'DBLine.Real1', 'Real', '14.3', '℃', '2024-10-15 21:33:31', '123');
INSERT INTO `sys_trend_logs` VALUES (34, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '82.8', '℃', '2024-10-15 21:33:31', '12222');
INSERT INTO `sys_trend_logs` VALUES (35, 'PLC_1', 'DBLine.Real1', 'Real', '78.5', '℃', '2024-10-15 21:33:32', '123');
INSERT INTO `sys_trend_logs` VALUES (36, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '74.1', '℃', '2024-10-15 21:33:32', '12222');
INSERT INTO `sys_trend_logs` VALUES (37, 'PLC_1', 'DBLine.Real1', 'Real', '86.7', '℃', '2024-10-15 21:33:33', '123');
INSERT INTO `sys_trend_logs` VALUES (38, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '32.0', '℃', '2024-10-15 21:33:33', '12222');
INSERT INTO `sys_trend_logs` VALUES (39, 'PLC_1', 'DBLine.Real1', 'Real', '6.7', '℃', '2024-10-15 21:33:34', '123');
INSERT INTO `sys_trend_logs` VALUES (40, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '46.6', '℃', '2024-10-15 21:33:34', '12222');
INSERT INTO `sys_trend_logs` VALUES (41, 'PLC_1', 'DBLine.Real1', 'Real', '37.1', '℃', '2024-10-15 21:33:35', '123');
INSERT INTO `sys_trend_logs` VALUES (42, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '67.8', '℃', '2024-10-15 21:33:35', '12222');
INSERT INTO `sys_trend_logs` VALUES (43, 'PLC_1', 'DBLine.Real1', 'Real', '33.0', '℃', '2024-10-15 21:33:36', '123');
INSERT INTO `sys_trend_logs` VALUES (44, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '69.4', '℃', '2024-10-15 21:33:36', '12222');
INSERT INTO `sys_trend_logs` VALUES (45, 'PLC_1', 'DBLine.Real1', 'Real', '54.6', '℃', '2024-10-15 21:33:38', '123');
INSERT INTO `sys_trend_logs` VALUES (46, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '87.2', '℃', '2024-10-15 21:33:38', '12222');
INSERT INTO `sys_trend_logs` VALUES (47, 'PLC_1', 'DBLine.Real1', 'Real', '23.1', '℃', '2024-10-15 21:33:39', '123');
INSERT INTO `sys_trend_logs` VALUES (48, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '55.1', '℃', '2024-10-15 21:33:39', '12222');
INSERT INTO `sys_trend_logs` VALUES (49, 'PLC_1', 'DBLine.Real1', 'Real', '21.4', '℃', '2024-10-15 21:33:40', '123');
INSERT INTO `sys_trend_logs` VALUES (50, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '13.3', '℃', '2024-10-15 21:33:40', '12222');
INSERT INTO `sys_trend_logs` VALUES (51, 'PLC_1', 'DBLine.Real1', 'Real', '4.5', '℃', '2024-10-15 21:33:41', '123');
INSERT INTO `sys_trend_logs` VALUES (52, 'PLC_1', 'DBLine.Real1', 'Real', '89.3', '℃', '2024-10-15 21:33:42', '123');
INSERT INTO `sys_trend_logs` VALUES (53, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '65.6', '℃', '2024-10-15 21:33:42', '12222');
INSERT INTO `sys_trend_logs` VALUES (54, 'PLC_1', 'DBLine.Real1', 'Real', '73.1', '℃', '2024-10-15 21:33:43', '123');
INSERT INTO `sys_trend_logs` VALUES (55, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '35.0', '℃', '2024-10-15 21:33:43', '12222');
INSERT INTO `sys_trend_logs` VALUES (56, 'PLC_1', 'DBLine.Real1', 'Real', '74.2', '℃', '2024-10-15 21:33:44', '123');
INSERT INTO `sys_trend_logs` VALUES (57, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '4.8', '℃', '2024-10-15 21:33:44', '12222');
INSERT INTO `sys_trend_logs` VALUES (58, 'PLC_1', 'DBLine.Real1', 'Real', '51.3', '℃', '2024-10-15 21:33:45', '123');
INSERT INTO `sys_trend_logs` VALUES (59, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '1.2', '℃', '2024-10-15 21:33:45', '12222');
INSERT INTO `sys_trend_logs` VALUES (60, 'PLC_1', 'DBLine.Real1', 'Real', '96.9', '℃', '2024-10-15 21:33:46', '123');
INSERT INTO `sys_trend_logs` VALUES (61, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '29.9', '℃', '2024-10-15 21:33:46', '12222');
INSERT INTO `sys_trend_logs` VALUES (62, 'PLC_1', 'DBLine.Real1', 'Real', '32.0', '℃', '2024-10-15 21:33:47', '123');
INSERT INTO `sys_trend_logs` VALUES (63, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '13.0', '℃', '2024-10-15 21:33:47', '12222');
INSERT INTO `sys_trend_logs` VALUES (64, 'PLC_1', 'DBLine.Real1', 'Real', '94.5', '℃', '2024-10-15 21:33:48', '123');
INSERT INTO `sys_trend_logs` VALUES (65, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '43.1', '℃', '2024-10-15 21:33:48', '12222');
INSERT INTO `sys_trend_logs` VALUES (66, 'PLC_1', 'DBLine.Real1', 'Real', '7.3', '℃', '2024-10-15 21:33:49', '123');
INSERT INTO `sys_trend_logs` VALUES (67, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '21.8', '℃', '2024-10-15 21:33:49', '12222');
INSERT INTO `sys_trend_logs` VALUES (68, 'PLC_1', 'DBLine.Real1', 'Real', '22.8', '℃', '2024-10-15 21:33:50', '123');
INSERT INTO `sys_trend_logs` VALUES (69, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '76.0', '℃', '2024-10-15 21:33:50', '12222');
INSERT INTO `sys_trend_logs` VALUES (70, 'PLC_1', 'DBLine.Real1', 'Real', '64.6', '℃', '2024-10-15 21:33:51', '123');
INSERT INTO `sys_trend_logs` VALUES (71, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '93.4', '℃', '2024-10-15 21:33:51', '12222');
INSERT INTO `sys_trend_logs` VALUES (72, 'PLC_1', 'DBLine.Real1', 'Real', '91.2', '℃', '2024-10-15 21:33:52', '123');
INSERT INTO `sys_trend_logs` VALUES (73, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '73.0', '℃', '2024-10-15 21:33:52', '12222');
INSERT INTO `sys_trend_logs` VALUES (74, 'PLC_1', 'DBLine.Real1', 'Real', '44.3', '℃', '2024-10-15 21:33:53', '123');
INSERT INTO `sys_trend_logs` VALUES (75, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '33.5', '℃', '2024-10-15 21:33:53', '12222');
INSERT INTO `sys_trend_logs` VALUES (76, 'PLC_1', 'DBLine.Real1', 'Real', '43.3', '℃', '2024-10-15 21:33:54', '123');
INSERT INTO `sys_trend_logs` VALUES (77, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '51.7', '℃', '2024-10-15 21:33:54', '12222');
INSERT INTO `sys_trend_logs` VALUES (78, 'PLC_1', 'DBLine.Real1', 'Real', '39.0', '℃', '2024-10-15 21:33:55', '123');
INSERT INTO `sys_trend_logs` VALUES (79, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '90.8', '℃', '2024-10-15 21:33:55', '12222');
INSERT INTO `sys_trend_logs` VALUES (80, 'PLC_1', 'DBLine.Real1', 'Real', '42.0', '℃', '2024-10-15 21:33:56', '123');
INSERT INTO `sys_trend_logs` VALUES (81, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '8.6', '℃', '2024-10-15 21:33:56', '12222');
INSERT INTO `sys_trend_logs` VALUES (82, 'PLC_1', 'DBLine.Real1', 'Real', '66.0', '℃', '2024-10-15 21:33:57', '123');
INSERT INTO `sys_trend_logs` VALUES (83, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '99.8', '℃', '2024-10-15 21:33:57', '12222');
INSERT INTO `sys_trend_logs` VALUES (84, 'PLC_1', 'DBLine.Real1', 'Real', '19.5', '℃', '2024-10-15 21:33:58', '123');
INSERT INTO `sys_trend_logs` VALUES (85, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '56.4', '℃', '2024-10-15 21:33:58', '12222');
INSERT INTO `sys_trend_logs` VALUES (86, 'PLC_1', 'DBLine.Real1', 'Real', '30.0', '℃', '2024-10-15 21:33:59', '123');
INSERT INTO `sys_trend_logs` VALUES (87, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '7.6', '℃', '2024-10-15 21:33:59', '12222');
INSERT INTO `sys_trend_logs` VALUES (88, 'PLC_1', 'DBLine.Real1', 'Real', '76.9', '℃', '2024-10-15 21:34:00', '123');
INSERT INTO `sys_trend_logs` VALUES (89, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '40.7', '℃', '2024-10-15 21:34:00', '12222');
INSERT INTO `sys_trend_logs` VALUES (90, 'PLC_1', 'DBLine.Real1', 'Real', '63.8', '℃', '2024-10-15 21:34:01', '123');
INSERT INTO `sys_trend_logs` VALUES (91, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '85.1', '℃', '2024-10-15 21:34:02', '12222');
INSERT INTO `sys_trend_logs` VALUES (92, 'PLC_1', 'DBLine.Real1', 'Real', '27.8', '℃', '2024-10-15 21:34:02', '123');
INSERT INTO `sys_trend_logs` VALUES (93, 'PLC_1', 'DBLine.Real1', 'Real', '99.2', '℃', '2024-10-15 21:34:04', '123');
INSERT INTO `sys_trend_logs` VALUES (94, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '65.4', '℃', '2024-10-15 21:34:04', '12222');
INSERT INTO `sys_trend_logs` VALUES (95, 'PLC_1', 'DBLine.Real1', 'Real', '47.8', '℃', '2024-10-15 21:34:05', '123');
INSERT INTO `sys_trend_logs` VALUES (96, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '1.3', '℃', '2024-10-15 21:34:05', '12222');
INSERT INTO `sys_trend_logs` VALUES (97, 'PLC_1', 'DBLine.Real1', 'Real', '68.1', '℃', '2024-10-15 21:34:06', '123');
INSERT INTO `sys_trend_logs` VALUES (98, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '32.0', '℃', '2024-10-15 21:34:06', '12222');
INSERT INTO `sys_trend_logs` VALUES (99, 'PLC_1', 'DBLine.Real1', 'Real', '5.3', '℃', '2024-10-15 21:34:07', '123');
INSERT INTO `sys_trend_logs` VALUES (100, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '7.9', '℃', '2024-10-15 21:34:07', '12222');
INSERT INTO `sys_trend_logs` VALUES (101, 'PLC_1', 'DBLine.Real1', 'Real', '64.8', '℃', '2024-10-15 21:34:08', '123');
INSERT INTO `sys_trend_logs` VALUES (102, 'PLC_1', 'DBLine.Real1', 'Real', '40.2', '℃', '2024-10-15 21:34:09', '123');
INSERT INTO `sys_trend_logs` VALUES (103, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '16.3', '℃', '2024-10-15 21:34:09', '12222');
INSERT INTO `sys_trend_logs` VALUES (104, 'PLC_1', 'DBLine.Real1', 'Real', '35.1', '℃', '2024-10-15 21:34:10', '123');
INSERT INTO `sys_trend_logs` VALUES (105, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '65.0', '℃', '2024-10-15 21:34:10', '12222');
INSERT INTO `sys_trend_logs` VALUES (106, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '97.5', '℃', '2024-10-15 21:34:11', '12222');
INSERT INTO `sys_trend_logs` VALUES (107, 'PLC_1', 'DBLine.Real1', 'Real', '48.1', '℃', '2024-10-15 21:34:12', '123');
INSERT INTO `sys_trend_logs` VALUES (108, 'PLC_1', 'DBLine.ArrReal[10]', 'Real', '81.8', '℃', '2024-10-15 21:34:12', '12222');
INSERT INTO `sys_trend_logs` VALUES (109, 'L1MFB11', 'DBLine.Real1', 'Real', '22.0', '℃', '2024-10-15 21:54:42', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (110, 'L1MFB11', 'DBLine.Real1', 'Real', '26.7', '℃', '2024-10-15 21:54:42', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (111, 'L1MFB11', 'DBLine.Real1', 'Real', '48.9', '℃', '2024-10-15 21:54:43', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (112, 'L1MFB11', 'DBLine.Real1', 'Real', '98.4', '℃', '2024-10-15 21:54:44', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (113, 'L1MFB11', 'DBLine.Real1', 'Real', '34.5', '℃', '2024-10-15 21:54:45', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (114, 'L1MFB11', 'DBLine.Real1', 'Real', '12.3', '℃', '2024-10-15 21:54:46', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (115, 'L1MFB11', 'DBLine.Real1', 'Real', '59.6', '℃', '2024-10-15 21:54:47', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (116, 'L1MFB11', 'DBLine.Real1', 'Real', '17.5', '℃', '2024-10-15 21:54:48', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (117, 'L1MFB11', 'DBLine.Real1', 'Real', '81.3', '℃', '2024-10-15 21:54:49', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (118, 'L1MFB11', 'DBLine.Real1', 'Real', '95.5', '℃', '2024-10-15 21:54:50', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (119, 'L1MFB11', 'DBLine.Real1', 'Real', '67.1', '℃', '2024-10-15 21:54:51', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (120, 'L1MFB11', 'DBLine.Real1', 'Real', '92.2', '℃', '2024-10-15 21:54:52', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (121, 'L1MFB11', 'DBLine.Real1', 'Real', '95.8', '℃', '2024-10-15 21:54:53', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (122, 'L1MFB11', 'DBLine.Real1', 'Real', '53.5', '℃', '2024-10-15 21:54:55', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (123, 'L1MFB11', 'DBLine.Real1', 'Real', '98.8', '℃', '2024-10-15 21:54:56', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (124, 'L1MFB11', 'DBLine.Real1', 'Real', '13.9', '℃', '2024-10-15 21:54:57', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (125, 'L1MFB11', 'DBLine.Real1', 'Real', '77.4', '℃', '2024-10-15 21:54:58', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (126, 'L1MFB11', 'DBLine.Real1', 'Real', '46.6', '℃', '2024-10-15 21:54:59', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (127, 'L1MFB11', 'DBLine.Real1', 'Real', '16.0', '℃', '2024-10-15 21:55:00', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (128, 'L1MFB11', 'DBLine.Real1', 'Real', '99.4', '℃', '2024-10-15 21:55:01', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (129, 'L1MFB11', 'DBLine.Real1', 'Real', '44.9', '℃', '2024-10-15 21:55:02', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (130, 'L1MFB11', 'DBLine.Real1', 'Real', '31.3', '℃', '2024-10-15 21:55:03', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (131, 'L1MFB11', 'DBLine.Real1', 'Real', '56.6', '℃', '2024-10-15 21:55:04', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (132, 'L1MFB11', 'DBLine.Real1', 'Real', '2.4', '℃', '2024-10-15 21:55:05', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (133, 'L1MFB11', 'DBLine.Real1', 'Real', '6.4', '℃', '2024-10-15 21:55:06', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (134, 'L1MFB11', 'DBLine.Real1', 'Real', '22.2', '℃', '2024-10-15 21:55:07', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (135, 'L1MFB11', 'DBLine.Real1', 'Real', '84.7', '℃', '2024-10-15 21:55:08', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (136, 'L1MFB11', 'DBLine.Real1', 'Real', '65.8', '℃', '2024-10-15 21:55:09', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (137, 'L1MFB11', 'DBLine.Real1', 'Real', '39.9', '℃', '2024-10-15 21:55:10', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (138, 'L1MFB11', 'DBLine.Real1', 'Real', '90.6', '℃', '2024-10-15 21:55:11', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (139, 'L1MFB11', 'DBLine.Real1', 'Real', '53.5', '℃', '2024-10-15 21:55:12', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (140, 'L1MFB11', 'DBLine.Real1', 'Real', '79.8', '℃', '2024-10-15 21:55:13', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (141, 'L1MFB11', 'DBLine.Real1', 'Real', '93.1', '℃', '2024-10-15 21:55:14', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (142, 'L1MFB11', 'DBLine.Real1', 'Real', '99.5', '℃', '2024-10-15 21:55:15', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (143, 'L1MFB11', 'DBLine.Real1', 'Real', '51.5', '℃', '2024-10-15 21:55:16', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (144, 'L1MFB11', 'DBLine.Real1', 'Real', '72.9', '℃', '2024-10-15 21:55:17', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (145, 'L1MFB11', 'DBLine.Real1', 'Real', '81.2', '℃', '2024-10-15 21:55:18', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (146, 'L1MFB11', 'DBLine.Real1', 'Real', '7.1', '℃', '2024-10-15 21:55:19', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (147, 'L1MFB11', 'DBLine.Real1', 'Real', '80.5', '℃', '2024-10-15 21:55:21', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (148, 'L1MFB11', 'DBLine.Real1', 'Real', '55.7', '℃', '2024-10-15 21:55:22', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (149, 'L1MFB11', 'DBLine.Real1', 'Real', '22.7', '℃', '2024-10-15 21:55:23', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (150, 'L1MFB11', 'DBLine.Real1', 'Real', '3.2', '℃', '2024-10-15 21:55:24', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (151, 'L1MFB11', 'DBLine.Real1', 'Real', '94.0', '℃', '2024-10-15 21:55:25', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (152, 'L1MFB11', 'DBLine.Real1', 'Real', '72.7', '℃', '2024-10-15 21:55:26', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (153, 'L1MFB11', 'DBLine.Real1', 'Real', '6.0', '℃', '2024-10-15 21:55:27', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (154, 'L1MFB11', 'DBLine.Real1', 'Real', '18.7', '℃', '2024-10-15 21:55:28', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (155, 'L1MFB11', 'DBLine.Real1', 'Real', '51.9', '℃', '2024-10-15 21:55:29', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (156, 'L1MFB11', 'DBLine.Real1', 'Real', '90.6', '℃', '2024-10-15 21:55:30', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (157, 'L1MFB11', 'DBLine.Real1', 'Real', '80.7', '℃', '2024-10-15 21:55:31', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (158, 'L1MFB11', 'DBLine.Real1', 'Real', '56.0', '℃', '2024-10-15 21:55:32', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (159, 'L1MFB11', 'DBLine.Real1', 'Real', '99.9', '℃', '2024-10-15 21:55:33', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (160, 'L1MFB11', 'DBLine.Real1', 'Real', '82.8', '℃', '2024-10-15 21:55:34', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (161, 'L1MFB11', 'DBLine.Real1', 'Real', '52.5', '℃', '2024-10-15 21:55:36', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (162, 'L1MFB11', 'DBLine.Real1', 'Real', '63.7', '℃', '2024-10-15 21:55:37', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (163, 'L1MFB11', 'DBLine.Real1', 'Real', '60.4', '℃', '2024-10-15 21:55:38', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (164, 'L1MFB11', 'DBLine.Real1', 'Real', '49.7', '℃', '2024-10-15 21:55:39', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (165, 'L1MFB11', 'DBLine.Real1', 'Real', '27.3', '℃', '2024-10-15 21:55:40', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (166, 'L1MFB11', 'DBLine.Real1', 'Real', '98.9', '℃', '2024-10-15 21:55:41', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (167, 'L1MFB11', 'DBLine.Real1', 'Real', '93.7', '℃', '2024-10-15 21:55:42', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (168, 'L1MFB11', 'DBLine.Real1', 'Real', '55.1', '℃', '2024-10-15 21:55:43', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (169, 'L1MFB11', 'DBLine.Real1', 'Real', '39.0', '℃', '2024-10-15 21:55:44', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (170, 'L1MFB11', 'DBLine.Real1', 'Real', '88.5', '℃', '2024-10-15 21:55:46', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (171, 'L1MFB11', 'DBLine.Real1', 'Real', '89.0', '℃', '2024-10-15 21:55:47', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (172, 'L1MFB11', 'DBLine.Real1', 'Real', '92.4', '℃', '2024-10-15 21:55:48', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (173, 'L1MFB11', 'DBLine.Real1', 'Real', '73.0', '℃', '2024-10-15 21:55:49', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (174, 'L1MFB11', 'DBLine.Real1', 'Real', '25.0', '℃', '2024-10-15 21:55:50', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (175, 'L1MFB11', 'DBLine.Real1', 'Real', '68.7', '℃', '2024-10-15 21:55:51', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (176, 'L1MFB11', 'DBLine.Real1', 'Real', '99.3', '℃', '2024-10-15 21:55:52', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (177, 'L1MFB11', 'DBLine.Real1', 'Real', '83.6', '℃', '2024-10-15 21:55:53', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (178, 'L1MFB11', 'DBLine.Real1', 'Real', '36.9', '℃', '2024-10-15 21:55:54', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (179, 'L1MFB11', 'DBLine.Real1', 'Real', '8.8', '℃', '2024-10-15 21:55:55', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (180, 'L1MFB11', 'DBLine.Real1', 'Real', '9.6', '℃', '2024-10-15 21:55:56', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (181, 'L1MFB11', 'DBLine.Real1', 'Real', '96.2', '℃', '2024-10-15 21:55:57', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (182, 'L1MFB11', 'DBLine.Real1', 'Real', '97.3', '℃', '2024-10-15 21:55:58', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (183, 'L1MFB11', 'DBLine.Real1', 'Real', '73.8', '℃', '2024-10-15 21:55:59', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (184, 'L1MFB11', 'DBLine.Real1', 'Real', '82.4', '℃', '2024-10-15 21:56:00', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (185, 'L1MFB11', 'DBLine.Real1', 'Real', '79.2', '℃', '2024-10-15 21:56:01', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (186, 'L1MFB11', 'DBLine.Real1', 'Real', '79.0', '℃', '2024-10-15 21:56:02', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (187, 'L1MFB11', 'DBLine.Real1', 'Real', '30.1', '℃', '2024-10-15 21:56:03', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (188, 'L1MFB11', 'DBLine.Real1', 'Real', '90.3', '℃', '2024-10-15 21:56:04', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (189, 'L1MFB11', 'DBLine.Real1', 'Real', '72.0', '℃', '2024-10-15 21:56:05', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (190, 'L1MFB11', 'DBLine.Real1', 'Real', '39.8', '℃', '2024-10-15 21:56:06', '系统温度');
INSERT INTO `sys_trend_logs` VALUES (191, 'L1MFB11', 'DBLine.Real1', 'Real', '6.5', '℃', '2024-10-15 21:56:07', '系统温度');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `nickname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `gender` tinyint(1) NULL DEFAULT 1 COMMENT '性别((1:男;2:女))',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `dept_id` int NULL DEFAULT NULL COMMENT '部门ID',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户头像',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '用户状态((1:正常;0:禁用))',
  `email` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户邮箱',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标识(0:未删除;1:已删除)',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `login_name`(`username` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 301 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'root', '有来技术', 0, '$2a$10$xVWsNOhHrCxh5UbpCE7/HuJ.PAOKcYAqRxD2CO2nVnJS.IAXkr5aq', NULL, 'https://p0.ssl.qhimgs1.com/t01063b7a44d5a6f130.png', '17621590365', 1, 'youlaitech@163.com', 0, NULL, NULL);
INSERT INTO `sys_user` VALUES (2, 'admin', '系统管理员', 1, '$2a$10$xVWsNOhHrCxh5UbpCE7/HuJ.PAOKcYAqRxD2CO2nVnJS.IAXkr5aq', 1, 'https://p0.ssl.qhimgs1.com/t01063b7a44d5a6f130.png', '17621210366', 1, '', 0, '2019-10-10 13:41:22', '2022-07-31 12:39:30');
INSERT INTO `sys_user` VALUES (300, 'allen', '艾伦', 1, '$2a$10$5ejzHwsg6jFPcw.JrQXWfeaOJ0GZIS0smttBo6BuJ0n0zaF4YjXjC', 1, 'https://p0.ssl.qhimgs1.com/t01063b7a44d5a6f130.png', '15884198278', 1, '1598996466@qq.com', 0, '2024-08-29 14:53:22', '2024-09-20 10:16:15');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (3, 3);
INSERT INTO `sys_user_role` VALUES (287, 2);
INSERT INTO `sys_user_role` VALUES (288, 4);
INSERT INTO `sys_user_role` VALUES (300, 4);

SET FOREIGN_KEY_CHECKS = 1;
