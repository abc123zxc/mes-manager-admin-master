package com.youlai.system.model.query;

import com.youlai.system.common.base.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/*
账户分配查询实体
 */
@Data
public class AccountQuery extends BasePageQuery {

    @Schema(description = "关键字(用户名称)")
    private String keywords;

}
