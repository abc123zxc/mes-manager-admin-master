package com.youlai.system.model.query;

import com.youlai.system.common.base.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class DeviceConfigQuery extends BasePageQuery {
    @Schema(description = "PLC 名称")
    private String keywords;

    @Schema(description = "关键字 启用状态")
    private Boolean enable;
}
