package com.youlai.system.model.query;

import com.youlai.system.common.base.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class TaskConfigQuery extends BasePageQuery {

    @Schema(description = "名称")
    private String keywords;

    @Schema(description = "关键字 启用状态")
    private Boolean enable;
}
