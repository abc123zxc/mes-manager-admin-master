package com.youlai.system.model.query;

import com.youlai.system.common.base.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class EventTextQuery  extends BasePageQuery {

    @Schema(description = "关键字")
    private String keywords;

    /*
    1:信息 2：警号 3：故障
     */
    @Schema(description = "类型" )
    private Integer type;
}
