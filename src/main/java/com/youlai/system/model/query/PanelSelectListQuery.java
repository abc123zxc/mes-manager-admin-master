package com.youlai.system.model.query;

import lombok.Data;

@Data
public class PanelSelectListQuery {
    private String device;
    private String type;
}
