package com.youlai.system.model.query;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
/**
 * TrendAnalyzeQuery 类用于表示趋势分析查询的参数实体。
 * 这个类包含了进行趋势分析所需的基本属性。
 */
@Data
public class TrendAnalyzeQuery {

    /**
     * 唯一标识符，通常用作数据库记录的主键。
     * 如果这是新记录，则此字段可能为空。
     */
    private Long id;

    /**
     * 设备标识符，用于区分不同的设备或数据来源。
     * 必须提供设备标识符。
     */
    @NotNull(message = "设备标识符不能为空")
    @Size(min = 1, max = 255, message = "设备标识符长度必须介于1到255之间")
    private String device;

    /**
     * 地址信息，可以是设备的物理地址或者网络位置。
     * 地址信息是可选的。
     */
    @Size(max = 100, message = "地址信息长度不能超过100个字符")
    private String address;

    /**
     * 描述信息，用于提供关于查询对象的额外说明或备注。
     * 描述信息是可选的。
     */
    @Size(max = 200, message = "描述信息长度不能超过200个字符")
    private String description;
}