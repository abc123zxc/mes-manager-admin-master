package com.youlai.system.model.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class DataLogsQuery {
    @Schema(description = "所属设备")
    private Integer device_id;
    @Schema(description = "起始时间")
    private String startTime;
    @Schema(description = "结束时间")
    private String endTime;
}
