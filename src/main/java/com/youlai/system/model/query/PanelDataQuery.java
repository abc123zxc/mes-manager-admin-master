package com.youlai.system.model.query;

import lombok.Data;

@Data
public class PanelDataQuery {
    private String device;
    private String address;
}
