package com.youlai.system.model.query;


import com.youlai.system.common.base.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class EventDataQuery extends BasePageQuery {

    @Schema(description = "描述")
    private String keywords;

    @Schema(description = "类型")
    private Integer type;

    @Schema(description = "是否确认")
    private Boolean confirm;

    @Schema(description="创建时间-开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String startTime;

    @Schema(description="创建时间-结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String endTime;
}
