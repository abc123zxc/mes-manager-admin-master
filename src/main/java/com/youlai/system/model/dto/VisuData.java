package com.youlai.system.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "大屏数据")
@Data
public class VisuData {

    @Schema(description = "系列")
    private String series;

    @Schema(description = "类型")
    private String category;

    @Schema(description = "值")
    private String value;

}
