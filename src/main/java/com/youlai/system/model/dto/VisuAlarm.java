package com.youlai.system.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "大屏报警")
@Data
public class VisuAlarm {

    @Schema(description = "名称")
    private String name;

    @Schema(description = "类型")
    private String type;

    @Schema(description = "内容")
    private String message;

    @Schema(description = "时间")
    private String updateTime;

    @Schema(description = "持续时间")
    private String duration;

}
