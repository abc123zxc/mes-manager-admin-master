package com.youlai.system.model.vo;


import lombok.Data;

import java.util.List;

@Data
public class TrendBigViewVO {
    private List<Integer> data;
    private List<Integer> day;
    private Integer status;
}
