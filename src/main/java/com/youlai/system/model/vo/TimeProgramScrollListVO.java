package com.youlai.system.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;


@Data
@Schema(description = "时间程序 显示")
public class TimeProgramScrollListVO {

    private Long id;

    private Integer index;

    private String name;

    private String description;

    private String time;
}