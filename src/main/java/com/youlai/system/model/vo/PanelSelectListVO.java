package com.youlai.system.model.vo;

import lombok.Data;

@Data
public class PanelSelectListVO {
    private Long id;
    private String device;
    private String address;
    private String type;
    private Boolean enable;
    private String unit;
    private String description;
}
