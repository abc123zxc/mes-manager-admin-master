package com.youlai.system.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "PLC Parameter 分页对象")
@Data
public class DeviceConfigPageVO {

    @Schema(description = "节点ID")
    private Long id;
    @Schema(description = "IP")
    private String ip;
    @Schema(description = "端口")
    private Integer port;
    @Schema(description = "PLC名称")
    private String name;
    @Schema(description = "PLC 类型")
    private Integer type;
    @Schema(description = "通讯协议")
    private Integer protocol;
    @Schema(description = "连接状态")
    private Boolean connected;
    @Schema(description = "节点订阅状态")
    private Boolean subscribed;
    @Schema(description = "启用")
    private Boolean enable;
    @Schema(description = "时间")
    private String time;
    @Schema(description = "描述")
    private String description;
    @Schema(description = "位置1")
    private String x ;
    @Schema(description = "位置2")
    private String y;

}
