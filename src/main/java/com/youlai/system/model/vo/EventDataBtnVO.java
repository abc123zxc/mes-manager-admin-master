package com.youlai.system.model.vo;

import lombok.Data;

@Data
public class EventDataBtnVO {
    private Boolean blink_Message = true;
    private Long number_Message;
    private Boolean blink_Warning = true;
    private Long number_Warning;
    private Boolean blink_Fault = true;
    private Long number_Fault;
}
