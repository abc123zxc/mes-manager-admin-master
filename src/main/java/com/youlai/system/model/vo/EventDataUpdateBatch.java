package com.youlai.system.model.vo;

import lombok.Data;

@Data
public class EventDataUpdateBatch {

    private String ids;
    private String userName;
    private String time;
}
