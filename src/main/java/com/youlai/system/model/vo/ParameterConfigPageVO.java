package com.youlai.system.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "PLC Parameter 分页对象")
@Data
public class ParameterConfigPageVO {

    @Schema(description = "节点ID")
    private Long id;
    @Schema(description = "所属设备id")
    private String device;
    @Schema(description = "opc 地址")
    private String address;
    @Schema(description = "数据类型")
    private String type;
    @Schema(description = "是否启用")
    private Boolean enable;
    @Schema(description = "参数")
    private String parameter;
    @Schema(description = "单位")
    private String unit;
    @Schema(description = "更新时间")
    private String time;
    @Schema(description = "描述")
    private String description;
}
