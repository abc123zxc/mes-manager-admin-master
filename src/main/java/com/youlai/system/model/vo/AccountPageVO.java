package com.youlai.system.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "账户分页对象")
@Data
public class AccountPageVO {

    @Schema(description = "账户id")
    private Long id;

    @Schema(description = "用户名称")
    private String username;

    @Schema(description = "账户密码")
    private String password;

    @Schema(description = "url")
    private String url;

}
