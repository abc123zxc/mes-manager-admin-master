package com.youlai.system.model.vo;

import lombok.Data;

@Data
public class PanelDataVO {
    private Long id;
    private String device;
    private String address;
    private String value;
    private String unit;
    private String type;
    private String description;
}
