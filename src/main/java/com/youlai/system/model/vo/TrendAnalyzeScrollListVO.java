package com.youlai.system.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * TrendAnalyzeScrollListVO 类用于表示滚动列表中的数据分析条目。
 * 这些条目通常会在用户界面中以列表的形式展现给用户。
 */
@Data
@Schema(description = "数据分析列表")
public class TrendAnalyzeScrollListVO {

    /**
     * 唯一标识符，通常用作数据库记录的主键。
     */
    private Long id;

    /**
     * 设备标识符，用于区分不同的设备或数据来源。
     */
    private String device;

    /**
     * 地址信息，可以是设备的物理地址或者网络位置。
     */
    private String address;

    /**
     * 描述信息，用于提供关于查询对象的额外说明或备注。
     */
    private String description;
}