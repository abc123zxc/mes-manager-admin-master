package com.youlai.system.model.vo;


import lombok.Data;

import java.util.List;

@Data
public class BarChartDataVO {
    private List<Integer> data;
    private List<String> name;
}
