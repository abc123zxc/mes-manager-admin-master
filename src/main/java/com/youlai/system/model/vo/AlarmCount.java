package com.youlai.system.model.vo;


import lombok.Data;


/**
 * 用于表示报警计数的类。
 * 这个类通常用于统计不同类型的报警事件的数量，如事件、警告和故障。
 */
@Data
public class AlarmCount {

    /**
     * 事件报警的数量。
     * 例如，这可以是系统中发生的各种事件的数量。
     */
    private Integer event;

    /**
     * 警告报警的数量。
     * 例如，这可以是系统中发生的各种警告的数量。
     */
    private Integer warning;

    /**
     * 故障报警的数量。
     * 例如，这可以是系统中发生的各种故障的数量。
     */
    private Integer fault;
}