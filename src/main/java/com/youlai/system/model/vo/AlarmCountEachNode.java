package com.youlai.system.model.vo;

import lombok.Data;


/**
 * 用于表示每个节点的报警计数的类。
 * 这个类通常用于统计不同类型的报警事件的数量，并且这些统计数据是针对每个特定节点的。
 */
@Data
public class AlarmCountEachNode {

    /**
     * 节点标识符。
     * 例如，这可以是设备的唯一标识符、服务器名称或其他能够唯一标识节点的字符串。
     */
    private String node;

    /**
     * 事件报警的数量。
     * 例如，这可以是该节点中发生的各种事件的数量。
     */
    private Integer event;

    /**
     * 警告报警的数量。
     * 例如，这可以是该节点中发生的各种警告的数量。
     */
    private Integer warning;

    /**
     * 故障报警的数量。
     * 例如，这可以是该节点中发生的各种故障的数量。
     */
    private Integer fault;
}