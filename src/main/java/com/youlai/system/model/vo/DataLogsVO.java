package com.youlai.system.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "查询返回对象用于数据分析")
@Data
public class DataLogsVO {

    @Schema(description = "当前值")
    private Float value;

    @Schema(description = "时间")
    private String time;

}