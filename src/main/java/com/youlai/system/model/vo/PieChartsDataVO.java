package com.youlai.system.model.vo;

import lombok.Data;

@Data
public class PieChartsDataVO {
    private Integer event;
    private Integer warning;
    private Integer fault;
}
