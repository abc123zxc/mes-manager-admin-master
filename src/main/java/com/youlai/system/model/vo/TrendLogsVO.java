package com.youlai.system.model.vo;

import com.youlai.system.model.entity.TrendLogs;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Schema(description = "曲线调用")
@Data
public class TrendLogsVO {


    @Schema(description = "曲线")
    private TrendLogs trendLogs;

    @Schema(description = "记录")
    private List<TrendLogs> trendLogsList;
}
