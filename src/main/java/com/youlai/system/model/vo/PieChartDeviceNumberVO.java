package com.youlai.system.model.vo;


import lombok.Data;

@Data
public class PieChartDeviceNumberVO {
    private String name;
    private Integer value;
    private Integer ratio;

}
