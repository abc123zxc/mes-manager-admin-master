package com.youlai.system.model.vo;

import lombok.Data;

@Data
public class OPCWriteNode {

    private String nodeId;
    private String value;
    private Class<?> valueType;
}
