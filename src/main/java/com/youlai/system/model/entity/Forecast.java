package com.youlai.system.model.entity;


import lombok.Data;

import java.util.Date;


/**
 * Forecast实体类，用于表示预测数据。
 */
@Data
public class Forecast {

    /**
     * 预测值
     */
    private String value;

    /**
     * 时间戳
     */
    private String time;
}