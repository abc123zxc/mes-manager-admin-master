package com.youlai.system.model.entity;

import cn.hutool.json.JSONObject;
import lombok.Data;

@Data
public class AnalyzeData {


    public AnalyzeData(int count, int min, int max, int sum, double mean, double median, int mode, int range, double variance, double stdDeviation) {
        this.count = count;
        this.min = min;
        this.max = max;
        this.sum = sum;
        this.mean = mean;
        this.median = median;
        this.mode = mode;
        this.range = range;
        this.variance = variance;
        this.stdDeviation = stdDeviation;
    }

    public AnalyzeData() {

    }

    @Override
    public String toString() {
        JSONObject result = new JSONObject();
        result.put("count", count);
        result.put("min", min);
        result.put("max", max);
        result.put("sum", sum);
        result.put("mean", mean);
        result.put("median", median);
        result.put("mode", mode);
        result.put("range", range);
        result.put("variance", variance);
        result.put("std_deviation", stdDeviation);

        return result.toString();
    }

    // 计数
    private int count;
    // 最小值和最大值
    private int min;
    private int max;
    // 总和
    private int sum;
    // 平均值
    private double mean;
    // 中位数
    private double median;
    // 众数
    private int mode;
    // 范围
    private int range;
    // 方差
    private double variance;
    // 标准差
    private double stdDeviation;


}
