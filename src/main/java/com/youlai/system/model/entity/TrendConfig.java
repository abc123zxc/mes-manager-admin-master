package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youlai.system.common.util.CommUtils;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * TrendConfig实体类，用于配置趋势数据。
 */
@Data
@TableName("sys_trend_config")
public class TrendConfig {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 设备标识
     */
    @NotNull(message = "设备标识不能为空")
    private String device;

    /**
     * 地址
     */
    @NotNull(message = "地址不能为空")
    private String address;

    /**
     * 类型
     */
    @NotNull(message = "类型不能为空")
    @Size(max = 50, message = "类型长度不能超过50个字符")
    private String type;

    /**
     * 参数名称
     */
    @NotNull(message = "参数名称不能为空")
    @Size(max = 50, message = "参数名称长度不能超过50个字符")
    private String parameter;

    /**
     * 启用状态
     */
    @NotNull(message = "启用状态不能为空")
    private Boolean enable;

    /**
     * 单位
     */
    @Size(max = 20, message = "单位长度不能超过20个字符")
    private String unit;

    /**
     * 创建时间
     */
    private String time = CommUtils.getCurrentDateTime();

    /**
     * 描述
     */
    @Size(max = 100, message = "描述长度不能超过100个字符")
    private String description;

    @TableField("L_parameter")
    private String LParameter = "0";

    @TableField("L_textId")
    private String LTextId = "E00_1000";;

    @TableField("L_enable")
    private Boolean LEnable = false ;


    @TableField("LL_parameter")
    private String LLParameter = "0";

    @TableField("LL_textId")
    private String LLTextId = "E00_1000";;

    @TableField("LL_enable")
    private Boolean LLEnable = false ;

    @TableField("H_parameter")
    private String HParameter = "0";

    @TableField("H_textId")
    private String HTextId = "E00_1000";;

    @TableField("H_enable")
    private Boolean HEnable  = false ;


    @TableField("HH_parameter")
    private String HHParameter = "0";

    @TableField("HH_textId")
    private String HHTextId = "E00_1000";

    @TableField("HH_enable")
    private Boolean HHEnable = false;

}