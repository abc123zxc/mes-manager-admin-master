package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * DataLogs实体类，用于记录设备的数据日志。
 */
@Data
@TableName("data_logs")
public class DataLogs {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 设备ID
     */
    @NotNull(message = "设备ID不能为空")
    private Integer device_id;

    /**
     * 数据值
     */
    @NotNull(message = "数据值不能为空")
    private String value;

    /**
     * 时间戳
     */
    @NotNull(message = "时间戳不能为空")
    private String time;

    /**
     * 属性名
     */
    @NotNull(message = "属性名不能为空")
    private String property;

    /**
     * 描述
     */
    private String description;
}
