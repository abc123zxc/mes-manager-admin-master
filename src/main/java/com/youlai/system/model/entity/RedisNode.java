package com.youlai.system.model.entity;

import jakarta.validation.Valid;
import lombok.Data;

/**
 * RedisNode实体类，用于表示Redis节点的状态。
 */
@Data
public class RedisNode {

    /**
     * 上一个值，默认为"0"
     */
    private String previousValue = "0";

    /**
     * 当前值，默认为"0"
     */
    private String currentValue = "0";

    /**
     * 趋势配置
     */
    @Valid
    private TrendConfig trendConfig;


    /**
     * TrendConfig.java
     *     private Long id;
     *     private String device;
     *     private String address;
     *     private String type;
     *     private String parameter;
     *     private Boolean enable;
     *     private String unit;
     *     private String time;
     *     private String description;
     */
}
