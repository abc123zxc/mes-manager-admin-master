package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
    * AIStream实体类，用于表示AI交互的记录。
    */
@Data
@TableName("AIStream")
public class AIStream {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 问题文本
     */
    @NotNull(message = "问题文本不能为空")
    private String question;

    /**
     * 回答文本
     */
    @NotNull(message = "回答文本不能为空")
    private String answer;

    /**
     * 附加数据
     */
    private String data;
}