package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName sys_alarm_statis
 */
@TableName(value ="sys_alarm_statis")
@Data
public class SysAlarmStatis  {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    private Integer event;

    /**
     * 
     */
    private Integer warning;

    /**
     * 
     */
    private Integer fault;

    /**
     * 
     */
    private String time;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}