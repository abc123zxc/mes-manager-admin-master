package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youlai.system.common.base.BaseEntity;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * Student实体类，用于表示学生信息。
 */
@Data
@TableName("Student")
public class Student extends BaseEntity {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 姓名
     */
    @NotNull(message = "姓名不能为空")
    @Size(max = 50, message = "姓名长度不能超过50个字符")
    private String name;

    /**
     * 地址
     */
    @NotNull(message = "地址不能为空")
    @Size(max = 100, message = "地址长度不能超过100个字符")
    private String address;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 性别（true为男性，false为女性）
     */
    private Boolean sex;
}