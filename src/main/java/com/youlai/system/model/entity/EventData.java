package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.youlai.system.common.util.CommUtils;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@TableName("sys_event")
public class EventData {
    /**
     * 事件ID
     */
    private Long id;

    /**
     * 事件类型
     */
    @NotNull(message = "类型不能为空")
    @Size(max = 50, message = "类型长度不能超过50个字符")
    private String type;

    /**
     * 事件组别
     */
    @NotNull(message = "组别不能为空")
    @Size(max = 50, message = "组别长度不能超过50个字符")
    private String deviceGroup;

    /**
     * 事件名称
     */
    @NotNull(message = "名称不能为空")
    @Size(max = 100, message = "名称长度不能超过100个字符")
    private String name;

    /**
     * 事件地址
     */
    @NotNull(message = "地址不能为空")
    @Size(max = 255, message = "地址长度不能超过255个字符")
    private String address;

    /**
     * 设备code
     */
    @NotNull(message = "地址不能为空")
    @Size(max = 255, message = "地址长度不能超过255个字符")
    private String code;


    /**
     * 消息类型（1: 一般信息, 2: 警告信息, 3: 故障信息）
     */
    @NotNull(message = "消息类型不能为空")
    private Integer status = 0;

    /**
     * 确认状态（true: 已确认, false: 未确认）
     */
    @NotNull(message = "确认状态不能为空")
    private Boolean confirm = false;

    /**
     * 事件时间
     */
    @NotNull(message = "时间不能为空")
    private String time = CommUtils.getCurrentDateTime();

    /**
     * 事件描述
     */
    @NotNull(message = "描述不能为空")
    @Size(max = 1000, message = "描述长度不能超过1000个字符")
    private String description;

    /**
     * 确认用户
     */
    @Size(max = 50, message = "确认用户长度不能超过50个字符")
    private String confirmUser;

    /**
     * 确认时间
     */
    private String confirmTime;
}
