package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youlai.system.common.util.CommUtils;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * 时间程序实体类
 */
@TableName(value = "sys_time_program")
@Data
public class TimeProgram {

    /**
     * 主键ID，自增长
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    @Size(max = 50, message = "名称长度不能超过50个字符")
    private String name;

    /**
     * 描述
     */
    @Size(max = 500, message = "描述长度不能超过500个字符")
    private String description;

    /**
     * 步骤
     */
    @NotNull(message = "步骤不能为空")
    private Integer step;

    /**
     * 索引
     */
    @TableField("`index`")
    @NotNull(message = "索引不能为空")
    private Integer index;

    /**
     * Y轴数据
     */
    @Size(max = 1000, message = "Y轴数据长度不能超过1000个字符")
    @NotNull(message = "Y轴数据不能为空")
    private String y;

    /**
     * X轴数据
     */
    @Size(max = 1000, message = "X轴数据长度不能超过1000个字符")
    @NotNull(message = "X轴数据不能为空")
    private String x;

    /**
     * 数据内容
     */
    @Size(max = 10000, message = "数据内容长度不能超过10000个字符")
    private String data;

    /**
     * 时间，默认值为当前日期时间
     */
    private String time = CommUtils.getCurrentDateTime();

    /**
     * 序列化版本UID，用于序列化时保证兼容性
     */
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}