package com.youlai.system.model.entity;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * ParameterPanel实体类，用于表示参数面板上的配置项。
 */
@Data
public class ParameterPanel {

    /**
     * 设备标识
     */
    @NotNull(message = "设备标识不能为空")
    private String device;

    /**
     * 值
     */
    private String value;

    /**
     * 启用状态
     */
    @NotNull(message = "启用状态不能为空")
    private Boolean enable;

    /**
     * 单位
     */
    @Size(max = 20, message = "单位长度不能超过20个字符")
    private String unit;

    /**
     * 描述
     */
    @Size(max = 100, message = "描述长度不能超过100个字符")
    private String description;
}