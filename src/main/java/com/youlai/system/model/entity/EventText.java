package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youlai.system.common.util.CommUtils;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
@TableName("sys_event_text_config")
public class EventText {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 文本ID
     */
    @NotBlank(message = "文本ID不能为空")
    private String textId;

    /**
     * 中文文本
     */
    @NotBlank(message = "中文文本不能为空")
    private String zhCn;

    /**
     * 英文文本
     */
    @NotBlank(message = "英文文本不能为空")
    private String enUs;

    /**
     * 德文文本
     */
    @NotBlank(message = "德文文本不能为空")
    private String deDe;

    /**
     * 韩文文本
     */
    @NotBlank(message = "韩文文本不能为空")
    private String koKr;

    /**
     * 创建时间
     */
    private String time = CommUtils.getCurrentDateTime();
}