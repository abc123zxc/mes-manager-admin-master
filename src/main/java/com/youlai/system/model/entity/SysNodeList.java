package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youlai.system.common.util.CommUtils;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName sys_node_list
 */
@TableName(value ="sys_node_list")
@Data
public class SysNodeList implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    @TableField("`deviceName`")
    private String deviceName;

    /**
     * 
     */
    @TableField("`nodeId`")
    private String nodeId;

    /**
     * 
     */
    @TableField("`dataType`")
    private String dataType;

    /**
     * 
     */
    @TableField("`displayName`")
    private String displayName;

    /**
     * 
     */
    private Boolean enable;

    /**
     * 
     */
    private String description;

    /**
     * 
     */
    private String time = CommUtils.getCurrentDateTime();

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}