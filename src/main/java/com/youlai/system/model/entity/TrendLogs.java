package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * TrendLogs实体类，用于记录趋势日志。
 */
@Data
@TableName("sys_trend_logs")
public class TrendLogs {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 设备标识
     */
    @NotNull(message = "设备标识不能为空")
    private String device;

    /**
     * 地址
     */
    @NotNull(message = "地址不能为空")
    private String address;

    /**
     * 类型
     */
    private String type;

    /**
     * 值
     */
    @NotNull(message = "值不能为空")
    private String value;

    /**
     * 单位
     */
    @Size(max = 20, message = "单位长度不能超过20个字符")
    private String unit;

    /**
     * 时间戳或时间相关字段
     */
    @Size(max = 50, message = "时间字段长度不能超过50个字符")
    private String time;

    /**
     * 描述
     */
    @Size(max = 100, message = "描述长度不能超过100个字符")
    private String description;
}