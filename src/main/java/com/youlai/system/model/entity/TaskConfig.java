package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("sys_taskSchedule")
public class TaskConfig {
    private Long id;
    private String name;
    private String cron;
    private Boolean enabled;
}
