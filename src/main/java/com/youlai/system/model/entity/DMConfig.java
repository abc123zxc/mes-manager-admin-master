package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youlai.system.common.util.CommUtils;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;


@Data
@TableName("sys_dm_config")
public class DMConfig {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 设备名称
     */
    @NotBlank(message = "设备名称不能为空")
    private String device;

    /**
     * 地址
     */
    @NotBlank(message = "地址不能为空")
    private String address;

    /**
     * 是否启用
     */
    private Boolean enable = false; // 默认值为 false

    /**
     * 文本ID
     */
    @NotBlank(message = "文本ID不能为空")
    private String textId;

    /**
     * 描述
     */
    @NotBlank(message = "不能为空")
    private String zhCn;
    /**
     * 描述
     */
    @NotBlank(message = "不能为空")
    private String enUs;
    /**
     * 描述
     */
    @NotBlank(message = "不能为空")
    private String deDe;
    /**
     * 描述
     */
    @NotBlank(message = "不能为空")
    private String koKr;

    /**
     * 创建时间
     */
    private String time = CommUtils.getCurrentDateTime();
}