package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.List;

@Data
@TableName("sys_panel_description")
public class PanelDescription {

    private Long id;
    @NotBlank(message = "不能为空")
    private String componentName;
    @NotBlank(message = "不能为空")
    private String img;
    @NotBlank(message = "不能为空")
    private String title;
    @NotBlank(message = "不能为空")
    private String description;
    @NotBlank(message = "不能为空")
    private List<String> tags;


}
