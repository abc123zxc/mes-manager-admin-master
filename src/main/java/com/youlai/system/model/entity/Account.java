package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
@TableName("Account")
public class Account {

    @TableId(type = IdType.AUTO)
    private Long id;

    @NotBlank(message = "username不能为空")
    private String username;

    @NotBlank(message = "password 不能为空")
    private String password;

    @NotBlank(message = "url 不能为空")
    @Pattern(regexp = "^(http|https)://.*$",
            message = "url 格式不正确")
    private String url;
}
