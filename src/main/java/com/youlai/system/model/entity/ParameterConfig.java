package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youlai.system.common.util.CommUtils;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@TableName("sys_parameter_config")
public class ParameterConfig {

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 设备标识
     */
    @NotBlank(message = "设备标识不能为空")
    private String device;

    /**
     * 地址
     */
    @NotBlank(message = "地址不能为空")
    private String address;

    /**
     * 类型
     */
    @NotBlank(message = "类型不能为空")
    @Size(max = 50, message = "类型长度不能超过50个字符")
    private String type;

    /**
     * 参数名称
     */
    @NotBlank(message = "参数名称不能为空")
    @Size(max = 50, message = "参数名称长度不能超过50个字符")
    private String parameter;

    /**
     * 是否启用
     */
    @NotNull(message = "启用状态不能为空")
    private Boolean enable;

    /**
     * 单位
     */
    @Size(max = 20, message = "单位长度不能超过20个字符")
    private String unit;

    /**
     * 创建时间
     */
    private String time = CommUtils.getCurrentDateTime();

    /**
     * 描述
     */
    @Size(max = 100, message = "描述长度不能超过100个字符")
    private String description;
}