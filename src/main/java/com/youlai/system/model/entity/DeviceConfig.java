package com.youlai.system.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.youlai.system.common.util.CommUtils;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * DeviceConfig实体类，用于配置系统设备信息。
 */
@Data
@TableName("sys_device_config")
public class DeviceConfig {



    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * IP地址
     */
    @NotNull(message = "IP地址不能为空")
    private String ip;

    /**
     * 端口号
     */
    @NotNull(message = "端口号不能为空")
    private Integer port;

    /**
     * 设备名称
     */
    @NotNull(message = "设备名称不能为空")
    private String name;

    /**
     * 设备类型
     */
    @NotNull(message = "设备类型不能为空")
    private Integer type;

    /**
     * 通信协议
     */
    @NotNull(message = "通信协议不能为空")
    private Integer protocol;

    /**
     * 连接状态
     */
//    @NotNull(message = "连接状态不能为空")
    private Boolean connected = false;

    /**
     * 订阅状态
     */
//    @NotNull(message = "订阅状态不能为空")
    private Boolean subscribed = false;

    /**
     * 启用状态
     */
    @NotNull(message = "启用状态不能为空")
    private Boolean enable;

    /**
     * 地点1
     */
    @NotNull(message = "地点不能为空")
    private String position;


    /**
     * 创建时间
     */
    private String time = CommUtils.getCurrentDateTime();

    /**
     * 描述
     */
    private String description;

    /**
     * 全参构造函数
     */
    public DeviceConfig(Long id, String ip, Integer port, String name, Integer type, Integer protocol,
                        Boolean connected, Boolean subscribed, Boolean enable, String position, String time, String description) {
        this.id = id;
        this.ip = ip;
        this.port = port;
        this.name = name;
        this.type = type;
        this.protocol = protocol;
        this.connected = connected != null ? connected : false;
        this.subscribed = subscribed != null ? subscribed : false;
        this.enable = enable;
        this.position = position;
        this.time = time != null ? time : CommUtils.getCurrentDateTime();
        this.description = description;
    }

    // 默认构造函数
    public DeviceConfig() {
        // 默认构造函数可以为空
    }
}