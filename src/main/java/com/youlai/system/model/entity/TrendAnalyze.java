package com.youlai.system.model.entity;

import lombok.Data;

@Data
public class TrendAnalyze {
    private String value;
    private String unit;
}
