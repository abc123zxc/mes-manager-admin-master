package com.youlai.system.model.Property;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/*
时间程序 配置 来自 application.xml

#时间程序配置
timeProgram:
    OPCPrefix: "ns=3;s=\""
  #DB符号名称
    DBName_Content: "TimeProgram"
  #符号名称
    DBStatic_Content: "Data"
    #时间程序存储名称
    DBData_Content: "Data"
    #DB数组长度
    DB_Length: 100
    #时间程序步骤长度
    DBStep_Length: 150

 */
@Component
@Data
public class TimeProgramProperty {

    @Value("${timeProgram.OPCPrefix}")
    private String OPCPrefix;

    @Value("${timeProgram.DBName_Content}")
    private String DBName_Content;

    @Value("${timeProgram.DBStatic_Content}")
    private String DBStatic_Content;

    @Value("${timeProgram.DBData_Content}")
    private String DBData_Content;

    @Value("${timeProgram.DB_Length}")
    private Integer DB_Length;

    @Value("${timeProgram.DBStep_Length}")
    private Integer DBStep_Length;





//    // Getter 方法
//    public String getMyProperty() {
//        return myProperty;
//    }
//
//    // Setter 方法（可选）
//    public void setMyProperty(String myProperty) {
//        this.myProperty = myProperty;
//    }
}
