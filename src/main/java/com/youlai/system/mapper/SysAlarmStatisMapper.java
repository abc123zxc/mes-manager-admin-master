package com.youlai.system.mapper;

import com.youlai.system.model.entity.SysAlarmStatis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;



@Mapper
public interface SysAlarmStatisMapper extends BaseMapper<SysAlarmStatis> {

}




