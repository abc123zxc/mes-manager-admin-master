package com.youlai.system.mapper;

import com.youlai.system.model.entity.SysNodeList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Guijiang.Lai
* @description 针对表【sys_node_list】的数据库操作Mapper
* @createDate 2024-12-08 14:51:01
* @Entity com.youlai.system.model.entity.SysNodeList
*/
@Mapper
public interface SysNodeListMapper extends BaseMapper<SysNodeList> {

}




