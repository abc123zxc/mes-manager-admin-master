package com.youlai.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youlai.system.model.entity.TrendAnalyze;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TrendAnalyzeMapper extends BaseMapper<TrendAnalyze> {
}
