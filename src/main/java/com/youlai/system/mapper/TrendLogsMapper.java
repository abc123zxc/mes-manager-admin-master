package com.youlai.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youlai.system.model.entity.TrendLogs;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TrendLogsMapper extends BaseMapper<TrendLogs> {
    TrendLogs getTrendLogsByAddress(String address);

    List<TrendLogs> getTrendLogsByDevice(String device);
}
