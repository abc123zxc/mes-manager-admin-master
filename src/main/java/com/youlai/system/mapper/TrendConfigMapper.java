package com.youlai.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youlai.system.model.entity.TrendConfig;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TrendConfigMapper extends BaseMapper<TrendConfig> {
    TrendConfig getTrendConfigByAddress(String address);
}
