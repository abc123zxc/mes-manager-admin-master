package com.youlai.system.mapper;

import com.youlai.system.model.entity.TimeProgram;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Guijiang.Lai
* @description 针对表【sys_time_program】的数据库操作Mapper
* @createDate 2024-10-22 19:14:05
* @Entity com.youlai.system.model.entity.TimeProgram
*/

@Mapper
public interface TimeProgramMapper extends BaseMapper<TimeProgram> {

}




