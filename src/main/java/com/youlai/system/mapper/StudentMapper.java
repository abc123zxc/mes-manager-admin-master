package com.youlai.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youlai.system.model.entity.Student;
import com.youlai.system.model.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StudentMapper  extends BaseMapper<Student> {
    List<Student> getAllStudents();
    Student getStudentById(Long id);
    void insertStudent(Student student);
    void updateStudent(Student student);
    void deleteStudent(Student student);
}
