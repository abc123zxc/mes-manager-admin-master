package com.youlai.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youlai.system.model.entity.EventText;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EventTextConfigMapper extends BaseMapper<EventText> {
}
