package com.youlai.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youlai.system.model.entity.DeviceConfig;
import com.youlai.system.model.entity.ParameterConfig;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DeviceConfigMapper extends BaseMapper<DeviceConfig> {
}
