package com.youlai.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youlai.system.model.entity.PanelDescription;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PanelDescriptionMapper extends BaseMapper<PanelDescription> {

}
