package com.youlai.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youlai.system.model.entity.TaskConfig;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface TaskScheduleMapper extends BaseMapper<TaskConfig> {

}




