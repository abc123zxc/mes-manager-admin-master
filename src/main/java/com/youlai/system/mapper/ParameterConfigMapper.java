package com.youlai.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youlai.system.model.entity.ParameterConfig;
import com.youlai.system.model.entity.TrendConfig;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ParameterConfigMapper extends BaseMapper<ParameterConfig> {
}
