package com.youlai.system.manager;


import com.youlai.system.common.util.RedisUtil;
import com.youlai.system.global.GlobalData;
import org.springframework.stereotype.Component;

/*
系统语言
 */
@Component
public class LanguageManager {
    private static final String LANGUAGE_KEY = "current_language";

    private  RedisUtil redisUtil;

    public LanguageManager(RedisUtil redisUtil) {
        this.redisUtil = redisUtil;
    }

    // 获取当前语言
    public String getCurrentLanguage() {
        return (String) redisUtil.get(LANGUAGE_KEY);
    }

    // 设置当前语言
    public void setCurrentLanguage(GlobalData.Language language) {
        redisUtil.set(LANGUAGE_KEY, language);
    }

    public boolean setCurrentLanguage(String language) {
      return   redisUtil.set(LANGUAGE_KEY, language);
    }
}
