package com.youlai.system.common.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@Component
public class RedisUtil {


    private final RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public RedisUtil(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }


    /**
     * 设置缓存对象
     *
     * @param key   缓存key
     * @param value 缓存value
     * @return 操作结果
     */
    public boolean set(final String key, Object value) {
        try {
            ValueOperations<String, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 获取缓存对象
     *
     * @param key 缓存key
     * @return 缓存value
     */
    public Object get(final String key) {
        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        return operations.get(key);
    }

    /**
     * 删除缓存对象
     *
     * @param key 缓存key
     * @return 操作结果
     */
    public boolean delete(final String key) {
        try {
            redisTemplate.delete(key);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 设置缓存并指定过期时间
     *
     * @param key      缓存key
     * @param value    缓存value
     * @param timeout  过期时间
     * @param timeUnit 时间单位
     * @return 操作结果
     */
    public boolean set(final String key, Object value, long timeout, TimeUnit timeUnit) {
        try {
            ValueOperations<String, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value, timeout, timeUnit);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 判断缓存是否存在
     *
     * @param key 缓存key
     * @return 是否存在
     */
    public boolean exists(final String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 增加或减少缓存对象的数值类型值
     *
     * @param key 缓存key
     * @param delta 增加或减少的数量
     * @return 新的value值
     */
    public Long increment(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    public Object updateValue(final String key, Function<Object, Object> updater) {
        Object currentValue = get(key);
        if (currentValue == null) {
            throw new RuntimeException("缓存不存在");
        }

        Object newValue = updater.apply(currentValue);
        set(key, newValue);
        return newValue;
    }

    // 可以继续添加其他所需方法...
}