package com.youlai.system.common.util;

import com.alibaba.dashscope.aigc.generation.Generation;
import com.alibaba.dashscope.aigc.generation.GenerationParam;
import com.alibaba.dashscope.aigc.generation.GenerationResult;
import com.alibaba.dashscope.common.Message;
import com.alibaba.dashscope.common.Role;
import com.alibaba.dashscope.exception.ApiException;
import com.alibaba.dashscope.exception.InputRequiredException;
import com.alibaba.dashscope.exception.NoApiKeyException;
import com.alibaba.dashscope.utils.Constants;
import com.alibaba.dashscope.utils.JsonUtils;
import com.youlai.system.model.entity.AIStream;

import java.util.ArrayList;
import java.util.List;

public class AIUtils {
    /**
     * Qwen2 AI 大数据模型调用
     */
    public static AIStream callQwen2AI(AIStream answer)
            throws NoApiKeyException, ApiException, InputRequiredException {
        Constants.apiKey="sk-483aadf1195e4f4da06ef492881516fc";
        AIStream aiAnswer = new AIStream();
        Generation gen = new Generation();
        List<Message> messages = new ArrayList<>();
        Message systemMsg =
                Message.builder().role(Role.SYSTEM.getValue()).content("You are a helpful assistant.").build();
        Message userMsg = Message.builder().role(Role.USER.getValue()).content(answer.getQuestion()).build();
        messages.add(systemMsg);
        messages.add(userMsg);
        GenerationParam param =
                GenerationParam.builder().model(Generation.Models.QWEN_TURBO).messages(messages)
                        .resultFormat(GenerationParam.ResultFormat.MESSAGE)
                        .build();
        GenerationResult result = gen.call(param);
        System.out.println(JsonUtils.toJson(result));
        aiAnswer.setData(JsonUtils.toJson(result));
        aiAnswer.setQuestion(JsonUtils.toJson(answer.getQuestion()));
        aiAnswer.setAnswer(JsonUtils.toJson(result.getOutput().getChoices()));
        return aiAnswer;
    }



}
