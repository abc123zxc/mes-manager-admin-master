package com.youlai.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.common.result.PageResult;
import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.EventText;
import com.youlai.system.model.query.EventTextQuery;
import com.youlai.system.plugin.dupsubmit.annotation.PreventDuplicateSubmit;
import com.youlai.system.service.EventTextConfigService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "890.国际化文本配置")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/event/text")
public class EventTextConfigController {

    //private static final Logger log = LoggerFactory.getLogger(EventTextConfigController.class);

    private final EventTextConfigService eventTextConfigService;

    @GetMapping("/page")
    @Operation(summary = "分页查询")
    public PageResult<EventText> getParameterConfigPage(@ParameterObject EventTextQuery query) {
        Page<EventText> result = eventTextConfigService.getEventTextConfigPage(query);
        return PageResult.success(result);
    }

    @GetMapping
    @Operation(summary = "获取所有")
    public Result<List<EventText>> getAllTrendConfig() {
        return Result.success(eventTextConfigService.list());
    }

    @GetMapping("/type2")
    @Operation(summary = "获取所有 返回")
    public Result<List<EventText>> getAllTrendConfigType2() {
        return Result.success(eventTextConfigService.list());
    }


    @GetMapping("/{id}")
    @Operation(summary = "获取By Id")
    public Result<EventText> getParameterConfigById(@PathVariable Long id) {
        return Result.success(eventTextConfigService.getById(id));
    }

    @PutMapping
    @Operation(summary = "更新信息")
    @PreAuthorize("@ss.hasPerm('sys:evenText:edit')")
    public Result<Boolean> updateParameterConfig(@Valid @RequestBody EventText eventText) {
        return Result.judge(eventTextConfigService.updateById(eventText));
    }

    @DeleteMapping("/{ids}")
    @Operation(summary = "删除信息")
    @PreAuthorize("@ss.hasPerm('sys:evenText:delete')")
    public Result<Boolean> deleteParameterConfig(@PathVariable String ids) {
        return Result.judge(eventTextConfigService.deleteEventTextConfig(ids));
    }

    @PostMapping
    @PreventDuplicateSubmit
    @Operation(summary = "添加")
    @PreAuthorize("@ss.hasPerm('sys:evenText:add')")
    public Result<Boolean> saveParameterConfig(@Valid  @RequestBody EventText eventText) {
        return Result.judge(eventTextConfigService.save(eventText));
    }

    @PostMapping("/list")
    @PreventDuplicateSubmit
    @Operation(summary = "批量添加")
    @PreAuthorize("@ss.hasPerm('sys:evenText:addList')")
    public Result<Boolean> saveParameterConfigList(@Valid @RequestBody List<EventText> eventTextList) {
        boolean allSucceeded = eventTextList.stream()
                .allMatch(config -> eventTextConfigService.save(config));
        return Result.judge(allSucceeded);
    }


}