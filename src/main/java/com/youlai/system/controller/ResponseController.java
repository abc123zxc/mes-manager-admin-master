package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.manager.LanguageManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "300.更新操作")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/response")
public class ResponseController {

    private final LanguageManager languageManager;
    @GetMapping("/language/{language}")
    @Operation(summary = "语言切换")
    public Result<Boolean> changeLanguage (@PathVariable String language){
        return Result.success(languageManager.setCurrentLanguage(language)) ;
    }
}
