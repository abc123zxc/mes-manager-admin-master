package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.model.query.DataLogsQuery;
import com.youlai.system.model.vo.DataLogsVO;
import com.youlai.system.service.DataLogsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.youlai.system.common.util.TensorFlow.comForecast;

@Tag(name = "980.数据处理")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/data")
public class DataLogsController {
    private final DataLogsService dataLogsService;

    @GetMapping("/forecast")
    @Operation(summary = "数据预测")
    public Result<List<DataLogsVO>> getForecastData(@ParameterObject DataLogsQuery dataLogsQuery){
        List<DataLogsVO> dataLogsVOList =  dataLogsService.getDataLog(dataLogsQuery);
        List<DataLogsVO> result = comForecast(dataLogsVOList);
        return Result.success(result) ;
    }

}
