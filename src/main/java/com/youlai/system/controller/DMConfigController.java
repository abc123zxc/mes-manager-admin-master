package com.youlai.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.common.result.PageResult;
import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.DMConfig;
import com.youlai.system.model.query.DMConfigQuery;
import com.youlai.system.plugin.dupsubmit.annotation.PreventDuplicateSubmit;
import com.youlai.system.service.DMConfigService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "880.离散量配置")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/dm")
public class DMConfigController {


    private final DMConfigService dmConfigService;

    @GetMapping("/page")
    @Operation(summary = "分页查询")
    public PageResult<DMConfig> getDMConfigPage(@ParameterObject DMConfigQuery query) {
        Page<DMConfig> result = dmConfigService.getDMConfigPage(query);
        return PageResult.success(result);
    }

    @GetMapping
    @Operation(summary = "获取所有")
    public Result<List<DMConfig>> getAllDMConfig() {
        return Result.success(dmConfigService.list());
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取By Id")
    public Result<DMConfig> getDMConfigById(@PathVariable Long id) {
        return Result.success(dmConfigService.getById(id));
    }

    @PutMapping
    @Operation(summary = "更新信息")
    @PreAuthorize("@ss.hasPerm('sys:dmConfig:edit')")
    public Result<Boolean> updateDMConfig(@Valid @RequestBody DMConfig eventText) {
        return Result.judge(dmConfigService.updateById(eventText));
    }

    @DeleteMapping("/{ids}")
    @Operation(summary = "删除信息")
    @PreAuthorize("@ss.hasPerm('sys:dmConfig:delete')")
    public Result<Boolean> deleteDMConfig(@PathVariable String ids) {
        return Result.judge(dmConfigService.deleteDMConfig(ids));
    }

    @PostMapping
    @PreventDuplicateSubmit
    @Operation(summary = "添加")
    @PreAuthorize("@ss.hasPerm('sys:dmConfig:add')")
    public Result<Boolean> saveDMConfig(@Valid  @RequestBody DMConfig dmConfig) {
        return Result.judge(dmConfigService.save(dmConfig));
    }

    @PostMapping("/list")
    @PreventDuplicateSubmit
    @Operation(summary = "批量添加")
    @PreAuthorize("@ss.hasPerm('sys:dmConfig:addList')")
    public Result<Boolean> saveDMConfigList( @RequestBody List<DMConfig> eventTextList) {
        boolean allSucceeded = eventTextList.stream()
                .allMatch(dmConfigService::save);
        return Result.judge(allSucceeded);
    }


}