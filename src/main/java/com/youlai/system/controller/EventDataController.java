package com.youlai.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.common.result.PageResult;
import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.EventData;
import com.youlai.system.model.query.EventDataQuery;
import com.youlai.system.model.vo.EventDataBtnVO;
import com.youlai.system.model.vo.EventDataUpdateBatch;
import com.youlai.system.service.EventDataService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.*;

@Tag(name = "910.Event Data")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/event")
public class EventDataController {

//    private static final Logger log = LoggerFactory.getLogger(EventDataController.class);

    private final EventDataService eventDataService;


    @GetMapping("/page")
    @Operation(summary = "分页查询")
    public PageResult<EventData> getEventDataPage(@ParameterObject EventDataQuery query) {
        Page<EventData> result = eventDataService.getEventDataPage(query);
        return PageResult.success(result);
    }

    @GetMapping("/number")
    @Operation(summary = "获取数量")
    public Result<EventDataBtnVO> getEventDataNumber() {
        EventDataBtnVO result = eventDataService.getEventDataBtnVO();
        return Result.success(result);
    }

    @PutMapping
    @Operation(summary = "更新信息")
//    @PreAuthorize("@ss.hasPerm('sys:trend:edit')")
    public Result<Boolean> updateTrend(@Valid @RequestBody EventData eventData){
        return Result.success(eventDataService.updateById(eventData));
    }

    @PutMapping("/updateByIds")
    @Operation(summary = "批量更新信息")
//    @PreAuthorize("@ss.hasPerm('sys:trend:edit')")
    public Result<Boolean> updateBath( @RequestBody EventDataUpdateBatch eventDataUpdateBatch){
        return Result.success(eventDataService.updateBatch(eventDataUpdateBatch));
    }


}