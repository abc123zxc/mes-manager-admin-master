package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.common.util.DataUtils;
import com.youlai.system.model.entity.AnalyzeData;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Tag(name = "990.数据分析接口")
@RestController
@RequestMapping("/api/v1/data")
@RequiredArgsConstructor
public class AnalyzeDataController {

    @PostMapping("/analyze")
    @Operation(summary = "数据分析")
    public Result<AnalyzeData> getAnalyzeData(@RequestBody int[] data){
        return Result.success(DataUtils.analyzeData(data));
    }

}
