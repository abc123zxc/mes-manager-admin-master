package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.model.query.TrendLogsQuery;
import com.youlai.system.model.vo.TrendLogsVO;
import com.youlai.system.service.TrendLogsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "970.PLC Trend Logs")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/trend/logs")
public class TrendLogsController {


    private final TrendLogsService trendLogsService;

    @Operation(summary = "获取设备历史记录")
    @GetMapping("")
    public Result<List<TrendLogsVO>> getDeviceLog(
            @ParameterObject TrendLogsQuery queryParams
    ) {
        List<TrendLogsVO> result = trendLogsService.listOfTrend(queryParams);
        return result != null ? Result.success(result) : Result.failed("没有找到数据");
    }

}
