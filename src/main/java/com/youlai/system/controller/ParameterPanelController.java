package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.ParameterPanel;
import com.youlai.system.service.ParameterPanelService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "960.参数面板")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/panel/")
public class ParameterPanelController {

    private final ParameterPanelService parameterPanelService;

    @GetMapping("/parameter")
    @Operation(summary = "获取所有")
    public Result<List<ParameterPanel>> getAllParameterPanelData(){
        return Result.success(parameterPanelService.getAllParameterPanelData());
    }

}
