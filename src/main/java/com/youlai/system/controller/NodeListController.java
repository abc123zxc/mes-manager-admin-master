package com.youlai.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.common.result.PageResult;
import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.SysNodeList;
import com.youlai.system.model.enums.OPCDataType;
import com.youlai.system.model.query.NodeListQuery;
import com.youlai.system.plugin.dupsubmit.annotation.PreventDuplicateSubmit;
import com.youlai.system.service.SysNodeListService;
import groovy.util.logging.Slf4j;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "190.OPC NODE")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/opcNode")
@Slf4j
public class NodeListController {

    private final SysNodeListService sysNodeListService;


    @GetMapping("/page")
    @Operation(summary = "分页查询")
    public PageResult<SysNodeList> getOPCNodePage(@ParameterObject NodeListQuery nodeListQuery){
        Page<SysNodeList> result = sysNodeListService.getOPCNodePage(nodeListQuery);
        return PageResult.success(result);
    }

    @GetMapping("/type/{opcDataType}")
    @Operation(summary = "获取类型")
    public  Result<List<SysNodeList>> getOPCNodeByDataType(@PathVariable OPCDataType opcDataType){
        return Result.success(sysNodeListService.getOPCNodeByDataType(opcDataType));
    }


    @GetMapping
    @Operation(summary = "获取所有")
    public Result<List<SysNodeList>> getAllTrendConfig(){
        return  Result.success(sysNodeListService.list());
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取By Id")
    public Result<SysNodeList> getAccountById(@PathVariable Long id){
        return Result.success(sysNodeListService.getById(id));
    }

    @PutMapping
    @Operation(summary = "更新信息")
//    @PreAuthorize("@ss.hasPerm('sys:device:edit')")
    public Result<Boolean> updateAccount(@Valid  @RequestBody SysNodeList sysNodeList){
        return  Result.judge(sysNodeListService.updateById(sysNodeList));
    }

    @DeleteMapping("/{ids}")
    @Operation(summary = "删除信息")
//    @PreAuthorize("@ss.hasPerm('sys:device:delete')")
    public Result<Boolean> deleteAccount(@PathVariable String ids){
        return Result.judge(sysNodeListService.deleteOPCNode(ids));
    }

    @PostMapping
    @PreventDuplicateSubmit
    @Operation(summary = "添加")
    //@PreAuthorize("@ss.hasPerm('sys:device:add')")
    public Result<Boolean> saveAccount(@Valid @RequestBody SysNodeList sysNodeList){
        return Result.judge(sysNodeListService.save(sysNodeList));
    }

    @PostMapping("/list")
    @PreventDuplicateSubmit
    @Operation(summary = "批量添加")
//    @PreAuthorize("@ss.hasPerm('sys:device:addList')")
    public Result<Boolean> saveTrendConfigList(@Valid @RequestBody List<SysNodeList> sysNodeLists){
        return Result.judge(sysNodeLists.stream()
                .allMatch(sysNodeListService::save));
    }

}
