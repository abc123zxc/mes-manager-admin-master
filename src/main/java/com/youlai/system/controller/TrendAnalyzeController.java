package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.TrendAnalyze;
import com.youlai.system.model.query.TrendAnalyzeQuery;
import com.youlai.system.model.vo.TrendAnalyzeScrollListVO;
import com.youlai.system.service.TrendAnalyzeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "940.trend 数据分析")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/trend/analyze")
public class TrendAnalyzeController {


    private final TrendAnalyzeService trendAnalyzeService;

    @Operation(summary = "获取数据")
    @GetMapping("")
    public Result<TrendAnalyze> getTrendAnalyzeData(
           @ParameterObject TrendAnalyzeQuery queryParams) {
        return Result.success(trendAnalyzeService.getTrendAnalyzeData(queryParams));
    }

    @Operation(summary = "获取scroll数据")
    @PostMapping("")
    public Result<List<TrendAnalyzeScrollListVO>> getTrendAnalyzeScroll(
            @RequestBody String desc) {
        return Result.success(trendAnalyzeService.getTrendAnalyzeScroll(desc));
    }


}
