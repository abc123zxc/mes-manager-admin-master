package com.youlai.system.controller;

import com.alibaba.dashscope.aigc.generation.Generation;
import com.alibaba.dashscope.aigc.generation.GenerationResult;
import com.alibaba.dashscope.exception.InputRequiredException;
import com.alibaba.dashscope.exception.NoApiKeyException;
import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.AIStream;
import com.youlai.system.common.util.AIUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;

@Tag(name = "AI询问操作")
@RestController
@RequestMapping("/api/v1/ai/ask/stream")
@RequiredArgsConstructor
public class AIController {

    @Operation(summary = "询问AI")
    @PostMapping()
    public Result<AIStream> askQwen2Stream(@RequestBody String question) {
        AIStream aiStream = new AIStream();
        aiStream.setQuestion(question);
        try {
            aiStream =  AIUtils.callQwen2AI(aiStream);
        } catch (NoApiKeyException e) {
            throw new RuntimeException(e);
        } catch (InputRequiredException e) {
            throw new RuntimeException(e);
        }
        System.out.println("调用了千问2模型");
        return Result.success(aiStream);
    }
}
