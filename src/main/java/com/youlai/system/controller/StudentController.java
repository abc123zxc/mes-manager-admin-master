package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.Student;
import com.youlai.system.service.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "999.测试")
@RestController
@RequestMapping("/api/v1/test/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping
    @Operation(summary = "获取所有学生数据")
    public Result<List<Student>> getAllStudents(){
        List<Student> students = studentService.getAllStudents();
        return Result.success(students);
    }

    @GetMapping("/{id}")
    @Operation(summary = "通过Id获取学生信息")
    public Result<Student> getStudentById(@PathVariable Long id){
        return Result.success(studentService.getStudentById(id));
    }

    @PostMapping
    @Operation(summary = "插入学生数据")
    public void insertStudent(@Valid  @RequestBody Student student){
        studentService.insertStudent(student);
    }

    @PutMapping("/{id}")
    @Operation(summary = "更新学生数据")
    public void updateStudent(@Valid @RequestBody Student student){
        studentService.updateStudent(student);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "删除学生数据")
    public  void deleteStudent(@RequestBody Student student){
        studentService.deleteStduent(student);
    }

//    @Scheduled(fixedDelay =  5000)
//    public void testSch (){
//        System.out.println("Hello--------------------------------------------------------");
//    }
}