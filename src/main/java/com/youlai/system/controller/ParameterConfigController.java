package com.youlai.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.common.result.PageResult;
import com.youlai.system.common.result.Result;
import com.youlai.system.global.GlobalData;
import com.youlai.system.model.entity.ParameterConfig;
import com.youlai.system.model.query.ParameterConfigQuery;
import com.youlai.system.model.vo.ParameterConfigPageVO;
import com.youlai.system.plugin.dupsubmit.annotation.PreventDuplicateSubmit;
import com.youlai.system.service.ParameterConfigService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "970.PLC Parameter")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/parameter")
public class ParameterConfigController {

    private static final Logger log = LoggerFactory.getLogger(ParameterConfigController.class);

    private final ParameterConfigService parameterConfigService;

    @GetMapping("/page")
    @Operation(summary = "分页查询")
    public PageResult<ParameterConfigPageVO> getParameterConfigPage(@ParameterObject ParameterConfigQuery parameterConfigQuery) {
        Page<ParameterConfigPageVO> result = parameterConfigService.getParameterConfigPage(parameterConfigQuery);
        return PageResult.success(result);
    }

    @GetMapping
    @Operation(summary = "获取所有")
    public Result<List<ParameterConfig>> getAllTrendConfig() {
        return Result.success(parameterConfigService.list());
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取By Id")
    public Result<ParameterConfig> getParameterConfigById(@PathVariable Long id) {
        return Result.success(parameterConfigService.getById(id));
    }

    @PutMapping
    @Operation(summary = "更新信息")
    @PreAuthorize("@ss.hasPerm('sys:parameter:edit')")
    public Result<Boolean> updateParameterConfig(@Valid @RequestBody ParameterConfig parameterConfig) {
        return Result.judge(parameterConfigService.updateById(parameterConfig));
    }

    @DeleteMapping("/{ids}")
    @Operation(summary = "删除信息")
    @PreAuthorize("@ss.hasPerm('sys:parameter:delete')")
    public Result<Boolean> deleteParameterConfig(@PathVariable String ids) {
        return Result.judge(parameterConfigService.deleteParameterConfig(ids));
    }

    @PostMapping
    @PreventDuplicateSubmit
    @Operation(summary = "添加")
    @PreAuthorize("@ss.hasPerm('sys:parameter:add')")
    public Result<Boolean> saveParameterConfig(@Valid  @RequestBody ParameterConfig parameterConfig) {
        return Result.judge(parameterConfigService.save(parameterConfig));
    }

    @PostMapping("/list")
    @PreventDuplicateSubmit
    @Operation(summary = "批量添加")
    @PreAuthorize("@ss.hasPerm('sys:parameter:addList')")
    public Result<Boolean> saveParameterConfigList(@Valid @RequestBody List<ParameterConfig> parameterConfigList) {
        boolean allSucceeded = parameterConfigList.stream()
                .allMatch(parameterConfigService::save);
        return Result.judge(allSucceeded);
    }

    @PostMapping("/op/download/{ids}")
    @PreventDuplicateSubmit
    @Operation(summary = "下载")
    @PreAuthorize("@ss.hasPerm('sys:parameter:download')")
    public Result<Boolean> downToPLC(@PathVariable String ids) {
        if(GlobalData.opcUaHelper.isConnected){
            return Result.success(parameterConfigService.writeParameterConfigsToPLC(ids));
        }else{
            return Result.failed("请连接PLC!");
        }
    }

}