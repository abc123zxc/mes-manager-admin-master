package com.youlai.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.common.result.PageResult;
import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.TaskConfig;
import com.youlai.system.model.query.TaskConfigQuery;
import com.youlai.system.service.TaskScheduleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "810.任务配置")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/task")
public class TaskScheduleController {


    private final TaskScheduleService taskScheduleService;


    @GetMapping("/page")
    @Operation(summary = "分页查询")
    public PageResult<TaskConfig> getEventDataPage(@ParameterObject TaskConfigQuery query) {
        Page<TaskConfig> result = taskScheduleService.getTaskConfigPage(query);
        return PageResult.success(result);
    }

    @PostMapping
    public Result<?> addTask(@RequestBody TaskConfig task) {
        return Result.judge(taskScheduleService.saveOrUpdate(task));
    }

    @DeleteMapping("/{id}")
    public Result<?> deleteTask(@PathVariable Long id) {
        return Result.judge(taskScheduleService.removeById(id));
    }

    @PutMapping("/{id}")
    public Result<?> updateTask(@PathVariable Long id, @RequestBody TaskConfig task) {
        task.setId(id);
        return Result.judge(taskScheduleService.saveOrUpdate(task));
    }

    @GetMapping
    public Result<List<TaskConfig>> listTasks() {
        return Result.success(taskScheduleService.list());
    }

    @GetMapping("/{id}")
    public Result<TaskConfig> getTask(@PathVariable Long id) {
        return Result.success(taskScheduleService.getById(id));
    }

}