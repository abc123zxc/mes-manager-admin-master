package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.People;
import com.youlai.system.service.PeopleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "990.People")
@RestController
@RequestMapping("/api/v1/test/people")
@RequiredArgsConstructor
public class PeopleController {
    private final PeopleService peopleService;

//    @GetMapping
//    @Operation(summary = "获取所有People")
//    public Result<List<People>> getAllPeople(){
//        return  Result.success(peopleService.getAllPeople());
//    }
//
//    @GetMapping("/{id}")
//    @Operation(summary = "通过Id获取People")
//    public Result<People> getPeopleById(@PathVariable Long id){
//        return  Result.success(peopleService.getPeopleById(id));
//    }

    //使用BasicMapp方法

    @GetMapping
    @Operation(summary = "插入数据")
    public Result<Boolean> getAllPeople(){
        People people = new People();
        people.setAge(23);
        people.setEmail("1598996466@qq.com");
        people.setName("Allen222");
        return Result.success(peopleService.save(people));
    }


}
