package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.global.BigView;
import com.youlai.system.model.vo.BarChartDataVO;
import com.youlai.system.model.vo.PieChartDeviceNumberVO;
import com.youlai.system.model.vo.PieChartsDataVO;
import com.youlai.system.model.vo.TrendBigViewVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "700.BigView")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/bigview")
public class BigViewController {

    private static Integer EVENT_NUMBER = 1;
    private static Integer WARNING_NUMBER = 2;
    private static Integer FAULT_NUMBER = 3;

    @GetMapping("/lt")
    @Operation(summary = "LT")
    public Result<List<TrendBigViewVO>> getLTData(){
        return  Result.success(BigView.getMonthlyEventCountsToArray(WARNING_NUMBER));
    }

    @GetMapping("/lc")
    @Operation(summary = "LC")
    public Result<List<TrendBigViewVO>> getLCData(){
        return  Result.success(BigView.getMonthlyEventCountsToArray(FAULT_NUMBER));
    }

    @GetMapping("/lb")
    @Operation(summary = "LB")
    public Result<List<TrendBigViewVO>> getLBData(){
        return  Result.success(BigView.getMonthlyEventCountsToArray(EVENT_NUMBER));
    }

    @GetMapping("/rb")
    @Operation(summary = "RB")
    public Result<PieChartsDataVO> getRBData(){
        return  Result.success(BigView.getMonthlyEventFaultWarningCounts());
    }

    @GetMapping("/rt")
    @Operation(summary = "RT")
    public Result<List<Integer>> getRTData(){
        return  Result.success(BigView.getVariableEnableAndDisEnableCount());
    }


    @GetMapping("/rb_type1")
    @Operation(summary = "RT_TYPE1")
    public Result <BarChartDataVO> getRBType1Data(){
        return  Result.success(BigView.getBarChartTotalData());
    }

    @GetMapping("/rt_type1")
    @Operation(summary = "RT_TYPE1")
    public Result <List<PieChartDeviceNumberVO>> getRTType1Data(){
        return  Result.success(BigView.getPieChartDeviceNumberVO());
    }

}
