package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.SysAlarmStatis;
import com.youlai.system.service.SysAlarmStatisService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "999.测试")
@RestController
@RequestMapping("/api/v1/alarmstatis")
@RequiredArgsConstructor
public class SysAlarmStatisController {

    private final SysAlarmStatisService sysAlarmStatisService;

    @GetMapping
    @Operation(summary = "获取所有数据")
    public Result<List<SysAlarmStatis>> getAllStudents(){
        List<SysAlarmStatis> sysAlarmStatisList = sysAlarmStatisService.list();
        return Result.success(sysAlarmStatisList);
    }

    @GetMapping("/{time}")
    @Operation(summary = "通过Time获取学生信息")
    public Result<SysAlarmStatis> getSysAlarmStatisByTime(@PathVariable String time){
        return Result.success(sysAlarmStatisService.getAlarmStatistByTime(time));
    }

    @PostMapping
    @Operation(summary = "插入学生数据")
    public void saveSysAlarmStatis(@Valid  @RequestBody SysAlarmStatis sysAlarmStatis){
        sysAlarmStatisService.save(sysAlarmStatis);
    }

}