package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.model.dto.FileInfo;
import com.youlai.system.model.dto.VisuAlarm;
import com.youlai.system.model.dto.VisuData;
import com.youlai.system.service.OssService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Tag(name = "07.大屏")
@RestController
@RequestMapping("/api/data-visu")
@RequiredArgsConstructor
public class VisuController {

    private final static String visuFoldler = "D:\\大屏\\VisuData\\";

    @Operation(summary = "一周数据")
    @GetMapping("/one-week")
    public Result<List<VisuData>> getWeekData() {
        String csvFile = visuFoldler + "OneWeek.csv";
        String line = "";
        String csvSplitBy = ",";
        List<VisuData> dataList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            int index = 0;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split(csvSplitBy);
                var data = new VisuData();
                data.setCategory(lines[0]);
                data.setSeries(lines[2]);
                data.setValue(lines[3]);
                dataList.add(data);
            }
        } catch (IOException e) {
            Result.failed(e.getMessage());
        }
        return Result.success(dataList);
    }

    @Operation(summary = "今天数据")
    @GetMapping("/one-day")
    public Result<List<VisuData>> getDayData() {
        String csvFile = visuFoldler + "OneDay.csv";
        String line = "";
        String csvSplitBy = ",";
        List<VisuData> dataList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            int index = 0;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split(csvSplitBy);
                var data = new VisuData();
                data.setCategory(lines[0]);
                data.setSeries(lines[2]);
                data.setValue(lines[3]);
                dataList.add(data);
            }
        } catch (IOException e) {
            Result.failed(e.getMessage());
        }
        return Result.success(dataList);
    }

    @Operation(summary = "上周数据")
    @GetMapping("/last-week")
    public Result<List<VisuData>> getLastDayData() {
        String csvFile = visuFoldler + "LastWeek.csv";
        String line = "";
        String csvSplitBy = ",";
        List<VisuData> dataList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            int index = 0;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split(csvSplitBy);
                var data = new VisuData();
                data.setCategory(lines[0]);
                data.setSeries(lines[2]);
                data.setValue(lines[3]);
                dataList.add(data);
            }
        } catch (IOException e) {
            Result.failed(e.getMessage());
        }
        return Result.success(dataList);
    }

    @Operation(summary = "今天类型数据")
    @GetMapping("/type")
    public Result<List<VisuData>> getTypeData() {
        String csvFile = visuFoldler + "Type.csv";
        String line = "";
        String csvSplitBy = ",";
        List<VisuData> dataList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            int index = 0;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split(csvSplitBy);

                var data = new VisuData();
                data.setCategory(lines[0]);
                data.setSeries(lines[2]);
                data.setValue(lines[3]);
                dataList.add(data);
            }
        } catch (IOException e) {
            Result.failed(e.getMessage());
        }
        return Result.success(dataList);
    }

    @Operation(summary = "能源数据")
    @GetMapping("/energy")
    public Result<List<VisuData>> getEnergyData() {
        String csvFile = visuFoldler + "Energy.csv";
        String line = "";
        String csvSplitBy = ",";
        List<VisuData> dataList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            int index = 0;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split(csvSplitBy);
                var data = new VisuData();
                data.setCategory(lines[0]);
                data.setSeries(lines[2]);
                data.setValue(lines[3]);
                dataList.add(data);
            }
        } catch (IOException e) {
            Result.failed(e.getMessage());
        }
        return Result.success(dataList);
    }

    @Operation(summary = "工位节拍数据")
    @GetMapping("/station")
    public Result<List<VisuData>> getStationData() {
        String csvFile = visuFoldler + "Station.csv";
        String line = "";
        String csvSplitBy = ",";
        List<VisuData> dataList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            int index = 0;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split(csvSplitBy);

                var data = new VisuData();
                data.setCategory(lines[0]);
                data.setSeries(lines[2]);
                data.setValue(lines[3]);
                dataList.add(data);
            }
        } catch (IOException e) {
            Result.failed(e.getMessage());
        }
        return Result.success(dataList);
    }

    @Operation(summary = "报警数据")
    @GetMapping("/alarm")
    public Result<List<VisuAlarm>> getAlarmData() {
        String csvFile = visuFoldler + "Alarm.csv";
        String line = "";
        String csvSplitBy = ",";
        List<VisuAlarm> dataList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            int index = 0;
            while ((line = br.readLine()) != null) {
                String[] lines = line.split(csvSplitBy);

                var data= new VisuAlarm();
                data.setName(lines[0]);
                data.setType(lines[1]);
                data.setMessage(lines[2]);
                data.setUpdateTime(lines[3]);
                data.setDuration(lines[4]);
                dataList.add(data);
            }
        } catch (IOException e) {
            Result.failed(e.getMessage());
        }
        return Result.success(dataList);
    }

}
