package com.youlai.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.common.result.PageResult;
import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.Account;
import com.youlai.system.model.query.AccountQuery;
import com.youlai.system.model.vo.AccountPageVO;
import com.youlai.system.plugin.dupsubmit.annotation.PreventDuplicateSubmit;
import com.youlai.system.service.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "990.Account")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/test/account")
public class AccountController {
    private final AccountService accountService;

    @GetMapping("/page")
    @Operation(summary = "账户分页查询")
    public PageResult<AccountPageVO> getAccountPage(@ParameterObject AccountQuery accountQuery){
        Page<AccountPageVO> result = accountService.getAccountPage(accountQuery);
        return PageResult.success(result);
    }

    @GetMapping
    @Operation(summary = "获取所有账户")
    public Result<List<Account>> getAllAccount(){
        return  Result.success(accountService.list());
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取账户By Id")
    public Result<Account> getAccountById(@PathVariable Long id){
        return Result.success(accountService.getById(id));
    }

    @PutMapping
    @Operation(summary = "更新账户信息")
    @PreAuthorize("@ss.hasPerm('sys:account:edit')")
    public Result<Boolean> updateAccount(@Valid @RequestBody Account account){
        return  Result.judge(accountService.updateById(account));
    }

    @DeleteMapping("/{ids}")
    @Operation(summary = "删除账户信息")
    @PreAuthorize("@ss.hasPerm('sys:account:delete')")
    public Result<Boolean> deleteAccount(@PathVariable String ids){
        return Result.judge(accountService.deleteAccount(ids));
    }

    @PostMapping
    @PreventDuplicateSubmit
    @Operation(summary = "添加用户")
    @PreAuthorize("@ss.hasPerm('sys:account:add')")
    public Result<Boolean> saveAccount(@Valid @RequestBody Account account){
        return Result.judge(accountService.save(account));
    }
}
