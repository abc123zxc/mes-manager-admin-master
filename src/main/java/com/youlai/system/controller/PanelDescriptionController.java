package com.youlai.system.controller;

import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.PanelDescription;
import com.youlai.system.model.query.PanelSelectListQuery;
import com.youlai.system.model.vo.PanelSelectListVO;
import com.youlai.system.service.PanelDescriptionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "920.Panel")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/panel")
public class PanelDescriptionController {

    private final PanelDescriptionService panelDescriptionService;

    @GetMapping("/description")
    @Operation(summary = "获取所有")
    public Result<List<PanelDescription>> getPanelDes(){
        return  Result.success(panelDescriptionService.list());
    }

    @GetMapping("/list")
    @Operation(summary = "获取所有")
    public Result<List<PanelSelectListVO>> getPanelSelectList(@ParameterObject PanelSelectListQuery query){
        return  Result.success(panelDescriptionService.getPanelSelectList(query));
    }

}
