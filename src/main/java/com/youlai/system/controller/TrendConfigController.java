package com.youlai.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.common.result.PageResult;
import com.youlai.system.common.result.Result;
import com.youlai.system.model.entity.TrendConfig;
import com.youlai.system.model.query.TrendConfigQuery;
import com.youlai.system.plugin.dupsubmit.annotation.PreventDuplicateSubmit;
import com.youlai.system.service.TrendConfigService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "980.PLC Trend")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/trend")
public class TrendConfigController {


    private final TrendConfigService trendConfigService;

    @GetMapping("/page")
    @Operation(summary = "分页查询")
    public PageResult<TrendConfig> getTrendConfigPage(@ParameterObject TrendConfigQuery trendConfigQuery){
        Page<TrendConfig> result = trendConfigService.getTrendConfigPage(trendConfigQuery);
        return PageResult.success(result);
    }

    @GetMapping
    @Operation(summary = "获取所有")
    public Result<List<TrendConfig>> getAllTrendConfig(){
        return  Result.success(trendConfigService.list());
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取By Id")
    public Result<TrendConfig> getTrendById(@PathVariable Long id){
        return Result.success(trendConfigService.getById(id));
    }

    @PutMapping
    @Operation(summary = "更新信息")
    @PreAuthorize("@ss.hasPerm('sys:trend:edit')")
    public Result<Boolean> updateTrend(@Valid  @RequestBody TrendConfig trendConfig){
        return Result.success(trendConfigService.updateTrendConfig(trendConfig));
    }

    @DeleteMapping("/{ids}")
    @Operation(summary = "删除信息")
    @PreAuthorize("@ss.hasPerm('sys:trend:delete')")
    public Result<Boolean> deleteTrend(@PathVariable String ids) throws UaException {
        return Result.success(trendConfigService.deleteTrend(ids));
    }

    @PostMapping
    @PreventDuplicateSubmit
    @Operation(summary = "添加")
    @PreAuthorize("@ss.hasPerm('sys:trend:add')")
    public Result<Boolean> saveTrend(@Valid @RequestBody TrendConfig trendConfig){
        return Result.success(trendConfigService.saveTrendConfig(trendConfig));
    }

    @PostMapping("/list")
    @PreventDuplicateSubmit
    @Operation(summary = "批量添加")
    @PreAuthorize("@ss.hasPerm('sys:trend:addList')")
    public Result<Boolean> saveTrendConfigList(@Valid @RequestBody List<TrendConfig> trendConfigList){
        return Result.success(trendConfigService.saveTrendConfigList(trendConfigList));
    }
}
