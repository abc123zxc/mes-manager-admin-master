package com.youlai.system.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.model.entity.SysNodeList;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NodeListConverter {
    Page<SysNodeList> entity2Page(Page<SysNodeList> page);
}
