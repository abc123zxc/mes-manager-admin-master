package com.youlai.system.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.model.entity.Account;
import com.youlai.system.model.vo.AccountPageVO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountConverter {

    Page<AccountPageVO> entity2Page(Page<Account> page);
}
