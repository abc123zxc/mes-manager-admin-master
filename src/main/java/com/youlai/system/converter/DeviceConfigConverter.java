package com.youlai.system.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.model.entity.DeviceConfig;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DeviceConfigConverter {
    Page<DeviceConfig> entity2Page(Page<DeviceConfig> page);
}
