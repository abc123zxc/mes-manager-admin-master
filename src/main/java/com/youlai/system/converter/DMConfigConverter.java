package com.youlai.system.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.model.entity.DMConfig;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DMConfigConverter {
    Page<DMConfig> entity2Page(Page<DMConfig> page);
}
