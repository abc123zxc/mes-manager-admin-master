package com.youlai.system.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.model.entity.ParameterConfig;
import com.youlai.system.model.entity.TrendConfig;
import com.youlai.system.model.vo.ParameterConfigPageVO;
import com.youlai.system.model.vo.TrendConfigPageVO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ParameterConfigConverter {
    Page<ParameterConfigPageVO> entity2Page(Page<ParameterConfig> page);
}
