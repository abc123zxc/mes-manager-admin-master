package com.youlai.system.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.model.entity.EventText;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EventTextConfigConverter {
    Page<EventText> entity2Page(Page<EventText> page);
}
