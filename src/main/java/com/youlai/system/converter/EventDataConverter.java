package com.youlai.system.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.model.entity.EventData;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EventDataConverter {
    Page<EventData> entity2Page(Page<EventData> page);
}
