package com.youlai.system.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.model.entity.TaskConfig;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TaskConfigConverter {
    Page<TaskConfig> entity2Page(Page<TaskConfig> page);
}
