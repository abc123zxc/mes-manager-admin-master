package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.SysNodeList;
import com.youlai.system.model.enums.OPCDataType;
import com.youlai.system.model.query.NodeListQuery;

import java.util.List;

/**
* @author Guijiang.Lai
* @description 针对表【sys_node_list】的数据库操作Service
* @createDate 2024-12-08 14:51:01
*/
public interface SysNodeListService extends IService<SysNodeList> {

    boolean deleteOPCNode(String idsStr);

    Page<SysNodeList> getOPCNodePage(NodeListQuery nodeListQuery);

    List<SysNodeList> getOPCNodeByDataType(OPCDataType opDataType);


    boolean saveOPCNodeList(List<SysNodeList> sysNodeLists);

}
