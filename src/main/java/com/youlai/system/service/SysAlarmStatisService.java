package com.youlai.system.service;

import com.youlai.system.model.entity.SysAlarmStatis;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Guijiang.Lai
* @description 针对表【sys_alarm_statis】的数据库操作Service
* @createDate 2024-10-02 17:24:48
*/
public interface SysAlarmStatisService extends IService<SysAlarmStatis> {

    SysAlarmStatis getAlarmStatistByTime(String currentDate);

//    boolean save(SysAlarmStatis sysAlarmStatis);
}
