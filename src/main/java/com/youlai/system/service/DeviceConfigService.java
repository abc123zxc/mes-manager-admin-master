package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.DeviceConfig;
import com.youlai.system.model.query.DeviceConfigQuery;

import java.util.List;

public interface DeviceConfigService extends IService<DeviceConfig> {

    boolean deleteDeviceConfig(String idsStr);
    Page<DeviceConfig> getDeviceConfigPage(DeviceConfigQuery deviceConfigQuery);

    boolean saveDeviceConfigList(List<DeviceConfig> deviceConfigList);

    boolean connectPlc(Long id) throws Exception;

    boolean connectAllPLC();

    boolean disconnectPlc(Long id);

    boolean subscribePlc(Long id) throws Exception;

    boolean cancelSubscription(Long id) throws Exception;

    boolean saveTrendConfigList(List<DeviceConfig> deviceConfigList);



}
