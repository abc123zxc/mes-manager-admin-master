package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.TrendConfig;
import com.youlai.system.model.query.TrendConfigQuery;

import java.util.List;


public interface TrendConfigService extends IService<TrendConfig> {

    boolean deleteTrendConfig(String idsStr);

    List<TrendConfig> getTrendByIds(String idsStr);


    Page<TrendConfig> getTrendConfigPage(TrendConfigQuery trendConfigQuery);

    TrendConfig getTrendConfigByAddress(String address);

    boolean updateTrendConfig(TrendConfig trendConfig);
    boolean deleteTrend(String ids);
    boolean saveTrendConfig(TrendConfig trendConfig);

    boolean saveTrendConfigList(List<TrendConfig> trendConfigList);

}
