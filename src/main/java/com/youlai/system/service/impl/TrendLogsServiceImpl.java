package com.youlai.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.mapper.TrendLogsMapper;
import com.youlai.system.model.entity.TrendLogs;
import com.youlai.system.model.query.TrendLogsQuery;
import com.youlai.system.model.vo.TrendLogsVO;
import com.youlai.system.service.TrendLogsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TrendLogsServiceImpl extends ServiceImpl<TrendLogsMapper, TrendLogs> implements TrendLogsService {


    @Override
    public TrendLogs getTrendLogsByAddress(String address) {
        return this.baseMapper.getTrendLogsByAddress(address);
    }

    @Override
    public List<TrendLogs> getTrendLogsByDevice(String device) {
        return this.baseMapper.getTrendLogsByDevice(device);
    }

    @Override
    public List<TrendLogsVO> listOfTrend(TrendLogsQuery trendLogsQuery) {
        if(StrUtil.isBlank(trendLogsQuery.getAddressIds()) ) return null;
        var trendLogsVOList = new ArrayList<TrendLogsVO>();
        var addressIds = trendLogsQuery.getAddressIds().split(",");
        for(var address : addressIds){
            var item = listOfTrend(address,trendLogsQuery.getStartTime() , trendLogsQuery.getEndTime());
            if(item == null ) continue;
            trendLogsVOList.add(item);
        }
        return trendLogsVOList;
    }


    @Override
    public TrendLogsVO listOfTrend(String address, String startTime, String endTime) {
        var trend = this.baseMapper.getTrendLogsByAddress(address);
        System.out.println(trend);
        if(trend != null){
            LambdaQueryWrapper<TrendLogs> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(TrendLogs::getAddress, address)
                    .ge(TrendLogs::getTime, startTime)
                    .le(TrendLogs::getTime, endTime);
            var trendLogList = this.list(queryWrapper);
            var trendLogVO = new TrendLogsVO();
            trendLogVO.setTrendLogs(trend);
            trendLogVO.setTrendLogsList(trendLogList);
            return trendLogVO;
        }
        return null;
    }
    @Override
    public TrendLogsVO listOfTrend(String address) {
//        var device = this.baseMapper.getTrendLogsByAddress(address);
//        if(device != null){
//            LambdaQueryWrapper<TrendLogs> queryWrapper = new LambdaQueryWrapper<>();
//            queryWrapper.eq(TrendLogs::getAddress, address);
//            var trendLogList = this.list(queryWrapper);
//            var trendLogVO = new TrendLogsVO();
//            trendLogVO.setTrendLogs(device);
//            trendLogVO.setTrendLogsList(trendLogList);
//            return trendLogVO;
//        }
        return null;
    }
}
