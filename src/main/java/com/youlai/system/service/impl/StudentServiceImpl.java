package com.youlai.system.service.impl;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.common.constant.SystemConstants;
import com.youlai.system.mapper.StudentMapper;
import com.youlai.system.model.entity.Student;
import com.youlai.system.model.entity.SysUser;
import com.youlai.system.service.StudentService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class StudentServiceImpl  extends ServiceImpl<StudentMapper, Student> implements StudentService {

    @Override
    public List<Student> getAllStudents() {
        return this.baseMapper.getAllStudents();
    }

    @Override
    public Student getStudentById(Long id) {
        return this.baseMapper.getStudentById(id) ;
    }

    @Override
    public void insertStudent(Student student) {
        this.baseMapper.insertStudent(student);
    }

    @Override
    public void updateStudent(Student student) {
        this.baseMapper.updateStudent(student);
    }

    @Override
    public void deleteStduent(Student student) {
        this.baseMapper.deleteStudent(student);
    }
}
