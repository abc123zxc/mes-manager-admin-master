package com.youlai.system.service.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.converter.DMConfigConverter;
import com.youlai.system.mapper.DMConfigMapper;
import com.youlai.system.model.entity.DMConfig;
import com.youlai.system.model.query.DMConfigQuery;
import com.youlai.system.service.DMConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DMConfigServiceImpl extends ServiceImpl<DMConfigMapper, DMConfig> implements DMConfigService {

    private final DMConfigConverter dmConfigConverter;

    // 在类的静态块中初始化映射
    private static final Map<Integer, String> TYPE_PREFIX_MAP = new HashMap<>();
    static {
        TYPE_PREFIX_MAP.put(1, "E");
        TYPE_PREFIX_MAP.put(2, "W");
        TYPE_PREFIX_MAP.put(3, "F");
    }

    @Override
    public boolean deleteDMConfig(String idsStr) {
        Assert.isTrue(StrUtil.isNotBlank(idsStr), "删除的数据为空");
        // 逻辑删除
        List<Long> ids = Arrays.stream(idsStr.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());
        return this.removeByIds(ids);
    }

    @Override
    public List<DMConfig> getDMByIds(String idsStr) {
        Assert.isTrue(StrUtil.isNotBlank(idsStr), "获取的数据为空");

        List<Long> ids = Arrays.stream(idsStr.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());

        List<DMConfig> texts = new ArrayList<>();
        for (Long id : ids) {
            DMConfig dmConfig = getById(id);
            if (dmConfig != null) {
                texts.add(dmConfig);
            }
        }

        return texts;
    }

    @Override
    public DMConfig getDMByAddress(String address) {

        LambdaQueryWrapper<DMConfig> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(StrUtil.isNotBlank(address),DMConfig::getAddress,address);

        return this.getOne(lambdaQueryWrapper);
    }

    @Override
    public Page<DMConfig> getDMConfigPage(DMConfigQuery query) {
        int pageNum = query.getPageNum();
        int pageSize = query.getPageSize();
        String keywords = query.getKeywords();
        Integer type = query.getType();
        Boolean enable = query.getEnable();

        LambdaQueryWrapper<DMConfig> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        lambdaQueryWrapper.like(StrUtil.isNotBlank(keywords),DMConfig::getZhCn,keywords);
        lambdaQueryWrapper.like(StrUtil.isNotBlank(keywords),DMConfig::getEnUs,keywords);
        lambdaQueryWrapper.eq(DMConfig::getEnable,enable);
        //条件筛选
        if (type != null && type != 0) {
            String prefix = TYPE_PREFIX_MAP.get(type);
            if(prefix != null){
                lambdaQueryWrapper.like(DMConfig::getTextId,prefix);
            }
        }

        Page<DMConfig> eventTextPage = this.page(
                new Page<>(pageNum,pageSize),
                lambdaQueryWrapper

        );

        Page<DMConfig> pageResult =  dmConfigConverter.entity2Page(eventTextPage);
        return pageResult;


    }
}
