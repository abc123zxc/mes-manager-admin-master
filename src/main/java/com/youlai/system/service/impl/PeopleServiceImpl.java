package com.youlai.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.mapper.PeopleMapper;
import com.youlai.system.model.entity.People;
import com.youlai.system.service.PeopleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeopleServiceImpl extends ServiceImpl<PeopleMapper, People> implements PeopleService {


    @Override
    public List<People> getAllPeople() {
        return this.baseMapper.getAllPeople();
    }

    @Override
    public People getPeopleById(Long id) {
        return this.baseMapper.getPeopleById(id);
    }
}
