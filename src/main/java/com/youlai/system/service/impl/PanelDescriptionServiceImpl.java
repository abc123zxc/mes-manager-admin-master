package com.youlai.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.common.util.RedisUtil;
import com.youlai.system.mapper.PanelDescriptionMapper;
import com.youlai.system.model.entity.PanelDescription;
import com.youlai.system.model.entity.TrendConfig;
import com.youlai.system.model.query.PanelDataQuery;
import com.youlai.system.model.query.PanelSelectListQuery;
import com.youlai.system.model.vo.PanelDataVO;
import com.youlai.system.model.vo.PanelSelectListVO;
import com.youlai.system.service.PanelDescriptionService;
import com.youlai.system.service.TrendConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PanelDescriptionServiceImpl extends ServiceImpl<PanelDescriptionMapper, PanelDescription> implements PanelDescriptionService {

    private final RedisUtil redisUtil;
    private final TrendConfigService trendConfigService;

    @Override
    public List<PanelSelectListVO> getPanelSelectList(PanelSelectListQuery query) {

        List<PanelSelectListVO> selectListVOList = new ArrayList<>();
        LambdaQueryWrapper<TrendConfig> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TrendConfig::getDevice, query.getDevice())
                .ge(TrendConfig::getType, query.getType());
        List<TrendConfig> trendConfigList = trendConfigService.list(queryWrapper);
        for(TrendConfig item: trendConfigList){
            PanelSelectListVO panelSelectListVO = new PanelSelectListVO();
            panelSelectListVO.setDescription(item.getDescription());
            panelSelectListVO.setId(item.getId());
            panelSelectListVO.setType(item.getType());
            panelSelectListVO.setEnable(item.getEnable());
            panelSelectListVO.setAddress(item.getAddress());
            panelSelectListVO.setUnit(item.getUnit());
            selectListVOList.add(panelSelectListVO);
        }
        return selectListVOList;
    }

    @Override
    public List<PanelDataVO> getPanelData(PanelDataQuery query) {
        return null;
    }
}
