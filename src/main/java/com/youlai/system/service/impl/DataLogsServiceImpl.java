package com.youlai.system.service.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.mapper.DataLogsMapper;
import com.youlai.system.model.entity.Account;
import com.youlai.system.model.entity.DataLogs;
import com.youlai.system.model.query.DataLogsQuery;
import com.youlai.system.model.vo.DataLogsVO;
import com.youlai.system.service.DataLogsService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DataLogsServiceImpl extends ServiceImpl<DataLogsMapper, DataLogs> implements DataLogsService {

    @Resource
    private DataLogsMapper dataLogsMapper;

    @Override
    public List<DataLogsVO> getDataLog(DataLogsQuery dataLogsQuery) {
        // 查询参数
        Integer deviceId = dataLogsQuery.getDevice_id();
        String startTime = dataLogsQuery.getStartTime();
        String endTime = dataLogsQuery.getEndTime();

        if (StringUtils.isEmpty(startTime) || StringUtils.isEmpty(endTime)) {
            throw new IllegalArgumentException("查询的起始时间和结束时间不能为空");
        }

        LambdaQueryWrapper<DataLogs> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DataLogs::getDevice_id, deviceId)
                .ge(DataLogs::getTime, startTime)
                .le(DataLogs::getTime, endTime);

        List<DataLogs> logs = dataLogsMapper.selectList(queryWrapper);

        // 将查询结果转换为 DataLogsVO 列表
        List<DataLogsVO> voList = new ArrayList<>();
        for (DataLogs log : logs) {
            DataLogsVO vo = new DataLogsVO();
            vo.setValue(Float.valueOf(log.getValue()));
            vo.setTime(log.getTime());
            voList.add(vo);
        }

        return voList;
    }
}
