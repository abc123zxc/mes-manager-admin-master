package com.youlai.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.converter.TaskConfigConverter;
import com.youlai.system.mapper.TaskScheduleMapper;
import com.youlai.system.model.entity.TaskConfig;
import com.youlai.system.model.query.TaskConfigQuery;
import com.youlai.system.service.TaskScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class TaskScheduleServiceImpl extends ServiceImpl<TaskScheduleMapper, TaskConfig>
    implements TaskScheduleService{

    private final TaskConfigConverter taskConfigConverter;


    @Override
    public Page<TaskConfig> getTaskConfigPage(TaskConfigQuery query) {
        int pageNum = query.getPageNum();
        int pageSize = query.getPageSize();
        String keywords = query.getKeywords();
        //Boolean enable = query.getEnable();

        Page<TaskConfig> TaskConfigPage = this.page(
                new Page<>(pageNum,pageSize),
                new LambdaQueryWrapper<TaskConfig>()
                        .like(StrUtil.isNotBlank(keywords),TaskConfig::getName,keywords)
                        //.eq(TaskConfig::getEnabled,enable)
        );

        return taskConfigConverter.entity2Page(TaskConfigPage);
    }


}




