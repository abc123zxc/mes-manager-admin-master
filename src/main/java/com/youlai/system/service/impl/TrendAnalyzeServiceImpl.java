package com.youlai.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.common.util.RedisUtil;
import com.youlai.system.mapper.TrendAnalyzeMapper;
import com.youlai.system.model.entity.RedisNode;
import com.youlai.system.model.entity.TrendAnalyze;
import com.youlai.system.model.entity.TrendConfig;
import com.youlai.system.model.query.TrendAnalyzeQuery;
import com.youlai.system.model.vo.TrendAnalyzeScrollListVO;
import com.youlai.system.plugin.opcua.OpcUaHelper;
import com.youlai.system.service.TrendAnalyzeService;
import com.youlai.system.service.TrendConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TrendAnalyzeServiceImpl extends ServiceImpl<TrendAnalyzeMapper, TrendAnalyze> implements TrendAnalyzeService {

    private final RedisUtil redisUtil;
    private final TrendConfigService trendConfigService;
    private final OpcUaHelper opcUaHelper;
    @Override
    public TrendAnalyze getTrendAnalyzeData(TrendAnalyzeQuery query) {
        TrendAnalyze trendAnalyze = new TrendAnalyze();
        String redisName = opcUaHelper.getRedisName(query.getDevice(),query.getAddress());
        if(query.getAddress() != null) {
            //获取Redis数据并且返回
            RedisNode redisNode = (RedisNode) redisUtil.get(redisName);
            trendAnalyze.setUnit(redisNode.getTrendConfig().getUnit());
            trendAnalyze.setValue(redisNode.getCurrentValue());
        }else{
            return null;
        }
        return trendAnalyze;
    }

    @Override
    public List<TrendAnalyzeScrollListVO> getTrendAnalyzeScroll(String description) {
        //去掉多余的双引号
        description = description.replace("\"", "");
        List<TrendAnalyzeScrollListVO> scrollListVOList = new ArrayList<>();
        LambdaQueryWrapper<TrendConfig>  queryWrapper= new LambdaQueryWrapper<>();
        // 如果 description 不为空，则添加筛选条件
        if (description != null && !description.trim().isEmpty() && !description.equals("\"\"")) {
            queryWrapper.like(TrendConfig::getDescription, description);
        }
        List<TrendConfig>  trendConfigList = trendConfigService.list(queryWrapper);
        for(TrendConfig item :  trendConfigList){
            TrendAnalyzeScrollListVO scrollListVO = new TrendAnalyzeScrollListVO();
            scrollListVO.setId(item.getId());
            scrollListVO.setDescription(item.getDescription());
            scrollListVO.setAddress(item.getAddress());
            scrollListVO.setDevice(item.getDevice());
            scrollListVOList.add(scrollListVO);
        }

        return scrollListVOList;
    }


}


