package com.youlai.system.service.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.converter.NodeListConverter;
import com.youlai.system.mapper.SysNodeListMapper;
import com.youlai.system.model.entity.SysNodeList;
import com.youlai.system.model.enums.OPCDataType;
import com.youlai.system.model.query.NodeListQuery;
import com.youlai.system.service.SysNodeListService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
* @author Guijiang.Lai
* @description 针对表【sys_node_list】的数据库操作Service实现
* @createDate 2024-12-08 14:51:01
*/
@Service
@RequiredArgsConstructor
public class SysNodeListServiceImpl extends ServiceImpl<SysNodeListMapper, SysNodeList>
    implements SysNodeListService{

    private final NodeListConverter nodeListConverter;

    @Override
    public boolean deleteOPCNode(String idsStr) {
        Assert.isTrue(StrUtil.isNotBlank(idsStr), "删除的用户数据为空");
        // 逻辑删除
        List<Long> ids = Arrays.stream(idsStr.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());
        return this.removeByIds(ids);
    }

    @Override
    public Page<SysNodeList> getOPCNodePage(NodeListQuery nodeListQuery) {
        int pageNum = nodeListQuery.getPageNum();
        int pageSize = nodeListQuery.getPageSize();
        String keywords = nodeListQuery.getKeywords();
        Boolean enable = nodeListQuery.getEnable();

        Page<SysNodeList> NodeListPage = this.page(
                new Page<>(pageNum,pageSize),
                new LambdaQueryWrapper<SysNodeList>()
                        .like(StrUtil.isNotBlank(keywords),SysNodeList::getDescription,keywords)
                        .eq(SysNodeList::getEnable,enable)
        );

        return nodeListConverter.entity2Page(NodeListPage);
    }

    @Override
    public List<SysNodeList> getOPCNodeByDataType(OPCDataType opDataType) {
        LambdaQueryWrapper<SysNodeList> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        switch (opDataType) {
            case NUMBER:
                // 使用 in 方法来表示 '或' 的关系
                lambdaQueryWrapper.in(SysNodeList::getDataType, Arrays.asList("INT", "DINT", "REAL"));
                break;
            case BOOL:
                // 假设 OPCDataType 有对应数据库中的字符串表示形式
                lambdaQueryWrapper.eq(SysNodeList::getDataType, opDataType.name());
                break;
            default:
                break;
        }

        return this.list(lambdaQueryWrapper);
    }

    @Override
    public boolean saveOPCNodeList(List<SysNodeList> sysNodeLists) {
        return false;
    }


}




