package com.youlai.system.service.impl;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.xiaoymin.knife4j.core.util.StrUtil;
import com.youlai.system.converter.EventDataConverter;
import com.youlai.system.mapper.EventDataMapper;
import com.youlai.system.model.entity.EventData;
import com.youlai.system.model.query.EventDataQuery;
import com.youlai.system.model.vo.AlarmCount;
import com.youlai.system.model.vo.AlarmCountEachNode;
import com.youlai.system.model.vo.EventDataBtnVO;
import com.youlai.system.model.vo.EventDataUpdateBatch;
import com.youlai.system.service.EventDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EventDataServiceImpl extends ServiceImpl<EventDataMapper, EventData> implements EventDataService {

    private final EventDataConverter eventDataConverter;


    @Override
    public Page<EventData> getEventDataPage(EventDataQuery query) {
        int pageNum = query.getPageNum();
        int pageSize = query.getPageSize();
        String keywords = query.getKeywords();
        Integer type = query.getType();
        String startTime = query.getStartTime();
        String endTime = query.getEndTime();
        Boolean confirm = query.getConfirm();
        LambdaQueryWrapper<EventData> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(EventData::getConfirm,confirm);
        if (StrUtil.isNotBlank(keywords)) {
            queryWrapper.like(EventData::getDescription, keywords)
                    .like(EventData::getDescription, keywords);
        }

        if (startTime != null) {
            queryWrapper.ge(EventData::getTime, startTime);
        }

        if (endTime != null) {
            queryWrapper.le(EventData::getTime, endTime);
        }

        if (type != null && type != 0) {
            queryWrapper.eq(EventData::getStatus, type);
        }

        Page<EventData> eventDataPage = this.page(
                new Page<>(pageNum,pageSize),
                queryWrapper
        );
        Page<EventData> pageResult = eventDataConverter.entity2Page(eventDataPage);
        return pageResult;
    }

    @Override
    public EventDataBtnVO getEventDataBtnVO() {
        EventDataBtnVO eventDataBtnVO = new EventDataBtnVO();

        // 使用 LambdaQueryWrapper 查询 type = 1 2 3 的记录数
        LambdaQueryWrapper<EventData> queryWrapper_Message = new LambdaQueryWrapper<>();
        queryWrapper_Message.eq(EventData::getStatus, 1);
        queryWrapper_Message.eq(EventData::getConfirm, false);
        LambdaQueryWrapper<EventData> queryWrapper_Warning = new LambdaQueryWrapper<>();
        queryWrapper_Warning.eq(EventData::getStatus, 2);
        queryWrapper_Warning.eq(EventData::getConfirm, false);
        LambdaQueryWrapper<EventData> queryWrapper_Fault= new LambdaQueryWrapper<>();
        queryWrapper_Fault.eq(EventData::getStatus, 3);
        queryWrapper_Fault.eq(EventData::getConfirm, false);

        eventDataBtnVO.setNumber_Message(baseMapper.selectCount(queryWrapper_Message));
        eventDataBtnVO.setNumber_Warning(baseMapper.selectCount(queryWrapper_Warning));
        eventDataBtnVO.setNumber_Fault(baseMapper.selectCount(queryWrapper_Fault));

        return eventDataBtnVO;
    }

    @Override
    public AlarmCount getAlarmCount(String startTime, String endTime) {

        //获取本月count  array[]  length = 31
        


        return null;
    }

    @Override
    public AlarmCountEachNode getAlarmCountEachNode(String startTime, String endTime) {
        return null;
    }

    @Override
    public boolean updateBatch(EventDataUpdateBatch eventDataUpdateBatch) {

        String userName = eventDataUpdateBatch.getUserName();
        String time = eventDataUpdateBatch.getTime();

        Assert.isTrue(StrUtil.isNotBlank(eventDataUpdateBatch.getIds()), "更新的数据为空");

        // 将字符串转换为 Long 类型的 ID 列表
        List<Long> ids = Arrays.stream(eventDataUpdateBatch.getIds().split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());



        List<EventData> eventDataList = this.listByIds(ids);
        eventDataList.forEach(eventData -> {
            eventData.setConfirmUser(userName);
            eventData.setConfirmTime(time);
            eventData.setConfirm(true);
        });

        // 批量保存更新后的记录
        this.saveOrUpdateBatch(eventDataList);

        return true;
    }
}
