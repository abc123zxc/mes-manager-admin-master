package com.youlai.system.service.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.converter.EventTextConfigConverter;
import com.youlai.system.mapper.EventTextConfigMapper;
import com.youlai.system.model.entity.EventText;
import com.youlai.system.model.query.EventTextQuery;
import com.youlai.system.service.EventTextConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EventTextConfigServiceImpl extends ServiceImpl<EventTextConfigMapper, EventText> implements EventTextConfigService {

    private final EventTextConfigConverter eventTextConfigConverter;

    // 在类的静态块中初始化映射
    private static final Map<Integer, String> TYPE_PREFIX_MAP = new HashMap<>();
    static {
        TYPE_PREFIX_MAP.put(1, "E");
        TYPE_PREFIX_MAP.put(2, "W");
        TYPE_PREFIX_MAP.put(3, "F");
    }


    @Override
    public boolean deleteEventTextConfig(String idsStr) {
        Assert.isTrue(StrUtil.isNotBlank(idsStr), "删除的用户数据为空");
        // 逻辑删除
        List<Long> ids = Arrays.stream(idsStr.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());
        return this.removeByIds(ids);
    }

    @Override
    public List<EventText> getEventTextByIds(String idsStr) {
        Assert.isTrue(StrUtil.isNotBlank(idsStr), "获取的用户数据为空");

        List<Long> ids = Arrays.stream(idsStr.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());

        List<EventText> texts = new ArrayList<>();
        for (Long id : ids) {
            EventText eventText = getById(id);
            if (eventText != null) {
                texts.add(eventText);
            }
        }

        return texts;
    }

    @Override
    public EventText getEventTextByTextId(String textId) {
        LambdaQueryWrapper<EventText> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(StrUtil.isNotBlank(textId),EventText::getTextId,textId);
        return this.getOne(lambdaQueryWrapper);
    }

    @Override
    public Page<EventText> getEventTextConfigPage(EventTextQuery query) {
        int pageNum = query.getPageNum();
        int pageSize = query.getPageSize();
        String keywords = query.getKeywords();
        Integer type = query.getType();

        LambdaQueryWrapper<EventText> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        lambdaQueryWrapper.like(StrUtil.isNotBlank(keywords),EventText::getZhCn,keywords);

        //条件筛选
        if (type != null && type != 0) {
            String prefix = TYPE_PREFIX_MAP.get(type);
            if(prefix != null){
                lambdaQueryWrapper.like(EventText::getTextId,prefix);
            }
        }
        Page<EventText> eventTextPage = this.page(
                new Page<>(pageNum,pageSize),
                lambdaQueryWrapper

        );

        Page<EventText> pageResult =  eventTextConfigConverter.entity2Page(eventTextPage);
        return pageResult;


    }
}
