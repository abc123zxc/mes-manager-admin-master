package com.youlai.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.common.util.CommUtils;
import com.youlai.system.global.GlobalData;
import com.youlai.system.mapper.TimeProgramMapper;
import com.youlai.system.model.Property.TimeProgramProperty;
import com.youlai.system.model.entity.TimeProgram;
import com.youlai.system.model.vo.OPCWriteNode;
import com.youlai.system.model.vo.TimeProgramScrollListVO;
import com.youlai.system.service.TimeProgramService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
* @author Guijiang.Lai
* @description 针对表【sys_time_program】的数据库操作Service实现
* @createDate 2024-10-22 19:14:05
*/
@Service
@RequiredArgsConstructor
public class TimeProgramServiceImpl extends ServiceImpl<TimeProgramMapper, TimeProgram>
    implements TimeProgramService{

    private final TimeProgramProperty timeProgramProperty;

    @Override
    public List<TimeProgramScrollListVO> getTimeProgramScroll(String description) {
        //去掉多余的双引号
        description = description.replace("\"", "");
        List<TimeProgramScrollListVO> scrollListVOList = new ArrayList<>();
        LambdaQueryWrapper<TimeProgram> queryWrapper= new LambdaQueryWrapper<>();
        // 如果 description 不为空，则添加筛选条件
        if (description != null && !description.trim().isEmpty() && !description.equals("\"\"")) {
            queryWrapper.like(TimeProgram::getName, description);
        }
        List<TimeProgram>  timeProgramList = this.list(queryWrapper);
        for(TimeProgram item :  timeProgramList){
            TimeProgramScrollListVO scrollListVO = new TimeProgramScrollListVO();
            scrollListVO.setId(item.getId());
            scrollListVO.setDescription(item.getDescription());
            scrollListVO.setTime(item.getTime());
            scrollListVO.setIndex(item.getIndex());
            scrollListVO.setName(item.getName());
            scrollListVOList.add(scrollListVO);
        }

        return scrollListVOList;
    }

    @Override
    public Boolean saveAndDownloadById(TimeProgram data) {

        //1、更新到数据库
        Boolean status =  this.updateById(data);

        //2、下载到PLC

        /*
        "TimeProgram".Data[2].Name
        "TimeProgram".Data[2].StepTime
        "TimeProgram".Data[2].Step
        "TimeProgram".Data[2].Description
        "TimeProgram".Data[2].Data[1]
         */

        List<OPCWriteNode> opcWriteNodeList = new ArrayList<>();
        Integer DBIndex = data.getIndex();

        //基础数据
        String OPCPrefix = timeProgramProperty.getOPCPrefix();
        String DBName = timeProgramProperty.getDBName_Content();
        String DBStatic = timeProgramProperty.getDBStatic_Content();
        String DataContent = timeProgramProperty.getDBData_Content();
        Integer StepLength = timeProgramProperty.getDBStep_Length();
        Integer DBLength = timeProgramProperty.getDB_Length();
//        String OPCPrefix = "ns=3;s=";
//        String DBName = "TimeProgram";
//        String DBStatic = "Data";
//        String DataContent = "Data";
//        Integer StepLength = 150;
//        Integer DBLength = 100;


        //节点拼凑
        // 使用 String.format 来拼凑节点
        String Node_StepTime = String.format("%s\"%s\".\"%s\"[%d].\"StepTime\"", OPCPrefix, DBName, DBStatic, DBIndex);
        String Node_Step = String.format("%s\"%s\".\"%s\"[%d].\"Step\"", OPCPrefix, DBName, DBStatic, DBIndex);

        // 基础数据
        OPCWriteNode opcWriteNode1 = new OPCWriteNode();
        opcWriteNode1.setNodeId(Node_StepTime);
        opcWriteNode1.setValue(String.valueOf(data.getStep()));
        opcWriteNode1.setValueType(Short.class);
        opcWriteNodeList.add(opcWriteNode1);

        OPCWriteNode opcWriteNode2 = new OPCWriteNode();
        opcWriteNode2.setNodeId(Node_Step);
        opcWriteNode2.setValue(String.valueOf(StepLength));
        opcWriteNode2.setValueType(Short.class);
        opcWriteNodeList.add(opcWriteNode2);

        //时间程序data 数据
        String[] values = data.getData().split(",");

        // 循环写入从 1 到 150 的节点
        for (int index = 0; index < values.length; index++) {

            OPCWriteNode tempOPCWriteNode = new OPCWriteNode();

            int valueIndex = index ;
            String currentValue = values[valueIndex];
            String Node_Data = ""+OPCPrefix+"\""+ DBName +"\".\""+DBStatic+"\"[" + DBIndex +"].\""+DataContent+"\"[" + valueIndex +"]";
//            String Node_Data = String.format("%s%s\".\"%s\"[%d].\"%s\"[%d]\"", OPCPrefix, DBName, DBStatic, DBIndex, DBStatic, valueIndex);
            tempOPCWriteNode.setValueType(Short.class);
            tempOPCWriteNode.setNodeId(Node_Data);
            tempOPCWriteNode.setValue(currentValue);

            opcWriteNodeList.add(tempOPCWriteNode);
        }


        return GlobalData.opcUaHelper.writeValuesAsync(opcWriteNodeList).join() && status;
    }



//    public Boolean saveAndDownloadById(TimeProgram data) {
//
//        //1、更新到数据库
////        Boolean status =  this.updateById(data);
//
////        System.out.println();timeProgramProperty.getMyProperty();
//
//        return  true;
//    }

    @Override
    public Boolean saveTimeProgram(TimeProgram data) {
        String x_Data = CommUtils.generateNumberSequence(1,150);
        String data_Data = CommUtils.generateZeroSequence(150);
        TimeProgram timeProgram = new TimeProgram();
        timeProgram.setName(data.getName());
        timeProgram.setX(x_Data);
        timeProgram.setY(data.getY());
        timeProgram.setIndex(data.getIndex());
        timeProgram.setStep(data.getStep());
        timeProgram.setData(data_Data);
        timeProgram.setDescription(data.getDescription());
        return save(timeProgram);
    }

}




