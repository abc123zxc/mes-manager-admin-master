package com.youlai.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.common.util.RedisUtil;
import com.youlai.system.mapper.ParameterPanelMapper;
import com.youlai.system.model.entity.ParameterPanel;
import com.youlai.system.model.entity.RedisNode;
import com.youlai.system.model.entity.TrendConfig;
import com.youlai.system.plugin.opcua.OpcUaHelper;
import com.youlai.system.service.ParameterPanelService;
import com.youlai.system.service.TrendConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ParameterPanelImpl extends ServiceImpl<ParameterPanelMapper, ParameterPanel> implements ParameterPanelService {

    private final TrendConfigService trendConfigService;

    private final RedisUtil redisUtil;

    private final OpcUaHelper opcUaHelper;

    @Override
    public List<ParameterPanel> getAllParameterPanelData() {
        List<ParameterPanel> parameterPanelList = new ArrayList<>();
        List<TrendConfig> trendConfigList;
        try{
            //获取数据库所有trend
            trendConfigList = trendConfigService.list();
            //获取redis数据
            for (TrendConfig trendConfig : trendConfigList){
                ParameterPanel parameterPanel = new ParameterPanel();
                String redisName = opcUaHelper.getRedisName(trendConfig.getDevice(),trendConfig.getAddress());
                RedisNode redisData = (RedisNode) redisUtil.get(redisName);
                parameterPanel.setUnit(redisData.getTrendConfig().getUnit());
                parameterPanel.setDescription(redisData.getTrendConfig().getDescription());
                parameterPanel.setEnable(redisData.getTrendConfig().getEnable());
                parameterPanel.setValue(redisData.getCurrentValue());
                parameterPanel.setDevice(redisData.getTrendConfig().getDevice());
                parameterPanelList.add(parameterPanel);
            }
        }catch (Exception e){
            throw new RuntimeException("更新故障：",e);
        }
        return  parameterPanelList;
    }
}
