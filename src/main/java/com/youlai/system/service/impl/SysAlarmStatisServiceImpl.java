package com.youlai.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.model.entity.SysAlarmStatis;
import com.youlai.system.service.SysAlarmStatisService;
import com.youlai.system.mapper.SysAlarmStatisMapper;
import org.springframework.stereotype.Service;

/**
* @author Guijiang.Lai
* @description 针对表【sys_alarm_statis】的数据库操作Service实现
* @createDate 2024-10-02 17:24:48
*/
@Service
public class SysAlarmStatisServiceImpl extends ServiceImpl<SysAlarmStatisMapper, SysAlarmStatis>
    implements SysAlarmStatisService{


    @Override
    public SysAlarmStatis getAlarmStatistByTime(String currentDate) {
        LambdaQueryWrapper<SysAlarmStatis> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(SysAlarmStatis::getTime,currentDate);
        return this.getOne(lambdaQueryWrapper);
    }

//    @Override
//    public boolean save(SysAlarmStatis sysAlarmStatis) {
//
//        this.save()
//        return false;
//    }
}




