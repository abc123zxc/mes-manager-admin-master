package com.youlai.system.service.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.converter.ParameterConfigConverter;
import com.youlai.system.global.GlobalData;
import com.youlai.system.mapper.ParameterConfigMapper;
import com.youlai.system.model.entity.ParameterConfig;
import com.youlai.system.model.query.ParameterConfigQuery;
import com.youlai.system.model.vo.OPCWriteNode;
import com.youlai.system.model.vo.ParameterConfigPageVO;
import com.youlai.system.service.ParameterConfigService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ParameterConfigServiceImpl extends ServiceImpl<ParameterConfigMapper, ParameterConfig> implements ParameterConfigService {

    private final ParameterConfigConverter parameterConfigConverter;

    public ParameterConfigServiceImpl(ParameterConfigConverter parameterConfigConverter) {
        this.parameterConfigConverter = parameterConfigConverter;
    }

    @Override
    public boolean deleteParameterConfig(String idsStr) {
        Assert.isTrue(StrUtil.isNotBlank(idsStr), "删除的用户数据为空");
        // 逻辑删除
        List<Long> ids = Arrays.stream(idsStr.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());
        return this.removeByIds(ids);
    }

    @Override
    public List<ParameterConfig> getParameterByIds(String idsStr) {
        Assert.isTrue(StrUtil.isNotBlank(idsStr), "获取的用户数据为空");

        // 将字符串ID列表转换为Long列表
        List<Long> ids = Arrays.stream(idsStr.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());

        // 逐个获取TrendConfig对象
        List<ParameterConfig> configs = new ArrayList<>();
        for (Long id : ids) {
            ParameterConfig config = getById(id);
            if (config != null) {
                configs.add(config);
            }
        }

        return configs;
    }

    /**
     * 将参数配置列表写入到PLC。
     *
     * @param ids 参数配置ID字符串，逗号分隔
     */
    @Override
    public boolean writeParameterConfigsToPLC(String ids) {
        List<ParameterConfig> parameterConfigList = getParameterByIds(ids);
        List<OPCWriteNode> opcWriteNodeList = new ArrayList<>();
        parameterConfigList.forEach(config -> {
            OPCWriteNode opcWriteNode = new OPCWriteNode();
            opcWriteNode.setNodeId(GlobalData.opcUaHelper.FormatNodeId(config.getAddress()));
            opcWriteNode.setValue(config.getParameter());
            opcWriteNode.setValueType(getValueClass(config.getType()));
            opcWriteNodeList.add(opcWriteNode);
        });

        return GlobalData.opcUaHelper.writeValuesAsync(opcWriteNodeList).join();
    }

//    private Class<?> getValueClass(String type) {
//        if ("int".equalsIgnoreCase(type)) {
//            return Short.class;
//        } else if ("real".equalsIgnoreCase(type)) {
//            return Float.class;
//        }
//        return Object.class; // Default or unknown type
//    }
private Class<?> getValueClass(String type) {
    if ("int".equalsIgnoreCase(type)) {
        return Short.class; // 假设 "int" 对应 16 位整数 (Int16)
    } else if ("real".equalsIgnoreCase(type)) {
        return Float.class;
    } else if ("bool".equalsIgnoreCase(type)) {
        return Boolean.class; // 添加对 bool 的支持
    } else if ("word".equalsIgnoreCase(type)) {
        return Short.class; // 假设 word 是 16 位无符号整数 (UInt16)，使用 Integer.class 更合适
    } else if ("byte".equalsIgnoreCase(type)) {
        return Byte.class; // 添加对 byte 的支持
    }
    return Object.class; // 默认或未知类型
}

    public Page<ParameterConfigPageVO> getParameterConfigPage(ParameterConfigQuery parameterConfigQuery){

        int pageNum = parameterConfigQuery.getPageNum();
        int pageSize = parameterConfigQuery.getPageSize();
        String keywords = parameterConfigQuery.getKeywords();
        Boolean enable = parameterConfigQuery.getEnable();

        Page<ParameterConfig> parameterConfigPage = this.page(
                new Page<>(pageNum,pageSize),
                new LambdaQueryWrapper<ParameterConfig>()
                        .like(StrUtil.isNotBlank(keywords),ParameterConfig::getDescription,keywords)
                        .eq(ParameterConfig::getEnable,enable)
        );

        Page<ParameterConfigPageVO> pageResult =  parameterConfigConverter.entity2Page(parameterConfigPage);
        return pageResult;



    }
}
