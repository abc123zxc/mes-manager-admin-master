package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.People;

import java.util.List;

public interface PeopleService extends IService<People> {

    List<People> getAllPeople();
    People getPeopleById(Long id);


}
