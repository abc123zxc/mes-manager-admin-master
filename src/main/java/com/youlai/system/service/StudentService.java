package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.Student;
import com.youlai.system.model.entity.SysUser;

import java.util.List;

public interface StudentService extends IService<Student> {
    List<Student> getAllStudents();
    Student getStudentById(Long id);
    void insertStudent(Student student);
    void updateStudent(Student student);
    void deleteStduent(Student  student);

}
