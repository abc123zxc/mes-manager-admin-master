package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.DMConfig;
import com.youlai.system.model.query.DMConfigQuery;

import java.util.List;

public interface DMConfigService extends IService<DMConfig> {
    boolean deleteDMConfig(String idsStr);

    List<DMConfig> getDMByIds(String idsStr);

    DMConfig getDMByAddress(String address);

    Page<DMConfig> getDMConfigPage(DMConfigQuery query);
}
