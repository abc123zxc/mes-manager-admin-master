package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.TrendLogs;
import com.youlai.system.model.query.TrendLogsQuery;
import com.youlai.system.model.vo.TrendLogsVO;

import java.util.List;


public interface TrendLogsService extends IService<TrendLogs> {
    TrendLogs getTrendLogsByAddress(String address);

    List<TrendLogs> getTrendLogsByDevice(String device);

    List<TrendLogsVO> listOfTrend(TrendLogsQuery trendLogsQuery);

    TrendLogsVO listOfTrend(String address, String startTime, String endTime);
    TrendLogsVO listOfTrend(String address);
}
