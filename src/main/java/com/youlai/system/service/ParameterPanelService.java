package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.ParameterPanel;

import java.util.List;


public interface ParameterPanelService extends IService<ParameterPanel> {

    List<ParameterPanel> getAllParameterPanelData();
}
