package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.TaskConfig;
import com.youlai.system.model.query.TaskConfigQuery;


public interface TaskScheduleService extends IService<TaskConfig> {

    Page<TaskConfig> getTaskConfigPage(TaskConfigQuery query);
}
