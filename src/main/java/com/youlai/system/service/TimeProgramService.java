package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.TimeProgram;
import com.youlai.system.model.vo.TimeProgramScrollListVO;

import java.util.List;

/**
* @author Guijiang.Lai
* @description 针对表【sys_time_program】的数据库操作Service
* @createDate 2024-10-22 19:14:05
*/
public interface TimeProgramService extends IService<TimeProgram> {
    List<TimeProgramScrollListVO> getTimeProgramScroll(String description);
    Boolean saveAndDownloadById(TimeProgram data);

    Boolean saveTimeProgram(TimeProgram data);
}
