package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.DataLogs;
import com.youlai.system.model.query.DataLogsQuery;
import com.youlai.system.model.vo.DataLogsVO;

import java.util.List;

public interface DataLogsService extends IService<DataLogs> {

    List<DataLogsVO> getDataLog (DataLogsQuery dataLogsQuery);
}
