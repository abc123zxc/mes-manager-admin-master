package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.EventText;
import com.youlai.system.model.query.EventTextQuery;

import java.util.List;

public interface EventTextConfigService extends IService<EventText> {
    boolean deleteEventTextConfig(String idsStr);

    List<EventText> getEventTextByIds(String idsStr);

    EventText getEventTextByTextId(String textId);
    Page<EventText> getEventTextConfigPage(EventTextQuery query);
}
