package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.TrendAnalyze;
import com.youlai.system.model.query.TrendAnalyzeQuery;
import com.youlai.system.model.vo.TrendAnalyzeScrollListVO;

import java.util.List;

public interface TrendAnalyzeService extends IService<TrendAnalyze> {

    TrendAnalyze  getTrendAnalyzeData(TrendAnalyzeQuery query);

    List<TrendAnalyzeScrollListVO> getTrendAnalyzeScroll(String description);
}
