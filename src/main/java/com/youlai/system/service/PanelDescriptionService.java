package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.PanelDescription;
import com.youlai.system.model.query.PanelDataQuery;
import com.youlai.system.model.query.PanelSelectListQuery;
import com.youlai.system.model.vo.PanelDataVO;
import com.youlai.system.model.vo.PanelSelectListVO;

import java.util.List;

public interface PanelDescriptionService extends IService<PanelDescription> {

    List<PanelSelectListVO> getPanelSelectList(PanelSelectListQuery query);

    List<PanelDataVO> getPanelData(PanelDataQuery query);
}
