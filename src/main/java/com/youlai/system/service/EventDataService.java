package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.EventData;
import com.youlai.system.model.query.EventDataQuery;
import com.youlai.system.model.vo.AlarmCount;
import com.youlai.system.model.vo.AlarmCountEachNode;
import com.youlai.system.model.vo.EventDataBtnVO;
import com.youlai.system.model.vo.EventDataUpdateBatch;


public interface EventDataService extends IService<EventData> {
    Page<EventData> getEventDataPage(EventDataQuery query);


    EventDataBtnVO getEventDataBtnVO();

    //通过日期获取全部
    AlarmCount getAlarmCount(String startTime, String endTime);

    //通过日期获取节点的
    AlarmCountEachNode getAlarmCountEachNode(String startTime, String endTime);

    //批量更新
    boolean updateBatch (EventDataUpdateBatch eventDataUpdateBatch);



}
