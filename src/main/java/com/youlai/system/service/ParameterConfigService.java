package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.ParameterConfig;
import com.youlai.system.model.query.ParameterConfigQuery;
import com.youlai.system.model.vo.ParameterConfigPageVO;

import java.util.List;

public interface ParameterConfigService extends IService<ParameterConfig> {

    boolean deleteParameterConfig(String idsStr);

    List<ParameterConfig> getParameterByIds(String idsStr);

    /**
     * 将参数配置列表写入到PLC。
     *
     * @param ids 参数配置ID字符串，逗号分隔
     */
    boolean writeParameterConfigsToPLC(String ids);
    Page<ParameterConfigPageVO> getParameterConfigPage(ParameterConfigQuery parameterConfigQuery);
}
