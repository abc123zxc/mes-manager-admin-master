package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.Account;
import com.youlai.system.model.query.AccountQuery;
import com.youlai.system.model.vo.AccountPageVO;

public interface AccountService extends IService<Account> {

    boolean deleteAccount(String idsStr);

    Page<AccountPageVO> getAccountPage(AccountQuery accountQuery);

}
