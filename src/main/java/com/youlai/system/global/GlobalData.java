package com.youlai.system.global;

import com.youlai.system.plugin.opcua.OpcUaHelper;

public class GlobalData {

    public static OpcUaHelper opcUaHelper;


   public enum Language {
        ZH_CN("ZH_CN"),
        EN_US("EN_US"),
        DE_DE("DE_DE"),
        KO_KR("KO_KR");

        private final String field;

        Language(String field) {
            this.field = field;
        }

        public String getField() {
            return field;
        }
    }


}

