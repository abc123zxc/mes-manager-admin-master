package com.youlai.system.global;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.youlai.system.helper.ApplicationContextHelper;
import com.youlai.system.model.entity.DMConfig;
import com.youlai.system.model.entity.DeviceConfig;
import com.youlai.system.model.entity.EventData;
import com.youlai.system.model.entity.TrendConfig;
import com.youlai.system.model.vo.BarChartDataVO;
import com.youlai.system.model.vo.PieChartDeviceNumberVO;
import com.youlai.system.model.vo.PieChartsDataVO;
import com.youlai.system.model.vo.TrendBigViewVO;
import com.youlai.system.service.*;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class BigView {

    private static  final DeviceConfigService deviceConfigService = ApplicationContextHelper.getBean(DeviceConfigService.class);
    private static final EventDataService eventDataService = ApplicationContextHelper.getBean(EventDataService.class);
    private static final DMConfigService  dmConfigService = ApplicationContextHelper.getBean(DMConfigService.class);
    private static final TrendConfigService trendConfigService = ApplicationContextHelper.getBean(TrendConfigService.class);
    private static final EventTextConfigService eventTextConfigService = ApplicationContextHelper.getBean(EventTextConfigService.class);
    private  static final  TimeProgramService timeProgramService = ApplicationContextHelper.getBean(TimeProgramService.class);
    private static final List<Integer> STATUSES = Arrays.asList(1, 2, 3); // 1:event, 2:warning, 3:fault

    /*
    获取LT-LF数据
     */
    public static List<TrendBigViewVO> getMonthlyEventCountsToArray(Integer id) {
        // 获取当前月份的第一天和最后一天
        LocalDate now = LocalDate.now();
        LocalDate firstDayOfMonth = now.withDayOfMonth(1);
        LocalDate lastDayOfMonth = now.withDayOfMonth(now.lengthOfMonth());

        // 生成本月每一天的日期列表
        List<LocalDate> dates = new ArrayList<>();
        for (LocalDate date = firstDayOfMonth; !date.isAfter(lastDayOfMonth); date = date.plusDays(1)) {
            dates.add(date);
        }

        // 查询本月每天的报警数量
        LambdaQueryWrapper<EventData> wrapper = new LambdaQueryWrapper<EventData>()
                .ge(EventData::getTime, firstDayOfMonth.atStartOfDay())
                .lt(EventData::getTime, lastDayOfMonth.plusDays(1).atStartOfDay())
                .select(EventData::getTime, EventData::getStatus);

        List<EventData> events = eventDataService.list(wrapper);

        // 统计每天的事件、警告和故障数量
        Map<LocalDate, Map<Integer, Integer>> countMap = new HashMap<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        for (EventData event : events) {
            LocalDateTime eventDateTime = LocalDateTime.parse(event.getTime(), formatter);
            LocalDate eventDate = eventDateTime.toLocalDate();
            int status = event.getStatus();

            countMap.putIfAbsent(eventDate, new HashMap<>());
            Map<Integer, Integer> statusCount = countMap.get(eventDate);
            statusCount.put(status, statusCount.getOrDefault(status, 0) + 1);
        }

        // 构建结果列表
        List<TrendBigViewVO> result = new ArrayList<>();

        // 定义状态列表
        List<Integer> statuses = Arrays.asList(id);

        for (Integer status : statuses) {
            TrendBigViewVO statusData = new TrendBigViewVO();
            statusData.setStatus(status);

            // 生成天数列表
            List<Integer> days = dates.stream()
                    .map(date -> date.getDayOfMonth())
                    .collect(Collectors.toList());
            statusData.setDay(days);

            // 生成数据列表
            List<Integer> data = dates.stream()
                    .map(date -> countMap.getOrDefault(date, Collections.emptyMap()).getOrDefault(status, 0))
                    .collect(Collectors.toList());
            statusData.setData(data);

            result.add(statusData);
        }

        return result;
    }

    /*
    获取PieCharts 数据 RB
     */

    public static PieChartsDataVO getMonthlyEventFaultWarningCounts() {
        // 获取当前月份的第一天和最后一天
        LocalDate now = LocalDate.now();
        LocalDate firstDayOfMonth = now.withDayOfMonth(1);
        LocalDate lastDayOfMonth = now.withDayOfMonth(now.lengthOfMonth());

        // 查询本月每天的报警数量
        LambdaQueryWrapper<EventData> wrapper = new LambdaQueryWrapper<EventData>()
                .ge(EventData::getTime, firstDayOfMonth.atStartOfDay())
                .lt(EventData::getTime, lastDayOfMonth.plusDays(1).atStartOfDay())
                .select(EventData::getTime, EventData::getStatus);

        List<EventData> events = eventDataService.list(wrapper);

        // 初始化状态计数器
        Map<Integer, Integer> statusCount = new HashMap<>();
        for (Integer status : STATUSES) {
            statusCount.put(status, 0);
        }

        // 统计每种状态的数量
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        for (EventData event : events) {
            int status = event.getStatus();
            if (STATUSES.contains(status)) {
                statusCount.put(status, statusCount.get(status) + 1);
            }
        }

        // 构建结果对象
        PieChartsDataVO result = new PieChartsDataVO();
        result.setEvent(statusCount.get(1));
        result.setWarning(statusCount.get(2));
        result.setFault(statusCount.get(3));

        return result;
    }

    /*
    获取RT数据 获取文本 离散量 模拟量 启用/未启用 数据
     */
    public static List<Integer> getVariableEnableAndDisEnableCount(){

        List<Integer> data = new ArrayList<>();
        Integer textCountOfEnable = 0;
        Integer textCountOfDisEnable = 0;
        Integer dmCountOfEnable = 0;
        Integer dmCountOfDisEnable = 0;
        Integer agCountOfEnable = 0;
        Integer agCountOfDisEnable = 0;

        //获取文本
        textCountOfEnable = (int) eventTextConfigService.count();

        textCountOfDisEnable = 0;

        //获取离散量
        LambdaQueryWrapper<DMConfig> dmEnable = new LambdaQueryWrapper<>();
        dmEnable.eq(DMConfig::getEnable,true);
        dmCountOfEnable = (int) dmConfigService.count(dmEnable);

        LambdaQueryWrapper<DMConfig> dmDisEnable = new LambdaQueryWrapper<>();
        dmDisEnable.eq(DMConfig::getEnable,false);
        dmCountOfDisEnable = (int) dmConfigService.count(dmDisEnable);

        //获取模拟量
        LambdaQueryWrapper<TrendConfig> agEnable = new LambdaQueryWrapper<>();
        agEnable.eq(TrendConfig::getEnable,true);
        agCountOfEnable = (int) trendConfigService.count(agEnable);

        LambdaQueryWrapper<TrendConfig> agDisEnable = new LambdaQueryWrapper<>();
        agDisEnable.eq(TrendConfig::getEnable,false);
        agCountOfDisEnable = (int) trendConfigService.count(agDisEnable);

        data.add(textCountOfEnable);
//        data.add(textCountOfDisEnable);
        data.add(dmCountOfEnable);
        data.add(dmCountOfDisEnable);
        data.add(agCountOfEnable);
        data.add(agCountOfDisEnable);

        return data;

    }


    /*
    获取 bar 数据
     */

    public static BarChartDataVO getBarChartTotalData(){

        List<BarChartDataVO> barChartDataVOList = new ArrayList<>();
        BarChartDataVO barChartDataVO = new BarChartDataVO();
        List<String> name = new ArrayList<>();
        List<Integer> data = new ArrayList<>();
        name.add("设备");
        name.add("文本");
        name.add("离散");
        name.add("模拟");
        name.add("程序");

        //获取设备程序数量
        data.add((int) deviceConfigService.count());
        //获取文本数量
        data.add((int) eventTextConfigService.count());
        //获取离散量数量
        data.add((int) dmConfigService.count());
        //获取模拟量数量
        data.add((int) trendConfigService.count());
        //获取时间程序数量
        data.add((int) timeProgramService.count());

        barChartDataVO.setName(name);
        barChartDataVO.setData(data);
        barChartDataVOList.add(barChartDataVO);
        return barChartDataVO;
    }

    /*
    PieChartDeviceNumberVO
     */

    public static List<PieChartDeviceNumberVO> getPieChartDeviceNumberVO() {
        // 创建结果列表
        List<PieChartDeviceNumberVO> pieChartDeviceNumberVOList = new ArrayList<>();

        // 查询在线设备数量
        LambdaQueryWrapper<DeviceConfig> onlineWrapper = new LambdaQueryWrapper<>();
        onlineWrapper.eq(DeviceConfig::getConnected, true);
        Integer online = Math.toIntExact(deviceConfigService.count(onlineWrapper));

        // 查询离线设备数量
        LambdaQueryWrapper<DeviceConfig> offlineWrapper = new LambdaQueryWrapper<>();
        offlineWrapper.eq(DeviceConfig::getConnected, false);
        Integer offline = Math.toIntExact(deviceConfigService.count(offlineWrapper));

        // 计算总数
        Integer total = online + offline;

        // 避免除以零的情况
        if (total == 0) {
            total = 1; // 或者处理为其他合适的值
        }

        // 计算百分比
        int onlineRatio = (int) ((double) online / total * 100);
        int offlineRatio = (int) ((double) offline / total * 100);

        // 创建在线设备的数据对象
        PieChartDeviceNumberVO onlineVO = new PieChartDeviceNumberVO();
        onlineVO.setName("在线(个)");
        onlineVO.setValue(online);
        onlineVO.setRatio(onlineRatio);
        pieChartDeviceNumberVOList.add(onlineVO);

        // 创建离线设备的数据对象
        PieChartDeviceNumberVO offlineVO = new PieChartDeviceNumberVO();
        offlineVO.setName("离线(个)");
        offlineVO.setValue(offline);
        offlineVO.setRatio(offlineRatio);
        pieChartDeviceNumberVOList.add(offlineVO);

        // 创建故障设备的数据对象（假设没有故障设备）
        PieChartDeviceNumberVO faultVO = new PieChartDeviceNumberVO();
        faultVO.setName("故障(个)");
        faultVO.setValue(0);
        faultVO.setRatio(0); // 故障设备的百分比为0
        pieChartDeviceNumberVOList.add(faultVO);

        return pieChartDeviceNumberVOList;
    }
}
