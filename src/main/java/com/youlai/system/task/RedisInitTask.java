package com.youlai.system.task;

import com.youlai.system.common.util.RedisUtil;
import com.youlai.system.global.GlobalData;
import com.youlai.system.manager.LanguageManager;
import com.youlai.system.model.entity.RedisNode;
import com.youlai.system.model.entity.TrendConfig;
import com.youlai.system.plugin.opcua.OpcUaHelper;
import com.youlai.system.service.TrendConfigService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@Order(3)
@RequiredArgsConstructor
public class RedisInitTask implements CommandLineRunner {


    private final TrendConfigService trendConfigService;
    private final RedisUtil redisUtil;
    private  final LanguageManager languageManager;
    private final OpcUaHelper opcUaHelper;

    public void run(String... args) {
        log.info("初始化任务3：程序任务初始化启动");
        log.info("初始化任务3：初始化 Redis ");

        //初始化redis 创建redis
        List<TrendConfig> trendConfigList =trendConfigService.list();

        for(TrendConfig  item: trendConfigList){
            RedisNode redisNode = new RedisNode();
            redisNode.setTrendConfig(item);
            String redisName = opcUaHelper.getRedisName(redisNode.getTrendConfig().getDevice(),item.getAddress());
            if(redisUtil.get(redisName) == null){
                redisUtil.set(redisName,redisNode,1, TimeUnit.DAYS);
//                redisUtil.set(item.getAddress(),redisNode,1, TimeUnit.DAYS);


            }

        }


        languageManager.setCurrentLanguage(GlobalData.Language.ZH_CN);

    }
}
