package com.youlai.system.task;

import com.youlai.system.model.entity.DeviceConfig;
import com.youlai.system.service.DeviceConfigService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@Order(2)
@RequiredArgsConstructor
public class DatabaseInitTask implements CommandLineRunner {

    private final DeviceConfigService deviceConfigService;

    @Override
    public void run(String... args) {
        log.info("初始化任务2：程序任务初始化启动");
        log.info("初始化任务2：初始化 DeviceConfigService ");

        try {
            List<DeviceConfig> deviceConfigList = deviceConfigService.list();
            deviceConfigList.forEach(deviceConfig -> {
                deviceConfig.setConnected(false);
                deviceConfig.setSubscribed(false);
                deviceConfigService.updateById(deviceConfig);
            });
        } catch (Exception e) {
            log.error("设备配置更新时发生错误", e);
        }
    }
}
