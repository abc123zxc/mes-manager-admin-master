package com.youlai.system.task;

import com.youlai.system.global.GlobalData;
import com.youlai.system.plugin.opcua.OpcUaHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Order(1)
@RequiredArgsConstructor
public class CommInitTask implements CommandLineRunner {


    /*
    *
    *总结一下，run(String... args) 方法是在Spring Boot应用完成了所有的初始化工作
    * （包括上下文的加载和所有Bean的初始化）之后执行的。
    * 如果你需要在应用启动后立即执行一些操作，那么这个方法是合适的选择。
    *
    * */

    @Override
    public void run(String... args) throws Exception {

        log.info("初始化任务1：程序任务初始化启动");
        GlobalData.opcUaHelper = new OpcUaHelper();

    }
}
