package com.youlai.system.opcua;

import cn.hutool.core.date.DateTime;
import com.youlai.system.plugin.opcua.OpcUaHelper;
import org.eclipse.milo.opcua.sdk.client.subscriptions.ManagedDataItem;
import org.eclipse.milo.opcua.stack.core.UaException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class OPCUATest2 {


    static OpcUaHelper opcuahelper = new OpcUaHelper("192.168.0.1", 4840);
    static List<String> subList = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.schedule(() -> {
            System.out.println("任务开始执行：" + System.currentTimeMillis());
            try {
                executeMyTask();
            } catch (UaException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, 10, TimeUnit.SECONDS);

        try{

            opcuahelper.Connect();

            List<String> nodeItems = new ArrayList<>();
            nodeItems.add("DBLine.Real1");
            nodeItems.add("DBLine.Real2");
            nodeItems.add("DBLine.Real3");
            List<String> str = opcuahelper.FormatNodeIds(nodeItems);
            subNodeItems(str);

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private static void executeMyTask() throws UaException, InterruptedException {
        System.out.println("---------------------delay event--------------------------------");

//        List<String> nodeItems = new ArrayList<>();
//        nodeItems.add("DBLine.Real1");
//        nodeItems.add("DBLine.Real11");
//        nodeItems.add("DBLine.Real100");
//        List<String> str = opcuahelper.FormatNodeIds(nodeItems);
//        subNodeItems(str);

        List<String> nodeItems = new ArrayList<>();
        nodeItems.add("ns=3;s=\"DBLine\".\"Real1\"");
        nodeItems.add("ns=3;s=\"DBLine\".\"Real2\"");
        nodeItems.add("ns=3;s=\"DBLine\".\"Real3\"");
        subNodeItems(nodeItems);

    }

    private static  void subNodeItems(List<String> list) throws UaException, InterruptedException {


        subList = list;

        ManagedDataItem.DataValueListener listener = (it, val) ->{
            System.out.println("=====订阅nodeid====== :" + it.getReadValueId().getNodeId().getIdentifier() + DateTime.now());
            System.out.println("=====订阅value===== :" + val.getValue().getValue());
        };

        opcuahelper.SetSubscriptionItems(subList, listener);
        opcuahelper.SubscriptionHandler();


        //持续订阅
        Thread.sleep(Long.MAX_VALUE);

    }
//    private static  void subNodeItems(List<String> list) throws UaException, InterruptedException {
//
//
//        subList = list;
//
//        ManagedDataItem.DataValueListener listener = (it, val) ->{
//            System.out.println("=====订阅nodeid====== :" + it.getReadValueId().getNodeId().getIdentifier() + DateTime.now());
//            System.out.println("=====订阅value===== :" + val.getValue().getValue());
//        };
//
//        opcuahelper.SetSubscriptionItems(subList, listener);
//        opcuahelper.SubscriptionHandler();
//
//
//        //持续订阅
//        Thread.sleep(Long.MAX_VALUE);
//
//    }


}
