package com.youlai.system.opcua;

import com.youlai.system.plugin.opcua.OpcUaHelper;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;

public class OPCUATest {


    public static void main(String[] args)  {
        try{
            OpcUaHelper opcuahelper = new OpcUaHelper("192.168.0.1", 4840);
            opcuahelper.Connect();

            // 要写入的节点ID (这里假设节点ID为ns=2;s=MyWord)
            NodeId nodeId = new NodeId(2, "MyWord");

            // 要写入的数据 (例如，写入12345，确保它是16位无符号整数)
            UInteger valueToWrite = UInteger.valueOf(12345);

            // 创建DataValue对象
            DataValue dataValue = new DataValue(new Variant(valueToWrite));

            // 执行写操作
//            opcuahelper.writeValue(nodeId, dataValue).get();

            System.out.println("Value written successfully.");


//            List<String> varList = new ArrayList<>();
//            varList.add("ns=3;s=\"DBLine\".\"Real1\"");
//            varList.add("ns=3;s=\"ProductionCountData\".\"OneDayCountor\"[1]");
//            varList.add("ns=3;s=\"ProductionCountData\".\"OneDayCountor\"[2]");
//            varList.add("ns=3;s=\"ProductionCountData\".\"OneDayCountor\"[3]");
//            varList.add("ns=3;s=\"ProductionCountData\".\"OneDayCountor\"[4]");
//            varList.add("ns=3;s=\"ProductionCountData\".\"OneDayCountor\"[5]");
//            varList.add("ns=3;s=\"ProductionCountData\".\"OneDayCountor\"[6]");
//            varList.add("ns=3;s=\"ProductionCountData\".\"OneDayCountor\"[7]");
//            varList.add("ns=3;s=\"ProductionCountData\".\"OneDayCountor\"[8]");
//            varList.add("ns=3;s=\"ProductionCountData\".\"OneDayCountor\"[9]");
//            varList.add("ns=3;s=\"ProductionCountData\".\"OneDayCountor\"[10]");


//            UaMonitoredItem.ValueConsumer subscribeCallback = (it, val) -> {
//                System.out.println("=====订阅nodeid====== :" + it.getReadValueId().getNodeId().getIdentifier() + DateTime.now());
//                System.out.println("=====订阅value===== :" + val.getValue().getValue());
//            };
//            ManagedDataItem.DataValueListener listener = (it, val) ->{
//                System.out.println("=====订阅nodeid====== :" + it.getReadValueId().getNodeId().getIdentifier() + DateTime.now());
//                System.out.println("=====订阅value===== :" + val.getValue().getValue());
//            };
//            opcuahelper.Subscribe(varList,subscribeCallback );
//            opcuahelper.SetSubscriptionItems(varList, listener);
//            opcuahelper.SubscriptionHandler();
//
//
//            //持续订阅
//            Thread.sleep(Long.MAX_VALUE);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
