package com.youlai.system.opcua;

import com.youlai.system.common.util.CommUtils;

public class Test2 {

    public static void main(String[] args) {
        float currentValue = 15.00f;
        float previousValue = 15.00f;
        Integer tolerance = 1;
        System.out.println(CommUtils.IsWithinTolerance(currentValue, previousValue, tolerance));
    }
}
