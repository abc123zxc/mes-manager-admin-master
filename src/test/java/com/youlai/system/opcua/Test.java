package com.youlai.system.opcua;

public class Test {

    public static String removeDoubleQuotes(String input) {
        if (input == null) {
            return "";
        }
        return input.replaceAll("\"", "");
    }
    public static void main(String[] args) {

        String testString = "\"DBLine\".\"Real2\"[0]";
        System.out.println(testString);
        System.out.println(removeDoubleQuotes(testString));
    }

}
