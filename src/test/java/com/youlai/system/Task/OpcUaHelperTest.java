package com.youlai.system.Task;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.identity.AnonymousProvider;
import org.eclipse.milo.opcua.sdk.client.api.subscriptions.UaMonitoredItem;
import org.eclipse.milo.opcua.sdk.client.api.subscriptions.UaSubscription;
import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;
import org.eclipse.milo.opcua.stack.core.types.enumerated.MonitoringMode;
import org.eclipse.milo.opcua.stack.core.types.enumerated.TimestampsToReturn;
import org.eclipse.milo.opcua.stack.core.types.structured.MonitoredItemCreateRequest;
import org.eclipse.milo.opcua.stack.core.types.structured.MonitoringParameters;
import org.eclipse.milo.opcua.stack.core.types.structured.ReadValueId;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class OpcUaHelperTest {
    private static final int DEFAULT_PORT = 4840;
    private static final String SECURITY_DIR_NAME = "security";
    private static final Path SECURITY_TEMP_DIR = Paths.get(System.getProperty("java.io.tmpdir"), SECURITY_DIR_NAME);

    public OpcUaHelperTest() {
        createSecurityDirectory();
    }

    /**
     * 创建安全目录。
     */
    private void createSecurityDirectory() {
        try {
            Files.createDirectories(SECURITY_TEMP_DIR);
            if (!Files.exists(SECURITY_TEMP_DIR)) {
                throw new RuntimeException("Unable to create security directory: " + SECURITY_TEMP_DIR);
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed to create security directory: " + SECURITY_TEMP_DIR, e);
        }
    }

    /**
     * 获取OPC UA客户端的端点URL。
     *
     * @param ip   IP地址
     * @param port 端口号
     * @return 端点URL
     */
    public String getEndpointUrl(String ip, int port) {
        return "opc.tcp://" + ip + ":" + port;
    }

    /**
     * 创建并连接OPC UA客户端。
     *
     * @param ip IP地址
     * @return 连接成功的OPC UA客户端实例
     * @throws Exception 如果连接过程中出现异常
     */
    public OpcUaClient connect(String ip) throws Exception {
        OpcUaClient opcUaClient = createClient(ip, DEFAULT_PORT);
        try {
            opcUaClient.connect().get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException("Failed to connect to " + ip, e);
        }
        return opcUaClient;
    }

    /**
     * 创建OPC UA客户端。
     *
     * @param ip   IP地址
     * @param port 端口号
     * @return 新创建的OPC UA客户端实例
     * @throws Exception 如果创建过程中出现异常
     */
    private OpcUaClient createClient(String ip, int port) throws Exception {
        return OpcUaClient.create(
                getEndpointUrl(ip, port),
                endpoints -> endpoints.stream()
                        .filter(e -> e.getSecurityPolicyUri().equals(SecurityPolicy.None.getUri()))
                        .findFirst(),
                configBuilder -> configBuilder
                        .setApplicationName(LocalizedText.english("opc-ua"))
                        .setApplicationUri("")
                        .setIdentityProvider(new AnonymousProvider())
                        .setRequestTimeout(UInteger.valueOf(5000))
                        .build()
        );
    }
    /**
     * 创建订阅任务。
     *
     * @param client OPC UA 客户端
     * @param ip     IP地址
     * @param nodeIds 节点ID列表
     * @return 订阅任务
     */
    public Runnable createSubscriptionTask(OpcUaClient client, String ip, List<NodeId> nodeIds) {
        return () -> {
            System.out.println("Starting subscription for " + ip);

            // 创建订阅
            CompletableFuture<UaSubscription> subscriptionFuture = client.getSubscriptionManager().createSubscription(1000.0);

            try {
                UaSubscription subscription = subscriptionFuture.get(5, TimeUnit.SECONDS);
                System.out.println("Subscription created: " + subscription);

                // 为每个节点创建监控项
                addMonitoredItems(subscription, nodeIds);

            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                System.err.println("Failed to create subscription: " + e.getMessage());
            }
        };
    }

    /**
     * 为给定的节点列表添加监控项。
     *
     * @param subscription 订阅
     * @param nodeIds      节点ID列表
     */
    private void addMonitoredItems(UaSubscription subscription, List<NodeId> nodeIds) {
        List<MonitoredItemCreateRequest> requests = new ArrayList<>();

        for (NodeId nodeId : nodeIds) {
            MonitoringParameters parameters = new MonitoringParameters(
                    UInteger.valueOf(100), // 客户端指定的监控项ID
                    1000.0, // 采样间隔
                    null, // 过滤器（可选）
                    UInteger.valueOf(100), // 队列大小
                    true // 是否丢弃过时的数据
            );

            ReadValueId readValueId = new ReadValueId(
                    nodeId,
                    UInteger.valueOf(100), // 属性ID，表示要读取的属性是节点的值
                    null, // 属性ID索引范围（可选）
                    null // 数据编码（可选）
            );

            MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(
                    readValueId,
                    MonitoringMode.Reporting,
                    parameters
            );

            requests.add(request);
        }


        CompletableFuture<List<UaMonitoredItem>> monitoredItemsFuture = subscription.createMonitoredItems(TimestampsToReturn.Server, requests);

        monitoredItemsFuture.thenAccept(monitoredItems -> {
            for (UaMonitoredItem item : monitoredItems) {
                System.out.println("Monitored item created: " + item);

                // 设置值变更监听器
                item.setValueConsumer(this::onItemValueChanged);
            }
        }).exceptionally(e -> {
            System.err.println("Failed to create monitored items: " + e.getMessage());
            return null;
        });
    }

    /**
     * 处理监控项值变更事件。
     *
     * @param item   监控项
     * @param value  数据值
     */
    private void onItemValueChanged(UaMonitoredItem item, DataValue value) {
        StatusCode statusCode = value.getStatusCode();
        if (statusCode.isGood()) {
            Object valueObj = value.getValue().getValue();
            System.out.println("Node: " + item.getReadValueId().getNodeId() + " Value: " + valueObj);
        } else {
            System.err.println("Error reading value: " + statusCode);
        }
    }
}